/** @type {import('next-sitemap').IConfig} */

const siteUrl = process.env.SITE_URL || 'https://offers.vacay.co.ke'

module.exports = {
    siteUrl,
    generateRobotsTxt: true,
    changefreq: 'daily',
    priority: 0.7,
    sitemapSize: 7000,
    exclude: ['/404', '/500', '/auth/*', '/api/*', '/dashboard/*'],
    robotsTxtOptions: {
        policies: [
            {
                userAgent: '*',
                allow: '/',
            },
            {
                userAgent: '*',
                disallow: ['/auth/*', '/api/*', '/dashboard/*'],
            },
        ],
        additionalSitemaps: [
            `${siteUrl}/server-sitemap.xml`,
        ],
    },
}
