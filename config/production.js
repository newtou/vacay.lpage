require('dotenv').config()
module.exports = {
    dbs: {
        mongodb: {
            uri: process.env.MONGO_URI,
        },
    },
    port: 8080,

    cloudinary: {
        name: process.env.CLOUD_NAME,
        api: process.env.CLOUD_API,
        secret: process.env.CLOUD_SECRET,
        upload_preset: process.env.CLOUD_UPLOAD_PRESET,
    },

    jwt: {
        secret: process.env.JWT_SECRET,
    },
    authorized: {
        emails: process.env.AUTHORIZED_EMAILS,
    },
    slack: {
        webhook: process.env.SLACK_WEBHOOK,
    },
    logging: {
        console: true,
        path: 'logs',
    },
    cors: {
        enabled: true,
        origins: [
            'http://localhost:3000',
            'https://offers.vacay.co.ke',
            'https://vacay-lpage.vercel.app',
            'https://vacay-landingadmin.vercel.app',
        ],
    },
}
