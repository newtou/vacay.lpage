import { useState, useEffect, useCallback } from 'react'
import { invoke } from '../lib/axios.config'

export function useUploads(
    files: string,
    setFiles: (files: string[]) => void, // eslint-disable-line
    map_id: string,
    updateGallery: () => void // eslint-disable-line
) {
    const [error, setError] = useState(null)
    const [progress, setProgress] = useState(0)

    const uploadImages = useCallback(
        async (items: unknown) => {
            // get time started
            setProgress(0)
            const query = new URLSearchParams({ map_id }).toString()

            const { res, error } = await invoke(
                'POST',
                `media?${query}`,
                items,
                setProgress
            )

            if (error) {
                setProgress(0)
                setError(error)
                return
            }

            if (res.images.count > 0) {
                setFiles(null)
                updateGallery()
            }
        },

        [setFiles, map_id, updateGallery]
    )

    useEffect(() => {
        uploadImages(files)
    }, [files, uploadImages])

    useEffect(() => {
        if (error) {
            setTimeout(() => {
                setError(null)
            }, 4000)
        }
    }, [error, , progress])

    return { progress, error }
}
