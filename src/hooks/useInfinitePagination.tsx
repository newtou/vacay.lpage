import { useEffect } from 'react'

type fetcherParamTypes = {
    page: number
    limit: number
}

function useInfinitePagination(
    fetcher: ({ page, limit }: fetcherParamTypes) => void, // eslint-disable-line
    page: number,
    limit: number
) {
    // run the fetcher function and update the state
    useEffect(() => {
        fetcher({ page, limit })
    }, [fetcher, page, limit])
    return null
}

export default useInfinitePagination
