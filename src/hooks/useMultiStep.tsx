import { useState } from 'react'

type MultiStep = {
    next: () => void
    prev: () => void
    steps: any[]
    jumpTo: (step: number) => void // eslint-disable-line
    activeStepIndex: number
    Active: any // eslint-disable-line
    isFirstStep: boolean
    isLastStep: boolean
}

export const useMultiStep = (steps: any[]): MultiStep => {
    const [activeStepIndex, setActiveStepIndex] = useState<number>(0)

    // next step
    const next = () => {
        setActiveStepIndex((prev) => {
            if (prev < steps.length - 1) {
                return prev + 1
            }
            return prev
        })
    }

    // previous step
    const prev = () => {
        setActiveStepIndex((prev) => {
            if (prev > 0) {
                return prev - 1
            }
            return prev
        })
    }

    // jump to a specific step
    const jumpTo = (step: number) => {
        setActiveStepIndex(step)
    }

    return {
        next,
        prev,
        steps,
        jumpTo,
        activeStepIndex,
        Active: steps[activeStepIndex],
        isFirstStep: activeStepIndex === 0,
        isLastStep: activeStepIndex === steps.length - 1,
    }
}
