import { useEffect } from 'react'
import { useRouter } from 'next/router'

function usePixel() {
    const router = useRouter()
    const isProduction = process.env.NODE_ENV === 'production'

    useEffect(() => {
        import('react-facebook-pixel')
            .then((x) => x.default)
            .then((ReactPixel) => {
                if (!isProduction) return

                ReactPixel.init(process.env.META_PIXEL)
                ReactPixel.pageView()

                router.events.on('routeChangeComplete', () => {
                    ReactPixel.pageView()
                })
            })
    }, [router.events, isProduction])
    return null
}

export default usePixel
