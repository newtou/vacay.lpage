import React, { useMemo } from 'react'

type Pagination = {
    nextPage: () => void
    previousPage: () => void
    pages: number[]
    currentPage: number
    sliceStart: number
    sliceEnd: number
}

const usePagination = (PerPage = 10, items: any[]): Pagination => {
    const [currentPage, setCurrentPage] = React.useState(1)

    const pages = []
    for (let i = 1; i <= Math.ceil(items.length / PerPage); i++) {
        pages.push(i)
    }

    // Get current posts
    const indexOfLastItem = useMemo(
        () => currentPage * PerPage,
        [currentPage, PerPage]
    )
    const indexOfFirstItem = useMemo(
        () => indexOfLastItem - PerPage,
        [indexOfLastItem, PerPage]
    )

    // next page
    const nextPage = () => {
        // check if there is a next page
        if (currentPage < pages.length) {
            setCurrentPage(currentPage + 1)
        }
    }

    // previous page
    const previousPage = () => {
        if (currentPage > 1) {
            setCurrentPage(currentPage - 1)
        }
    }

    return {
        nextPage,
        previousPage,
        pages,
        currentPage,
        sliceStart: indexOfFirstItem,
        sliceEnd: indexOfLastItem,
    }
}

export default usePagination
