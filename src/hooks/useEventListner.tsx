import { useEffect, useRef } from 'react'

function useEventListener(
    event: string,
    callback: (event: Event | undefined | null) => void, // eslint-disable-line
    element: HTMLElement | typeof window = window
) {
    const callbackRef = useRef(callback)

    useEffect(() => {
        callbackRef.current = callback
    }, [callback])

    useEffect(() => {
        if (element == null) return
        const handler = (e: Event) => callbackRef.current(e)
        element.addEventListener(event, handler)

        return () => element.removeEventListener(event, handler)
    }, [event, element])
}

export default useEventListener
