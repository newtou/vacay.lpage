import { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { AppDispatch, RootState } from '../store'
import { fetchOffers } from '../store/slices/Offers'

function useOffers(search: string) {
    const dispatch = useDispatch<AppDispatch>()
    const { offers, fetching } = useSelector((state: RootState) => state.offers)

    useEffect(() => {
        dispatch(fetchOffers({ search }))
    }, [dispatch, search])

    return { loading: fetching, offers }
}

export default useOffers
