import { useMemo, useRef } from 'react'

function useOptimize(url = '') {
    const ref = useRef()

    const optimizedSrc = useMemo(() => {
        if (!url) return ''
        const cloudinaryUrl = `https://res.cloudinary.com/${process.env.CLOUD_NAME}/image/fetch/f_auto,q_auto,c_fill,g_auto/`
        return `${cloudinaryUrl}${encodeURIComponent(url)}`
    }, [url])

    const blurredSrc = useMemo(() => {
        if (!url) return ''
        const cloudinaryUrl = `https://res.cloudinary.com/${process.env.CLOUD_NAME}/image/fetch/f_auto,q_10,c_fill,g_auto/`
        return `${cloudinaryUrl}${encodeURIComponent(url)}`
    }, [url])

    return { ref, optimizedSrc, blurredSrc }
}

export default useOptimize
