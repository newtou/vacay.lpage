import React from 'react'
import { Skeleton } from '@chakra-ui/react'

function OfferItemLazy() {
    return <Skeleton w='full' h='12vh' rounded='xl' my='1rem' />
}

export default OfferItemLazy
