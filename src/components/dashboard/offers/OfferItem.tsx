import React from 'react'
import PropTypes from 'prop-types'
import Link from 'next/link'
import {
    Badge,
    Heading,
    Text,
    VStack,
    MenuDivider,
    Box,
    HStack,
    Grid,
    GridItem,
} from '@chakra-ui/react'
import moment from 'moment'
import {
    HiOutlineTrash,
    HiOutlinePencilSquare,
    HiOutlinePhoto,
} from 'react-icons/hi2'
import { HiOutlineSearch } from 'react-icons/hi'
import { useDispatch } from 'react-redux'
import { removeOffer } from '@/src/store/slices/Offers'
import DeleteModal from '@/src/components/common/DeleteModal'
import Menu from '../../common/Menu'
import useDimensions from 'react-cool-dimensions'
import CustomImage from '../../common/CustomImage'
import { Offer } from '@/types/index'
import EditOfferDrawr from '../../drawrs/offer/EditOfferDrawr'
import ImageDrawr from '../../drawrs/images/ImageDrawr'

import SeoDrawr from '../../drawrs/offer/SeoDrawr'
import { AppDispatch } from '@/src/store'
import { useEdgeStore } from '@/src/lib/edgestore'

type OfferListItemProps = {
    offer: Offer
}

function OfferListItem({ offer }: OfferListItemProps) {
    const { edgestore } = useEdgeStore()
    const dispatch = useDispatch<AppDispatch>()

    const handleDelete = async (id: string) => {
        dispatch(removeOffer(id))
    }
    const { observe, width, height } = useDimensions()
    return (
        <Grid
            templateColumns='repeat(5, 1fr)'
            gap='2rem'
            alignItems='center'
            justifyContent='space-between'
            minH='10vh'
            bg='white'
            shadow='sm'
            p='20px'
            rounded='xl'
            my='1rem'>
            <GridItem colSpan={1}>
                <Box ref={observe} width='full' height='100px'>
                    {offer?.images ? (
                        <CustomImage
                            src={
                                offer?.images[0]?.url ||
                                '/images/imgplaceholder.png'
                            }
                            alt={offer?.name}
                            width={width}
                            height={height}
                            rounded='lg'
                            objectFit='cover'
                            shadow='sm'
                        />
                    ) : null}
                </Box>
            </GridItem>
            <GridItem colSpan={2}>
                <VStack
                    gap='3'
                    alignItems='flex-start'
                    justifyContent='flex-start'
                    w='full'>
                    <Link href={`/dashboard/offers/${offer?.slug}`} passHref>
                        <Heading
                            fontSize='xl'
                            fontWeight='bold'
                            cursor='pointer'>
                            {offer?.name}
                        </Heading>
                    </Link>
                    <Text as='small' fontSize='md' color='gray.500'>
                        Published &bull;{' '}
                        {moment(offer?.created).format('DD MMM, YY')}
                    </Text>
                </VStack>
            </GridItem>
            <GridItem colSpan={1}>
                <HStack>
                    <Badge
                        p='5px 10px'
                        fontSize='sm'
                        fontWeight='400'
                        textTransform='capitalize'
                        colorScheme={offer?.active ? 'teal' : 'gray'}
                        rounded='full'>
                        {offer?.active ? 'Active' : 'Inactive'}
                    </Badge>
                    <Badge
                        p='5px 10px'
                        fontSize='sm'
                        fontWeight='400'
                        textTransform='capitalize'
                        colorScheme={offer?.featured ? 'blue' : 'gray'}
                        rounded='full'>
                        {offer?.featured ? 'Featured' : 'Not featured'}
                    </Badge>
                </HStack>
            </GridItem>
            <GridItem
                as={HStack}
                alignItems='center'
                justifyContent='center'
                colSpan={1}>
                <Menu>
                    <SeoDrawr
                        mapId={offer?.id}
                        mapTitle={offer?.name}
                        defaultOptions={offer?.images}
                        renderTrigger={(onOpen) => (
                            <Menu.Item
                                icon={<HiOutlineSearch size='20px' />}
                                rounded='md'
                                onClick={onOpen}
                                fontSize='lg'>
                                Manage seo
                            </Menu.Item>
                        )}
                    />
                    <ImageDrawr
                        map_id={offer?.id}
                        renderTrigger={(onClick) => (
                            <Menu.Item
                                onClick={onClick}
                                icon={<HiOutlinePhoto size='20px' />}
                                rounded='md'
                                fontSize='lg'>
                                Manage media
                            </Menu.Item>
                        )}
                    />

                    <MenuDivider />

                    <EditOfferDrawr
                        offer={offer}
                        renderTrigger={(onOpen) => (
                            <Menu.Item
                                icon={<HiOutlinePencilSquare size='20px' />}
                                rounded='md'
                                fontSize='lg'
                                _hover={{
                                    color: 'brand.base',
                                    bg: 'blue.50',
                                }}
                                _focus={{
                                    color: 'brand.base',
                                    bg: 'blue.50',
                                }}
                                _active={{
                                    color: 'brand.base',
                                    bg: 'blue.50',
                                }}
                                onClick={onOpen}>
                                Edit Offer
                            </Menu.Item>
                        )}
                    />
                    <DeleteModal
                        renderTrigger={(onOpen) => (
                            <Menu.Item
                                rounded='md'
                                fontSize='lg'
                                _hover={{
                                    color: 'red.500',
                                    bg: 'red.50',
                                }}
                                _focus={{
                                    color: 'red.500',
                                    bg: 'red.50',
                                }}
                                _active={{
                                    color: 'red.500',
                                    bg: 'red.50',
                                }}
                                onClick={onOpen}
                                icon={<HiOutlineTrash size='20px' />}>
                                Delete Offer
                            </Menu.Item>
                        )}
                        id={offer?.id}
                        label={offer?.name}
                        delFunc={(id) => {
                            const edgeStoreImages = offer?.images.map(
                                (image) => {
                                    if (image?.url.includes('edgestore')) {
                                        return image?.url
                                    }
                                }
                            )

                            handleDelete(id)

                            if (edgeStoreImages) {
                                edgeStoreImages.forEach(async (image) => {
                                    await edgestore.publicFiles.delete({
                                        url: image,
                                    })
                                })
                            }
                        }}
                    />
                </Menu>
            </GridItem>
        </Grid>
    )
}

OfferListItem.propTypes = {
    offer: PropTypes.object.isRequired,
}

OfferListItem.defaultProps = {
    offer: {},
}

export default OfferListItem
