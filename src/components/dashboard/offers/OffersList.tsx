import React, { Suspense, lazy } from 'react'
import { Box, HStack } from '@chakra-ui/react'
import { ErrorBoundary } from 'react-error-boundary'
import Unavailable from '@/src/components/common/Unavailable'
import usePagination from '@/src/hooks/usePagination'
import Paginate from '../../common/Paginate'
import ErrorHandler from '../../common/ErrorHandler'
import SearchBar from '../../common/SearchBar'
import { Offer } from '@/types/index'
import OfferDrawr from '../../drawrs/offer/OfferDrawr'
import OfferItemLazy from './OfferItemLazy'
import Button from '../../ui/atoms/Button'

const OfferItem = lazy(() => import('./OfferItem'))

type OffersListProps = {
    offers: Offer[]
    loading: boolean
    searchTerm: string
    setSearchTerm: (value: string) => void // eslint-disable-line
}

function OffersList({
    offers,
    loading,
    searchTerm,
    setSearchTerm,
}: OffersListProps) {
    const { nextPage, previousPage, pages, currentPage, sliceStart, sliceEnd } =
        usePagination(8, offers)

    return (
        <ErrorBoundary FallbackComponent={ErrorHandler}>
            <Box
                position='relative'
                minHeight='60vh'
                mt='1rem'
                mx='auto'
                width='full'>
                <HStack
                    height='auto'
                    alignItems='center'
                    justifyContent='space-between'
                    bg='white'
                    p='20px'
                    rounded='xl'>
                    <SearchBar
                        label=''
                        term={searchTerm}
                        setTerm={setSearchTerm}
                        full
                    />

                    <OfferDrawr
                        renderTrigger={(onOpen) => (
                            <Button text='Add New Offer' onClick={onOpen} />
                        )}
                    />
                </HStack>
                <Box mt='1rem' rounded='lg'>
                    {offers.slice(sliceStart, sliceEnd).map((offer) => (
                        <Suspense key={offer?.id} fallback={<OfferItemLazy />}>
                            <OfferItem offer={offer} />
                        </Suspense>
                    ))}

                    {offers.length > 0 && !loading && (
                        <HStack
                            my='1rem'
                            alignItems='center'
                            justifyContent='start'>
                            <Paginate
                                next={nextPage}
                                prev={previousPage}
                                currentPage={currentPage}
                                pages={pages}
                                prevLabel='Prev'
                                nextLabel='Next'
                            />
                        </HStack>
                    )}
                    {!loading && !offers.length && (
                        <Box position='relative' width='100%' minHeight='50vh'>
                            <Unavailable message='✈ No published offers available' />
                        </Box>
                    )}
                </Box>
            </Box>
        </ErrorBoundary>
    )
}

OffersList.defaultProps = {
    offers: [],
}

export default OffersList
