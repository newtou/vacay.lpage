import {
    Grid,
    Text,
    Badge,
    VStack,
    HStack,
    GridItem,
    useDisclosure,
} from '@chakra-ui/react'
import React from 'react'
import Menu from '../../common/Menu'
import DeleteModal from '../../common/DeleteModal'
import { useDispatch } from 'react-redux'
import { HiOutlineTrash, HiOutlinePencilSquare } from 'react-icons/hi2'
import moment from 'moment'
import { deleteUser } from '@/src/store/slices/Users'
import { useSelector } from 'react-redux'
import EditUser from './EditUser'
import { User } from '@/types/index'
import { AppDispatch, RootState } from '@/src/store'

type UserItemProps = {
    user: User
}

function UserItem({ user }: UserItemProps) {
    const dipatch = useDispatch<AppDispatch>()
    const { loading } = useSelector((state: RootState) => state.users)
    const { me } = useSelector((state: RootState) => state.auth)

    const {
        isOpen: isEditOpen,
        onOpen: onEditOpen,
        onClose: onEditClose,
    } = useDisclosure()

    const deleteFunc = (id: string) => {
        dipatch(deleteUser(id))
    }
    return (
        <>
            <EditUser isOpen={isEditOpen} onClose={onEditClose} user={user} />

            <Grid
                gridTemplateColumns='repeat(5, 1fr)'
                bg='white'
                minH='10vh'
                shadow='sm'
                rounded='xl'
                p='25px'
                gap='1rem'
                w='full'>
                <GridItem as={HStack}>
                    <VStack alignItems='start' gap='2'>
                        <Text>{user?.name}</Text>
                        <Badge
                            p='5px 10px'
                            rounded='lg'
                            maxW='fit-content'
                            colorScheme={
                                user?.role === 'admin' ? 'teal' : 'blue'
                            }>
                            {user?.role}
                        </Badge>
                    </VStack>
                </GridItem>

                <GridItem as={HStack}>
                    <Text>{user?.email}</Text>
                </GridItem>
                <GridItem as={HStack}>
                    <Text>
                        {moment(user?.lastLogin).format('DD MMM, YY hh:mm a')}
                    </Text>
                </GridItem>
                <GridItem as={HStack}>
                    <Text>{moment(user?.created).format('DD MMM, YYYY')}</Text>
                </GridItem>

                <GridItem as={HStack}>
                    <Menu>
                        <Menu.Item
                            _hover={{
                                bg: 'blue.50',
                                color: 'brand.base',
                            }}
                            icon={<HiOutlinePencilSquare size='20px' />}
                            onClick={onEditOpen}>
                            Edit
                        </Menu.Item>
                        <DeleteModal
                            renderTrigger={(onOpen) => (
                                <Menu.Item
                                    isDisabled={me?.role !== 'admin'}
                                    _hover={{ bg: 'red.50', color: 'red.600' }}
                                    icon={<HiOutlineTrash size='20px' />}
                                    onClick={onOpen}>
                                    Delete
                                </Menu.Item>
                            )}
                            id={user?.id}
                            delFunc={deleteFunc}
                            loading={loading}
                        />
                    </Menu>
                </GridItem>
            </Grid>
        </>
    )
}

UserItem.defaultProps = {
    user: {},
}

export default UserItem
