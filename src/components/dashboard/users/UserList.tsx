import usePagination from '@/src/hooks/usePagination'
import {
    Box,
    Grid,
    Text,
    Stack,
    Alert,
    HStack,
    GridItem,
    AlertIcon,
} from '@chakra-ui/react'
import { motion } from 'framer-motion'
import React, { Suspense } from 'react'
import Paginate from '../../common/Paginate'
import Search from '../../common/Search'
import TableComponent from '../../common/TableComponent'
import UserItem from './UserItem'
import { useSelector } from 'react-redux'
import { User } from '@/types/index'
import AddUserDrawr from '../../drawrs/users/AddUserDrawr'
import Button from '../../ui/atoms/Button'
import { RootState } from '@/src/store'

type UserListProps = {
    searchTerm: string
    setSearchTerm: (term: string) => void // eslint-disable-line
}

function UserList({ searchTerm, setSearchTerm }: UserListProps) {
    const { me } = useSelector((state: RootState) => state.auth)
    const { users, fetching } = useSelector((state: RootState) => state.users)

    const HEADERS = [
        { name: 'Name', colSpan: 1 },
        { name: 'Email', colSpan: 1 },
        { name: 'Last Log In', colSpan: 1 },
        { name: 'Joined', colSpan: 1 },
        { name: 'Action', colSpan: 1 },
    ]

    const { sliceStart, sliceEnd, pages, currentPage, nextPage, previousPage } =
        usePagination(10, users)

    if (!me) return null

    if (me?.role !== 'admin')
        return (
            <Box>
                <Alert status='error' rounded='lg'>
                    <AlertIcon />
                    You are not authorized to view this page
                </Alert>
            </Box>
        )

    return (
        <>
            <Stack
                as={motion.div}
                direction='row'
                width='100%'
                alignItems='flex-start'
                spacing='3'>
                <Box w='full'>
                    <HStack
                        width='100%'
                        justify='space-between'
                        alignItems='center'
                        p='25px'
                        bg='white'
                        rounded='xl'
                        mb='2rem'>
                        <Search
                            label=''
                            term={searchTerm}
                            setTerm={setSearchTerm}
                            full
                        />
                        <AddUserDrawr
                            renderTrigger={(onOpen) => (
                                <Button
                                    text='Add New User'
                                    onClick={onOpen}
                                    disabled={fetching || me?.role !== 'admin'}
                                />
                            )}
                        />
                    </HStack>

                    <TableComponent
                        renderHeaders={() => (
                            <Grid
                                bg='white'
                                p='25px'
                                gap='1rem'
                                rounded='xl'
                                mb='1rem'
                                gridTemplateColumns='repeat(5, 1fr)'>
                                {HEADERS.map((header) => (
                                    <GridItem
                                        key={header?.name}
                                        colSpan={header?.colSpan}>
                                        <Text
                                            fontWeight='bold'
                                            fontSize='md'
                                            textTransform='uppercase'>
                                            {header.name}
                                        </Text>
                                    </GridItem>
                                ))}
                            </Grid>
                        )}>
                        {users &&
                            users
                                .slice(sliceStart, sliceEnd)
                                .map((user: User) => (
                                    <Suspense key={user?.id}>
                                        <UserItem user={user} />
                                    </Suspense>
                                ))}
                    </TableComponent>
                    <Paginate
                        currentPage={currentPage}
                        pages={pages}
                        next={nextPage}
                        prev={previousPage}
                    />
                </Box>
            </Stack>
        </>
    )
}

UserList.defaultProps = {
    users: [],
    loading: false,
    searchTerm: '',
    setSearchTerm: () => {},
}

export default UserList
