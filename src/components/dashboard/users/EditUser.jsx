import React from 'react'
import { Field, Formik } from 'formik'
import * as yup from 'yup'
import { invoke } from '@/src/lib/axios.config'
import { setError, setMessage } from '@/src/store/slices/Alert'
import UserForm from '@/src/components/forms/profile/NewUserForm'
import { useDispatch } from 'react-redux'
import { updateUser } from '@/src/store/slices/Users'
import Modall from '../../common/Modall'

function EditUser({ user, isOpen, onClose }) {
    const dispatch = useDispatch()

    const validator = yup.object().shape({
        name: yup.string().required('Name is required'),
        email: yup
            .string()
            .email('Invalid email')
            .required('Email is required'),
        role: yup.string().required('Role is required'),
    })

    const editUser = async (values, _) => {
        try {
            _.setSubmitting(true)
            const params = {
                ...values,
            }

            const { data = {} } = await invoke(
                'PUT',
                `users/${user?.id}`,
                params
            )

            dispatch(setMessage(data.message))
            dispatch(updateUser(data.user))
            _.setSubmitting(false)
            _.resetForm()
            onClose()
        } catch (error) {
            _.setSubmitting(false)
            dispatch(
                setError(
                    error.response.data ||
                        error.message ||
                        'Something went wrong'
                )
            )
        }
    }

    return (
        <Modall title='Edit User' onClose={onClose} isOpen={isOpen}>
            <Formik
                enableReinitialize
                initialValues={{
                    name: user?.name || '',
                    email: user?.email || '',
                    role: user?.role || '',
                }}
                validationSchema={validator}
                onSubmit={editUser}>
                {({ handleSubmit, errors, touched, isSubmitting }) => (
                    <UserForm
                        submit={handleSubmit}
                        errors={errors}
                        touched={touched}
                        Field={Field}
                        loading={isSubmitting}
                        edit
                    />
                )}
            </Formik>
        </Modall>
    )
}

export default EditUser
