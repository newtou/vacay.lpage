import React from 'react'
import { Image } from '@/types/index'
import {
    Box,
    Grid,
    GridItem,
    useDisclosure,
    useMediaQuery,
} from '@chakra-ui/react'
import useDimensions from 'react-cool-dimensions'
import CustomImage from './CustomImage'
import Button from '../ui/atoms/Button'
import { MdViewCarousel } from 'react-icons/md'
import Modall from './Modall'
import ImageSlider from '../offer/ImageSlider'

type ImageGridProps = {
    images: Image[]
    modalTitle?: string
}

type GridItemProps = {
    img: Image
    index: number
    onOpen: () => void
    // eslint-disable-next-line no-unused-vars
    setInitialSlide: (index: number) => void
    rowSpan?: number
    colSpan?: number
}

function GridItemComponent({
    img,
    index,
    onOpen,
    setInitialSlide,
    rowSpan,
    colSpan,
}: GridItemProps) {
    const { observe, height, width } = useDimensions()
    return (
        <GridItem
            key={img.id}
            height='100%'
            rowSpan={rowSpan || 1}
            colSpan={colSpan || 1}>
            <Box
                width='100%'
                ref={observe}
                cursor='pointer'
                position='relative'
                onClick={() => {
                    setInitialSlide(index)
                    onOpen()
                }}
                height={index === 0 ? '50vh' : '25vh'}>
                <CustomImage
                    src={img.url}
                    alt={img.pub_id}
                    width={width || 100}
                    height={height || 150}
                    objectFit='cover'
                    quality={90}
                    placeholder='blur'
                    priority
                />
                <Box
                    position='absolute'
                    _hover={{
                        bg: 'blackAlpha.100',
                    }}
                    top='0'
                    left='0'
                    height='100%'
                    width='100%'
                    zIndex={2}
                />
            </Box>
        </GridItem>
    )
}

function ImageGrid({ images, modalTitle }: ImageGridProps) {
    const { isOpen, onOpen, onClose } = useDisclosure()
    const [isMobile] = useMediaQuery('(max-width: 768px)')

    const [initialSlide, setInitialSlide] = React.useState(0)

    if (!images) return null
    return (
        <Box
            maxH='50vh'
            height='100%'
            bg='gray.100'
            rounded={['none', 'none', 'xl']}
            position='relative'
            overflow='hidden'>
            {isMobile || (images && images.length === 1) ? (
                <ImageSlider images={images} />
            ) : (
                <Grid
                    gridTemplateColumns='repeat(4, 1fr)'
                    gap='2'
                    display={['none', 'none', 'grid']}>
                    {images &&
                        images.slice(0, 5).map((img, index) => {
                            if (index === 0)
                                return (
                                    <GridItemComponent
                                        key={img.id}
                                        img={img}
                                        index={index}
                                        onOpen={onOpen}
                                        setInitialSlide={setInitialSlide}
                                        rowSpan={2}
                                        colSpan={2}
                                    />
                                )
                            if (images.length === 2) {
                                return (
                                    <GridItemComponent
                                        key={img.id}
                                        img={img}
                                        index={index}
                                        onOpen={onOpen}
                                        setInitialSlide={setInitialSlide}
                                        rowSpan={2}
                                        colSpan={2}
                                    />
                                )
                            }
                            if (
                                images.length < 5 &&
                                index === images.length - 1
                            )
                                return (
                                    <GridItemComponent
                                        key={img.id}
                                        img={img}
                                        index={index}
                                        onOpen={onOpen}
                                        setInitialSlide={setInitialSlide}
                                        rowSpan={1}
                                        colSpan={2}
                                    />
                                )
                            return (
                                <GridItemComponent
                                    key={img.id}
                                    img={img}
                                    index={index}
                                    onOpen={onOpen}
                                    setInitialSlide={setInitialSlide}
                                    rowSpan={1}
                                    colSpan={1}
                                />
                            )
                        })}

                    <Modall
                        onClose={() => {
                            onClose()
                            setInitialSlide(0)
                        }}
                        isOpen={isOpen}
                        size='5xl'
                        title={modalTitle}>
                        <Box mb='2rem' mx='0.5'>
                            <ImageSlider
                                images={images}
                                initialSlide={initialSlide}
                            />
                        </Box>
                    </Modall>
                    {images && images.length > 4 && (
                        <Button
                            right='1rem'
                            bottom='1rem'
                            position='absolute'
                            zIndex={5}
                            text='Show All'
                            icon={MdViewCarousel}
                            bg='gray.100'
                            border='1px solid'
                            borderColor='gray.600'
                            color='gray.600'
                            _hover={{
                                bg: 'gray.300',
                            }}
                            _active={{
                                bg: 'gray.200',
                            }}
                            _focus={{
                                bg: 'gray.200',
                            }}
                            onClick={onOpen}
                        />
                    )}
                </Grid>
            )}
        </Box>
    )
}

export default ImageGrid
