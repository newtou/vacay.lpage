import qs from 'querystring'
import Dropzone from 'react-dropzone'
import React, { useState } from 'react'
import {
    Box,
    Text,
    Button,
    Center,
    VStack,
    CircularProgress,
} from '@chakra-ui/react'

import { useEdgeStore } from '@/src/lib/edgestore'
import { invoke } from '@/src/lib/axios.config'
import { HiOutlineCloudUpload } from 'react-icons/hi'

type DropZoneProps = {
    updateGallery: () => void // eslint-disable-line
    map_id: string
}

function DropZone({ updateGallery, map_id }: DropZoneProps) {
    const { edgestore } = useEdgeStore()
    const [progress, setProgress] = useState<number>(0)
    const [uploading, setUploading] = useState<boolean>(false)

    const handleDrop = async (files: File[]) => {
        if (!edgestore) return

        if (!files.length) return

        setUploading(true)

        const images = await Promise.all(
            files.map(async (file) => {
                const res = await edgestore.publicFiles.upload({
                    file,
                    input: { itemId: map_id },
                    onProgressChange(progress) {
                        setProgress(progress)
                    },
                })

                return res.url
            })
        )

        const query = qs.stringify({
            map_id,
            files: images,
        })

        const { error } = await invoke('POST', `media?${query}`, {
            files: images,
        })

        if (error) {
            setUploading(false)
            throw new Error('Something went wrong', error)
        }

        updateGallery()
        setProgress(0)
        setUploading(false)
    }

    return (
        <Dropzone onDrop={handleDrop}>
            {({ getRootProps, getInputProps }) => (
                <Box as='section' my='1rem'>
                    <Box {...getRootProps()}>
                        <input {...getInputProps()} />
                        {progress || uploading ? (
                            <VStack
                                gap='3'
                                w='full'
                                alignItems='center'
                                justifyContent='space-between'>
                                <Text size='lg'>Uploading ...</Text>
                                <CircularProgress
                                    size={15}
                                    value={progress}
                                    color='brand.primary.600'
                                    isIndeterminate={uploading && !progress}
                                />
                            </VStack>
                        ) : (
                            <Box
                                bg='gray.50'
                                border='2px solid'
                                borderColor='gray.300'
                                borderStyle='dotted'
                                height='15vh'
                                rounded='xl'>
                                <Center height='100%' width='100%' gap='2'>
                                    <Button
                                        aria-label='Add images'
                                        fontSize='lg'
                                        rounded='lg'
                                        colorScheme='brand.primary'
                                        leftIcon={<HiOutlineCloudUpload />}
                                        color='white'>
                                        Upload images
                                    </Button>
                                    <Text fontSize='lg'>or drag n drop</Text>
                                </Center>
                            </Box>
                        )}
                    </Box>
                </Box>
            )}
        </Dropzone>
    )
}

export default DropZone
