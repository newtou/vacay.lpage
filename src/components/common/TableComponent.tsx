import React from 'react'
import { Box, VStack } from '@chakra-ui/react'

type TableComponentProps = {
    children: React.ReactNode
    renderHeaders: () => React.ReactNode
    caption: string
    showHeaders?: boolean
}
function TableComponent({
    children,
    renderHeaders,
    showHeaders = true,
}: TableComponentProps) {
    return (
        <Box rounded='lg'>
            {showHeaders && typeof renderHeaders === 'function'
                ? renderHeaders()
                : null}
            <VStack gap='1rem'>{children}</VStack>
        </Box>
    )
}

TableComponent.defaultProps = {
    caption: 'Table Caption',
    children: () => null,
    headers: ['Header 1', 'Header 2', 'Header 3'],
    size: 'lg',
    variant: 'striped',
}

export default TableComponent
