import React from 'react'
import PropTypes from 'prop-types'
import { Editor as TinyEditor } from '@tinymce/tinymce-react'

type EditorProps = {
    value: string
    onChange: (value: string) => void // eslint-disable-line no-unused-vars
    name: string
}

const plugins = [
    'advlist',
    'autolink',
    'lists',
    'link',
    'image',
    'charmap',
    'anchor',
    'searchreplace',
    'visualblocks',
    'fullscreen',
    'insertdatetime',
    'table',
    'paste',
    ' help',
]

function Editor({ value, onChange, name }: EditorProps) {
    return (
        // @ts-ignore
        <TinyEditor
            id={name}
            apiKey={process.env.TINYMCE_KEY}
            init={{
                skin: 'snow',
                icons: 'thin',
                plugins,
                menubar: 'file edit view insert format tools table help',
                toolbar:
                    'undo redo | formatselect | ' +
                    'bold italic backcolor | alignleft aligncenter ' +
                    'alignright alignjustify | bullist numlist outdent indent | ' +
                    'table ' +
                    'removeformat | help',
                height: '400px',
                toolbar_mode: 'sliding',
                toolbar_location: 'top',
                placeholder: 'Start typing ...',
                content_style:
                    'body { font-family:"Hauora Sans",sans-serif; font-size:14px }',
            }}
            value={value}
            onEditorChange={onChange}
        />
    )
}

Editor.propTypes = {
    value: PropTypes.string.isRequired,
    onChange: PropTypes.func.isRequired,
    name: PropTypes.string.isRequired,
}

Editor.defaultProps = {
    value: '',
    name: '',
    onChange: () => {},
}

export default Editor
