import React from 'react'
import { AnimatePresence, motion } from 'framer-motion'
import { Box } from '@chakra-ui/react'
import useMeasure from 'react-use-measure'

function ResizablePanel({ children, duration }) {
    const [ref, { height }] = useMeasure()
    return (
        <Box
            as={motion.div}
            position='relative'
            overflow='hidden'
            animate={{ height: height || 'auto' }}
            transition={{ duration }}>
            <AnimatePresence initial={false}>
                <Box
                    // as={motion.div}
                    // key={JSON.stringify(children, ignoreCircularReferences())}
                    // initial={{ opacity: 0 }}
                    // animate={{ opacity: 1 }}
                    // exit={{ opacity: 0 }}
                    // transition={{ duration }}
                    width='100%'
                    position={height ? 'absolute' : 'relative'}>
                    <Box ref={ref} px='1'>
                        {children}
                    </Box>
                </Box>
            </AnimatePresence>
        </Box>
    )
}

/*
  Replacer function to JSON.stringify that ignores
  circular references and internal React properties.

  https://github.com/facebook/react/issues/8669#issuecomment-531515508
*/
// const ignoreCircularReferences = () => {
//     const seen = new WeakSet()
//     return (key: string, value: any) => {
//         if (key.startsWith('_')) return // Don't compare React's internal props.
//         if (typeof value === 'object' && value !== null) {
//             if (seen.has(value)) return
//             seen.add(value)
//         }
//         return value
//     }
// }

export default ResizablePanel
