import {
    IconButton,
    Menu as CMenu,
    MenuButton as CMenuButton,
    MenuItem as CMenuItem,
    MenuList as CMenuList,
} from '@chakra-ui/react'
import React from 'react'
import { BsThreeDotsVertical } from 'react-icons/bs'

function Item({ children, ...props }) {
    return (
        <CMenuItem p='8px' borderRadius='none' rounded='md' {...props}>
            {children}
        </CMenuItem>
    )
}

function Menu({ children, icon, placement, variant }) {
    return (
        <CMenu placement={placement}>
            <CMenuButton
                variant={variant}
                as={IconButton}
                icon={icon}
                size='lg'
                rounded='xl'
                border='1px solid'
                borderColor='gray.300'
            />
            <CMenuList
                bg='#fff'
                display='flex'
                flexDir='column'
                shadow='xl'
                px='10px'
                gap='1'>
                {children}
            </CMenuList>
        </CMenu>
    )
}

Menu.defaultProps = {
    children: <tr></tr>,
    icon: <BsThreeDotsVertical />,
    placement: 'left',
    variant: 'outline',
}

export default Object.assign(Menu, {
    Item,
})
