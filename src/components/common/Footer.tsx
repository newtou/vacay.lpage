import {
    Box,
    Divider,
    Grid,
    Heading,
    HStack,
    Icon,
    Image,
    Stack,
    Text,
    VStack,
} from '@chakra-ui/react'
import React from 'react'
import { FaWhatsapp, FaFacebook, FaInstagram, FaTwitter } from 'react-icons/fa'
import ScrollUp from './ScrollUp'

function Footer() {
    const contacts = [
        {
            label: 'Tsavo Road, South B',
            href: 'https://goo.gl/maps/nTDfSdaBrpSho1nn7',
        },
        {
            label: ' +254 753 003 769',
            href: 'tel:254753003769',
        },
        {
            label: 'info@vacay.co.ke',
            href: 'MAILTO:info@vacay.co.ke',
        },
    ]
    const resources = [
        {
            label: 'Reviews',
            href: 'https://vacay.co.ke/reviews/',
        },
        {
            label: 'Blog',
            href: 'https://vacay.co.ke/blog/',
        },
        {
            label: 'Privacy Policy',
            href: 'https://www.vacay.co.ke/vacay-holiday-deals-privacy-policy/',
        },
    ]

    const social = [
        {
            label: 'Instagram',
            icon: FaInstagram,
            href: 'https://www.instagram.com/vacayholidaydeals/?hl=en',
        },
        {
            label: 'Facebook',
            icon: FaFacebook,
            href: 'https://www.facebook.com/vacaydeals/',
        },
        {
            label: 'Twitter',
            icon: FaTwitter,
            href: 'https://twitter.com/Vacay_Deals',
        },
        {
            label: 'Whatsapp',
            icon: FaWhatsapp,
            href: 'https://wa.me/254716875656',
        },
    ]

    return (
        <Stack
            direction='column'
            minHeight='40vh'
            bg='brand.dark'
            alignItems='center'
            fontSize='md'
            p={['20px 10px', '', '', '', '40px']}
            color='gray.400'>
            <Grid
                width={['90%', '90%', '80%']}
                mx='auto'
                my='4rem'
                gap='1rem'
                position='relative'
                templateColumns={[
                    'repeat(2, 1fr)',
                    'repeat(2, 1fr)',
                    'repeat(2,1fr)',
                    'repeat(3, 1fr)',
                    'repeat(3, 1fr)',
                    'repeat(4,1fr)',
                ]}>
                <VStack alignItems='flex-start' spacing='5' minHeight='20vh'>
                    <Image
                        alt='vacay-logo'
                        src='/logo-transparent.png'
                        width={['50%', '60%', '60%', '50%']}
                    />
                </VStack>

                <Box minHeight='20vh'>
                    <Heading fontSize='xl' my='1rem'>
                        Contact
                    </Heading>
                    <VStack alignItems='flex-start' spacing='5'>
                        {contacts.map((item) => (
                            <Text
                                key={item.label}
                                _hover={{ color: 'brand.accent' }}
                                as='a'
                                href={item.href}
                                rel='noopener noreferrer'
                                target='_blank'>
                                {item.label}
                            </Text>
                        ))}
                    </VStack>
                </Box>
                <Box minHeight='20vh'>
                    <Heading fontSize='xl' my='1rem'>
                        Resources
                    </Heading>
                    <VStack alignItems='flex-start' spacing='5'>
                        {resources.map((item) => (
                            <Text
                                key={item.label}
                                as='a'
                                _hover={{ color: 'brand.accent' }}
                                href={item.href}
                                rel='noopener noreferrer'
                                target='_blank'>
                                {item.label}
                            </Text>
                        ))}
                    </VStack>
                </Box>

                <Box minHeight='20vh'>
                    <Heading fontSize='xl' my='1rem'>
                        Find us
                    </Heading>
                    <VStack alignItems='flex-start' spacing='4'>
                        {social.map((item) => (
                            <HStack
                                as='a'
                                key={item.label}
                                href={item.href}
                                _hover={{ color: 'brand.accent' }}
                                rel='noopener noreferrer'
                                target='_blank'>
                                <Icon as={item.icon} />
                                <Text as='span'>{item.label}</Text>
                            </HStack>
                        ))}
                    </VStack>
                </Box>
                <ScrollUp />
            </Grid>

            <Box width='full'>
                <Divider
                    width='80%'
                    mx='auto'
                    borderColor='gray.600'
                    my='1rem'
                />
                <Text textAlign='center' color='gray.400'>
                    All Rights Reserved &copy;{' '}
                    <Text
                        as='a'
                        target='_blank'
                        rel='noopener noreferrer'
                        href='https://vacay.co.ke'
                        _hover={{ color: 'brand.accent' }}>
                        Vacay Holiday Deals
                    </Text>{' '}
                    {new Date().getFullYear()}
                </Text>
            </Box>
        </Stack>
    )
}

export default Footer
