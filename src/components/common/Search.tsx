import React from 'react'
import {
    Box,
    FormControl,
    FormLabel,
    InputGroup,
    Input,
    Icon,
    InputLeftElement,
    InputRightElement,
    useColorMode,
} from '@chakra-ui/react'
import { HiOutlineSearch, HiOutlineX } from 'react-icons/hi'

function Search({
    setTerm,
    term,
    label,
    placeholder = 'search',
    full = false,
}) {
    const { colorMode } = useColorMode()
    return (
        <Box height='auto' width='auto'>
            <FormControl
                height='3rem'
                width={
                    full ? '100%' : ['100%', '100%', '90%', '40%', '60%', '60%']
                }>
                <FormLabel fontWeight='600' fontSize='1.2rem' color='gray.500'>
                    {label}
                </FormLabel>
                <InputGroup height='3rem'>
                    <InputLeftElement fontSize='1rem' height='100%'>
                        <Icon
                            as={HiOutlineSearch}
                            fontSize='1.3rem'
                            color='gray.600'
                            boxSize={5}
                            mt='0.2rem'
                        />
                    </InputLeftElement>
                    <Input
                        bg='gray.100'
                        border='none'
                        fontSize='lg'
                        rounded='10px'
                        as={Input}
                        p='10px 40px'
                        value={term}
                        width='100%'
                        color='gray.500'
                        borderColor={
                            colorMode === 'light' ? 'gray.300' : 'gray.700'
                        }
                        _active={{ borderColor: 'gray.200', outline: 'none' }}
                        _focus={{
                            border: '1px solid',
                            borderColor: 'gray.200',
                            outline: 'none',
                        }}
                        _focusVisible={{
                            outlineColor: 'gray.400',
                            outlineWidth: '1px',
                        }}
                        placeholder={placeholder}
                        onChange={(e) => setTerm(e.target.value)}
                        height='3rem'
                    />

                    {term && (
                        <InputRightElement
                            fontSize='1rem'
                            height='100%'
                            cursor='pointer'
                            onClick={() => setTerm('')}>
                            <Icon
                                as={HiOutlineX}
                                fontSize='1.3rem'
                                color='gray.600'
                            />
                        </InputRightElement>
                    )}
                </InputGroup>
            </FormControl>
        </Box>
    )
}

export default Search
