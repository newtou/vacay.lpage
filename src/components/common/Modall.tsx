import React from 'react'
import {
    Modal,
    ModalOverlay,
    ModalContent,
    ModalCloseButton,
    ModalBody,
    ModalHeader,
} from '@chakra-ui/react'
import PropTypes from 'prop-types'

function Modall({
    onClose,
    isOpen,
    size,
    title,
    children,
    overlayClick = true,
}) {
    return (
        <Modal
            onClose={onClose}
            isOpen={isOpen}
            size={size}
            isCentered
            scrollBehavior='inside'
            closeOnOverlayClick={overlayClick}
            motionPreset='scale'>
            <ModalOverlay />
            <ModalContent p='20px' rounded='2xl' className='modal' bg='white'>
                <ModalCloseButton
                    top='1rem'
                    right='1rem'
                    bg='gray.100'
                    size='lg'
                    rounded='full'
                    border="2px solid"
                    borderColor="gray.200"
                    _hover={{ bg: 'gray.200' }}
                    _focus={{ bg: 'gray.200', outline: 'none' }}
                    _active={{ bg: 'gray.200', outline: 'none' }}
                />
                <ModalHeader fontSize='xl'>{title}</ModalHeader>
                <ModalBody>{children}</ModalBody>
            </ModalContent>
        </Modal>
    )
}

Modall.defaultProps = {
    onClose: () => {},
    isOpen: false,
    size: 'md',
    title: '',
    overlayClick: true,
    children: <h3>Hello</h3>,
}

Modall.propTypes = {
    onClose: PropTypes.func,
    isOpen: PropTypes.bool,
    size: PropTypes.string,
    title: PropTypes.string,
    overlayClick: PropTypes.bool,
    children: PropTypes.element,
}

export default Modall
