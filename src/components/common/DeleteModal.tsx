import React from 'react'
import { Box, Button, Alert, Text, AlertIcon, HStack } from '@chakra-ui/react'
import Modall from './Modall'
import { useDisclosure } from '@chakra-ui/react-use-disclosure'

type DeleteModalProps = {
    delFunc: (id: string) => void // eslint-disable-line
    id: string
    loading: boolean
    renderTrigger: (onOpen: () => void) => React.ReactNode // eslint-disable-line
}

function DeleteModal({
    delFunc,
    id,
    loading,
    renderTrigger,
}: DeleteModalProps) {
    const { onClose, isOpen, onOpen } = useDisclosure()
    return (
        <>
            {renderTrigger(onOpen)}
            <Modall size='md' isOpen={isOpen} onClose={onClose} title='Delete'>
                <Box>
                    <Alert
                        status='error'
                        rounded='lg'
                        p='20px'
                        alignItems='flex-start'>
                        <AlertIcon />
                        <Text fontSize='lg'>
                            Are you sure you want to continue? This action can
                            not be undone.
                        </Text>
                    </Alert>

                    <Box
                        as={HStack}
                        mt='2rem'
                        alignItems='center'
                        justifyContent='space-between'>
                        <Button
                            height='2.5rem'
                            rounded='lg'
                            width='40%'
                            fontSize='lg'
                            colorScheme='red'
                            onClick={onClose}>
                            Cancel
                        </Button>
                        <Button
                            height='2.5rem'
                            rounded='lg'
                            width='40%'
                            fontSize='lg'
                            isDisabled={loading}
                            colorScheme='gray'
                            isLoading={loading}
                            onClick={() => {
                                delFunc(id)
                                onClose()
                            }}>
                            <Text as='span'>Proceed</Text>
                        </Button>
                    </Box>
                </Box>
            </Modall>
        </>
    )
}

DeleteModal.defaultProps = {
    loading: false,
    label: '',
}

export default DeleteModal
