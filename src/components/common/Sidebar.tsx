import Link from 'next/link'
import { useRouter } from 'next/router'
import { useSelector } from 'react-redux'
import { VStack, Box, Text } from '@chakra-ui/react'

import { RootState } from '@/src/store'

function Sidebar() {
    const router = useRouter()
    const { me } = useSelector((state: RootState) => state.auth)

    const authLinks = [
        {
            label: 'Offers',
            href: '/dashboard/offers',
            roles: [],
        },
        {
            label: 'Enquiries',
            href: '/dashboard/enquiries',
            roles: [],
        },
        {
            // TODO: New user functionality to be added
            label: 'Users',
            href: '/dashboard/users',
            roles: ['content', 'user'],
        },
    ]
    return (
        <VStack w='full' mt='2rem' alignItems='start' justifyContent='start'>
            {authLinks.map((link) => {
                const isActive = router.asPath.includes(link.href)
                return (
                    <Box
                        display={
                            link.roles.includes(me?.role) ? 'none' : 'block' // TODO: change this to cater for all users
                        }
                        w='75%'
                        cursor='pointer'
                        key={link.label}
                        shadow={isActive ? 'sm' : 'none'}
                        bg={isActive ? 'brand.primary.500' : 'transparent'}
                        color={isActive ? 'white' : 'gray.800'}
                        _hover={{
                            bg: isActive ? 'brand.primary.600' : 'gray.200',
                            borderColor: isActive
                                ? 'brand.primary.100'
                                : 'gray.200',
                        }}
                        p='15px'
                        rounded='full'>
                        <Link href={link.href} passHref>
                            <Text fontSize='xl' fontWeight='semibold'>
                                {link.label}
                            </Text>
                        </Link>
                    </Box>
                )
            })}
        </VStack>
    )
}
export default Sidebar
