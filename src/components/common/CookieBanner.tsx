import { useCookie } from '@/src/context/CookieProvider'
import { Button, HStack, Text } from '@chakra-ui/react'
import React from 'react'

function CookieBanner() {
    const { setCookieConsent } = useCookie()

    const cookieToLocalStorage = () => {
        setCookieConsent('cookie_consent')
    }
    return (
        <HStack
            position='fixed'
            height='auto'
            width={['95%', '', '50%', '40%', '30%']}
            rounded='10px'
            p='10px 20px'
            bg='white'
            shadow='2xl'
            zIndex='50'
            fontSize='lg'
            bottom={['1rem', '', '1rem', '1rem', '2rem']}
            left={['2.5%', '', '', '1rem', '2rem']}>
            <Text>
                We use cookies to improve your browsing experience.
                <Text
                    as='a'
                    ml='5px'
                    href='https://www.vacay.co.ke/vacay-holiday-deals-privacy-policy/'
                    target='_blank'
                    rel='noopener noreferrer'
                    cursor='pointer'
                    textDecoration='underline'
                    color='brand.base'>
                    Privacy Policy
                </Text>
            </Text>
            <HStack my='1rem' justifyContent='flex-start'>
                <Button
                    height='2rem'
                    bg='brand.dark'
                    color='brand.accent'
                    _hover={{ bg: 'brand.dark' }}
                    fontSize='lg'
                    fontWeight='400'
                    _focus={{ outline: 'none' }}
                    _active={{ outline: 'none' }}
                    onClick={cookieToLocalStorage}>
                    Accept
                </Button>
            </HStack>
        </HStack>
    )
}

export default CookieBanner
