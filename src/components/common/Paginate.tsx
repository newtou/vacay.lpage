import React from 'react'
import { Button, HStack } from '@chakra-ui/react'
import { ErrorBoundary } from 'react-error-boundary'
import ErrorHandler from './ErrorHandler'
import { HiOutlineChevronRight, HiOutlineChevronLeft } from 'react-icons/hi2'

type PaginateProps = {
    prev: () => void
    next: () => void
    pages: number[]
    currentPage: number
    prevLabel: string
    nextLabel: string
}

const Paginate = ({
    prev,
    next,
    pages,
    currentPage,
    prevLabel,
    nextLabel,
}: PaginateProps) => {
    return (
        <ErrorBoundary FallbackComponent={ErrorHandler}>
            <HStack spacing='3' my='1rem'>
                <Button
                    aria-label='Previous Page'
                    onClick={prev}
                    isDisabled={currentPage <= 1}
                    rounded='xl'
                    fontWeight='400'
                    display='flex'
                    alignItems='center'
                    bg='brand.base'
                    color='white'
                    gap='0.5rem'
                    _hover={{
                        bg: 'brand.base',
                        color: 'white',
                    }}
                    _active={{
                        bg: 'brand.base',
                        color: 'white',
                    }}
                    _focus={{
                        bg: 'brand.base',
                        color: 'white',
                    }}>
                    <HiOutlineChevronLeft size='15px' />
                    {prevLabel}
                </Button>
                <Button
                    aria-label='Current Page'
                    pointerEvents='none'
                    fontWeight='400'
                    border='1px solid'
                    rounded='xl'
                    borderColor='gray.200'>
                    {currentPage} of {pages.length}
                </Button>
                <Button
                    aria-label='Next Page'
                    onClick={next}
                    isDisabled={currentPage === pages.length}
                    rounded='xl'
                    fontWeight='400'
                    bg='brand.base'
                    color='white'
                    display='flex'
                    alignItems='center'
                    gap='0.5rem'
                    _hover={{
                        bg: 'brand.base',
                        color: 'white',
                    }}
                    _active={{
                        bg: 'brand.base',
                        color: 'white',
                    }}
                    _focus={{
                        bg: 'brand.primary.700',
                        color: 'white',
                    }}>
                    {nextLabel}
                    <HiOutlineChevronRight size='15px' />
                </Button>
            </HStack>
        </ErrorBoundary>
    )
}

Paginate.defaultProps = {
    prevLabel: 'Prev',
    nextLabel: 'Next',
    perPage: 10,
    items: [],
    setItems: () => {},
}

export default Paginate
