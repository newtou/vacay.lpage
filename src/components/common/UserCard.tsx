import {
    Avatar,
    Heading,
    HStack,
    Icon,
    Menu,
    MenuButton,
    MenuDivider,
    MenuItem,
    MenuList,
    Text,
} from '@chakra-ui/react'
import React from 'react'
import {
    HiOutlineLogout,
    HiOutlineUser,
    HiOutlineChevronDown,
} from 'react-icons/hi'
import { format } from 'date-fns'

import EditProfileDrawr from '../drawrs/profile/EditProfileDrawr'

function UserCard({ user, logout }) {
    return (
        <HStack
            spacing='3'
            border='2px solid'
            borderColor='gray.300'
            p='5px 15px'
            rounded='full'>
            <Menu closeOnBlur closeOnSelect placement='bottom'>
                <MenuButton cursor='pointer'>
                    <HStack spacing='3'>
                        <Avatar size='sm' name={user?.name || user?.email} />
                        <Text fontWeight='semibold' fontSize='md'>
                            {user?.name}
                        </Text>
                        <Icon as={HiOutlineChevronDown} size='25px' />
                    </HStack>
                </MenuButton>

                <MenuList
                    p='10px'
                    bg='white'
                    shadow='xl'
                    rounded='10px'
                    fontSize='md'>
                    <Heading fontSize='xl' px='0.5rem'>
                        {user?.email}
                    </Heading>
                    <Text
                        as='small'
                        px='0.5rem'
                        fontSize='sm'
                        color='gray.500'
                        mt='5px'>
                        Last login:{' '}
                        {user?.lastLogin
                            ? format(new Date(user?.lastLogin), 'PP')
                            : format(new Date(), 'PP')}
                    </Text>
                    <MenuDivider my='1rem' />
                    <EditProfileDrawr
                        renderTrigger={(onOpen) => (
                            <MenuItem
                                icon={<HiOutlineUser size='20px' />}
                                onClick={onOpen}
                                fontSize='lg'
                                rounded='5px'>
                                Profile
                            </MenuItem>
                        )}
                    />
                    <MenuItem
                        onClick={logout}
                        icon={<HiOutlineLogout size='20px' />}
                        fontSize='lg'
                        rounded='5px'>
                        Logout
                    </MenuItem>
                </MenuList>
            </Menu>
        </HStack>
    )
}

UserCard.defaultProps = {
    user: {},
    logout: () => {},
}

export default UserCard
