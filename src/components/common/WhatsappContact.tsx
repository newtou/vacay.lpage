import React from 'react'
import {
    Box,
    Text,
    Heading,
    HStack,
    VStack,
    Grid,
    Stack,
    Icon,
} from '@chakra-ui/react'
import { FaInfoCircle, FaWhatsapp } from 'react-icons/fa'

function WhatsappContact() {
    const contacts = [
        {
            name: 'Support',
            href: 'https://wa.me/254753003769',
            number: '+254 753 003 769',
        },
        {
            name: 'Support',
            href: 'https://wa.me/254716875656',
            number: '+254 716 875 656',
        },
    ]

    return (
        <Box>
            <HStack
                minH='15vh'
                gap='1rem'
                bg='whatsapp.600'
                py={['2rem', '2rem', '1rem']}>
                <VStack alignItems='center' color='white'>
                    <Heading as='h2' fontWeight='semibold' fontSize='3xl'>
                        Start a conversation
                    </Heading>
                    <Text
                        fontSize='lg'
                        textAlign='center'
                        w={['90%', '90%', '80%']}>
                        Hi there! reach out to our team on whatsapp using the
                        contacts below.
                    </Text>
                </VStack>
            </HStack>
            <VStack py={['20px', '', '30px']} px='10px' gap='3'>
                <HStack>
                    <Icon as={FaInfoCircle} />

                    <Text fontSize='lg' color='gray.500' textAlign='center'>
                        The team typically replies in a few minutes
                    </Text>
                </HStack>

                <Grid
                    gap='3'
                    width='100%'
                    px={['5px', '5px', '10px']}
                    templateColumns={[
                        'repeat(1, 1fr)',
                        'repeat(2,1fr)',
                        'repeat(2, 1fr)',
                    ]}>
                    {contacts.map((item) => (
                        <Stack
                            as='a'
                            direction={['column', 'column']}
                            target='_blank'
                            key={item.href}
                            href={item.href}
                            rel='noopener noreferrer'
                            minH='8vh'
                            justifyContent='center'
                            alignItems='center'
                            width='100%'
                            p='20px'
                            rounded='xl'
                            bg='gray.50'
                            transition='all 0.2s ease-in-out'
                            _hover={{
                                bg: 'gray.100',
                            }}
                            border='2px solid'
                            borderColor='gray.100'
                            mb='1rem'>
                            <Icon
                                as={FaWhatsapp}
                                color='whatsapp.500'
                                fontSize='2rem'
                            />
                            <VStack
                                width='100%'
                                spacing='2'
                                alignItems='center'
                                justifyContent='center'>
                                <Heading size='md'>{item.name}</Heading>
                                <Text fontSize='lg' color='gray.600'>
                                    {item.number}
                                </Text>
                            </VStack>
                        </Stack>
                    ))}
                </Grid>
            </VStack>
        </Box>
    )
}

export default WhatsappContact
