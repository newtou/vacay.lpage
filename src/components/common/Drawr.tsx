import React from 'react'
import {
    Drawer,
    DrawerCloseButton,
    DrawerBody,
    DrawerContent,
    DrawerHeader,
    DrawerOverlay,
} from '@chakra-ui/react'

type DrawrProps = {
    children?: React.ReactNode
    title: string
    isOpen: boolean
    onClose: () => void
    size?: 'xs' | 'sm' | 'md' | 'lg' | 'xl' | 'full'
    placement?: 'top' | 'right' | 'bottom' | 'left'
    flat?: boolean
}

function Drawr({
    title,
    isOpen,
    onClose,
    size,
    placement,
    flat,
    children,
}: DrawrProps) {
    const roundedStyle = {
        bottom: { borderTopLeftRadius: 'xl', borderTopRightRadius: 'xl' },
        top: { borderBottomLeftRadius: 'xl', borderBottomRightRadius: 'xl' },
        left: { borderTopRightRadius: 'xl', borderBottomRightRadius: 'xl' },
        right: { borderTopLeftRadius: ['xl'], borderBottomLeftRadius: 'xl' },
    }[placement]
    return (
        <Drawer
            isOpen={isOpen}
            placement={placement}
            onClose={onClose}
            size={size}>
            <DrawerOverlay />
            <DrawerContent {...roundedStyle}>
                <DrawerCloseButton
                    p='5px'
                    border='2px solid'
                    borderColor='gray.200'
                    rounded='full'
                    height='40px'
                    width='40px'
                    _hover={{
                        bg: 'gray.100',
                    }}
                />
                <DrawerHeader
                    fontWeight='bold'
                    borderBottom='1px solid'
                    fontSize='2xl'
                    borderColor='gray.100'>
                    {title}
                </DrawerHeader>
                <DrawerBody p={flat ? '0' : '2rem'}>{children}</DrawerBody>
            </DrawerContent>
        </Drawer>
    )
}

Drawr.defaultProps = {
    size: 'md',
    placement: 'right',
}

export default Drawr
