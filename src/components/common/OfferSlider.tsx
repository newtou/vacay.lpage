import React, { useEffect, useRef, useState } from 'react'
import Overflow from './Overflow'

type OfferSliderProps = {
    children: any
    showButtons?: boolean
}

function OfferSlider({ children, showButtons }: OfferSliderProps) {
    const sliderRef = useRef(null)
    const [disabledLeft, setDisabledLeft] = useState(false)
    const [disabledRight, setDisabledRight] = useState(false)

    const scrollNext = () => {
        setDisabledLeft(false)

        const blogs = sliderRef?.current.children
        const blogWidth = blogs[0].offsetWidth

        const scrollBy = blogWidth + 20
        const scrollLeft = sliderRef?.current.scrollLeft

        const newScrollLeft = scrollLeft + scrollBy

        sliderRef.current.scrollLeft = newScrollLeft
    }

    const scrollPrev = () => {
        setDisabledRight(false)

        const blogs = sliderRef.current.children
        const blogWidth = blogs[0].offsetWidth

        const scrollBy = blogWidth + 30
        const scrollLeft = sliderRef.current.scrollLeft

        const newScrollLeft = scrollLeft - scrollBy

        if (sliderRef.current.scrollLeft === 0) {
            setDisabledLeft(true)
        } else {
            sliderRef.current.scrollLeft = newScrollLeft
        }
    }

    useEffect(() => {
        if (sliderRef.current.scrollLeft === 0) {
            setDisabledLeft(true)
        }
    }, [sliderRef.current?.scrollLeft])

    return (
        <Overflow
            ref={sliderRef}
            showButtons={showButtons}
            next={scrollNext}
            prev={scrollPrev}
            leftDisabled={disabledLeft}
            rightDisabled={disabledRight}>
            {children}
        </Overflow>
    )
}

export default OfferSlider
