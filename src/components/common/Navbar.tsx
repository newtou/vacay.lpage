import { Box, HStack, IconButton, Text } from '@chakra-ui/react'
import { HiOutlinePhone, HiOutlineEnvelope } from 'react-icons/hi2'
import { useDispatch, useSelector } from 'react-redux'
import { signOut, useSession } from 'next-auth/react'

import CustomImage from './CustomImage'
import React from 'react'
import UserCard from './UserCard'
import { clearAuth } from '../../store/slices/Auth'
import { clearGallery } from '../../store/slices/Media'
import { clearOffers } from '../../store/slices/Offers'
import { clearUsers } from '../../store/slices/Users'
import useDimensions from 'react-cool-dimensions'
import { useRouter } from 'next/router'
import { AppDispatch, RootState } from '@/src/store'

function Navbar() {
    const router = useRouter()
    const dispatch = useDispatch<AppDispatch>()
    const { data: session, status } = useSession()
    const { width, height, observe } = useDimensions()
    const { me } = useSelector((state: RootState) => state.auth)

    const handleLogout = async () => {
        await signOut({
            redirect: true,
            callbackUrl: router?.pathname === '/' ? '/' : '/auth',
        })

        if (status === 'unauthenticated') {
            dispatch(clearOffers())
            dispatch(clearGallery())
            dispatch(clearUsers())
            dispatch(clearAuth())
        }
    }

    return (
        <Box
            height='8vh'
            bg='white'
            opacity={0.98}
            position='sticky'
            p={['10px', '10px', '', '10px 0']}
            width='100%'
            top='0'
            left='0'
            zIndex='100'>
            <HStack
                width={['100%', '95%', '90%', '90%', '75%']}
                mx='auto'
                height='100%'
                justifyContent='space-between'>
                <Box
                    as='a'
                    ref={observe}
                    height='100px'
                    width='100px'
                    cursor='pointer'
                    onClick={() => {
                        router.push('/')
                    }}>
                    <CustomImage
                        src='/logo-transparent.png'
                        height={height}
                        width={width}
                        alt='logo'
                        objectFit='contain'
                        layout='responsive'
                        priority
                    />
                </Box>
                <HStack spacing='8'>
                    <HStack spacing='3' display={['flex', 'flex']}>
                        {!session?.user ? (
                            <HStack spacing={['2', '', '5']}>
                                <HStack as='a' href='tel:254753003769'>
                                    <IconButton
                                        aria-label='Phone'
                                        size='md'
                                        icon={<HiOutlinePhone size='20px' />}
                                        color='brand.base'
                                        rounded='full'
                                    />
                                    <Text
                                        display={['none', 'none', 'block']}
                                        fontSize='md'>
                                        +254753003769
                                    </Text>
                                </HStack>
                                <HStack as='a' href='MAILTO:info@vacay.co.ke'>
                                    <IconButton
                                        aria-label='Email'
                                        size='md'
                                        rounded='full'
                                        icon={<HiOutlineEnvelope size='20px' />}
                                        color='brand.base'
                                    />
                                    <Text
                                        fontSize='md'
                                        display={['none', 'none', 'block']}>
                                        info@vacay.co.ke
                                    </Text>
                                </HStack>
                            </HStack>
                        ) : null}
                    </HStack>

                    {session?.user && me && (
                        <UserCard user={me} logout={handleLogout} />
                    )}
                </HStack>
            </HStack>
        </Box>
    )
}

export default Navbar
