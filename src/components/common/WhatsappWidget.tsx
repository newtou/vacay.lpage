import React from 'react'
import { Box, useDisclosure, IconButton, useMediaQuery } from '@chakra-ui/react'
import { FaWhatsapp, FaTimes } from 'react-icons/fa'
import { motion, AnimatePresence } from 'framer-motion'
import WhatsappContact from './WhatsappContact'
import WhatsappDrawr from '../drawrs/widgets/WhatsappDrawr'

function WhatsappWidget() {
    const { isOpen, onToggle, onClose } = useDisclosure()
    const [isDesktop] = useMediaQuery('(min-width: 768px)')

    return (
        <Box
            zIndex='100'
            width='auto'
            right={['1rem', '1rem', '2rem']}
            bottom={['1.5rem', '1rem', '2rem']}
            position='fixed'>
            {isDesktop ? (
                <Box position='relative'>
                    <AnimatePresence initial={false} mode='wait'>
                        {isOpen && (
                            <Box
                                position='absolute'
                                bg='white'
                                rounded='xl'
                                shadow='lg'
                                overflow='hidden'
                                mb='1rem'
                                right='0'
                                bottom='0'
                                width={['350px', '', '500px']}
                                as={motion.div}
                                initial={{ opacity: 0, x: 100 }}
                                animate={{ opacity: 1, x: 0 }}
                                exit={{ opacity: 0, x: 500 }}>
                                <WhatsappContact />
                            </Box>
                        )}
                    </AnimatePresence>
                </Box>
            ) : (
                <Box>
                    <WhatsappDrawr onClose={onClose} isOpen={isOpen} />
                </Box>
            )}
            <IconButton
                as={motion.button}
                whileHover={{ scale: 1.1 }}
                whileTap={{ scale: 0.9 }}
                aria-label='Whatsapp widget toggler'
                height='60px'
                width='60px'
                rounded='full'
                shadow='2xl'
                fontSize='3xl'
                icon={isOpen ? <FaTimes /> : <FaWhatsapp />}
                bg='whatsapp.600'
                color='whatsapp.50'
                _hover={{
                    bg: 'whatsapp.700',
                    color: 'whatsapp.50',
                }}
                onClick={onToggle}
            />
        </Box>
    )
}

export default WhatsappWidget
