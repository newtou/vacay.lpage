import Image from 'next/image'
import { chakra } from '@chakra-ui/react'

const ChakraImage = chakra(Image, {
    shouldForwardProp: (prop) =>
        [
            'src',
            'alt',
            'className',
            'width',
            'height',
            'sizes',
            'priority',
            'quality',
        ].includes(prop),
})

const CustomImage = ({ src, alt, width, height, ...props }) => {
    return (
        <ChakraImage
            src={src}
            alt={alt}
            width={width || 100}
            height={height || 100}
            sizes={width !== undefined ? `${Math.round(width)}px` : '100vw'}
            placeholder='blur'
            w='full'
            h='full'
            aspectRatio={1}
            {...props}
        />
    )
}

CustomImage.defaultProps = {
    width: 100,
    height: 100,
    quality: 75,
}

export default CustomImage
