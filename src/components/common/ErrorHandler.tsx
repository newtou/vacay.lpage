import { Alert, AlertIcon, Box, Heading, Text } from '@chakra-ui/react'
import React from 'react'

function ErrorHandler({ error }) {
    const isProduction = process.env.NODE_ENV === 'production'
    return (
        <Box my='1rem' width='100%'>
            <Heading size='lg' my='1rem'>
                Oops!! Something went wrong 😕
            </Heading>
            <Alert
                rounded='10px'
                status={isProduction ? 'info' : 'warning'}
                alignItems='flex-start'>
                <AlertIcon />
                <Text fontSize='lg'>
                    {isProduction ? "We're working on it" : error.message}
                </Text>
            </Alert>
        </Box>
    )
}

export default ErrorHandler
