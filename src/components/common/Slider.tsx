import { Button, Icon } from '@chakra-ui/react'
import React from 'react'
import Slider from 'react-slick'
import { FaArrowRight, FaArrowLeft } from 'react-icons/fa'

function ArrowLeft({ currentSlide, slideCount, slideSize, ...props }) {
    return (
        <Button
            {...props}
            cursor='pointer'
            rounded='full'
            bg={['blackAlpha.700', '', 'gray.800']}
            _hover={{ bg: ['blackAlpha.800', '', 'gray.900'], color: 'white' }}
            color='white'
            aria-label={`Previous-${slideCount}`}
            className={currentSlide === 0 ? 'disabled' : ''}
            left='1rem'
            height='40px'
            width='40px'
            p='10px'
            position='absolute'
            top={slideSize === 'sm' ? '35%' : '45%'}
            border='2px solid'
            borderColor='gray.600'
            zIndex='50'
            aria-hidden='true'
            aria-disabled={currentSlide === 0 ? 'true' : 'false'}>
            <Icon as={FaArrowLeft} color='white' />
        </Button>
    )
}
function ArrowRight({ currentSlide, slideCount, slideSize, ...props }) {
    return (
        <Button
            {...props}
            aria-label='Next'
            cursor='pointer'
            rounded='full'
            position='absolute'
            bg={['blackAlpha.700', '', 'gray.800']}
            border='2px solid'
            borderColor='gray.600'
            color='white'
            _hover={{
                bg: ['blackAlpha.800', '', 'gray.900'],
                color: 'white',
            }}
            className={currentSlide === slideCount - 1 ? 'disabled' : ''}
            top={slideSize === 'sm' ? '35%' : '45%'}
            right='1rem'
            height='40px'
            width='40px'
            p='10px'
            aria-hidden='true'
            aria-disabled={currentSlide === slideCount - 1 ? 'true' : 'false'}>
            <Icon as={FaArrowRight} color='white' />
        </Button>
    )
}

type SlidesProps = {
    children: any
    dots?: boolean
    infinite?: boolean
    speed?: number
    arrows?: boolean
    slidesToShow: number
    slidesToScroll: number
    autoplay?: boolean
    slideSize?: 'sm' | 'md' | 'lg'
    initialSlide?: number
}

function Slides({
    children,
    dots,
    infinite,
    speed,
    arrows,
    slidesToShow,
    slidesToScroll,
    autoplay,
    slideSize,
    initialSlide,
}: SlidesProps) {
    const settings = {
        dots,
        infinite,
        speed,
        arrows,
        autoplay,
        initialSlide,
        slidesToShow,
        slidesToScroll,
        prevArrow: (
            <ArrowLeft
                currentSlide={undefined}
                slideCount={undefined}
                slideSize={slideSize}
            />
        ),
        nextArrow: (
            <ArrowRight
                currentSlide={undefined}
                slideCount={undefined}
                slideSize={slideSize}
            />
        ),
    }
    return (
        <Slider {...settings} className='slider'>
            {children}
        </Slider>
    )
}

Slides.defaultProps = {
    slideSize: 'lg',
    initialSlide: 0,
}

export default Slides
