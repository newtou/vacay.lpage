import { Box, IconButton, Tooltip } from '@chakra-ui/react'
import { FaChevronUp } from 'react-icons/fa'

function Backtop() {
    const backToTop = () => {
        window.scrollTo(0, 0)
    }

    return (
        <Box
            display={['none', 'none', 'none', 'block']}
            position='absolute'
            bottom='1rem'
            right={['1rem', '', '', '3rem']}
            zIndex='20'>
            <Tooltip
                hasArrow
                rounded='md'
                label='Back to top'
                aria-label='back to top tooltip'>
                <IconButton
                    aria-label='Back to top'
                    rounded='full'
                    height='40px'
                    shadow='lg'
                    width='40px'
                    bg='white'
                    fontSize='md'
                    color='brand.base'
                    _hover={{ bg: 'whiteAlpha.900' }}
                    _focus={{
                        bg: 'whiteAlpha.900',
                        outline: 'none',
                    }}
                    _active={{
                        bg: 'whiteAlpha.900',
                        outline: 'none',
                    }}
                    icon={<FaChevronUp />}
                    onClick={backToTop}
                />
            </Tooltip>
        </Box>
    )
}

export default Backtop
