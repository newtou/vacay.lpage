import React from 'react'
import {
    Box,
    FormControl,
    FormLabel,
    InputGroup,
    Input,
    Icon,
    InputLeftElement,
    InputRightElement,
} from '@chakra-ui/react'
import { HiOutlineSearch, HiOutlineX } from 'react-icons/hi'
import PropTypes from 'prop-types'

function SearchBar({
    setTerm,
    term,
    label,
    placeholder = 'search',
    full = false,
}) {
    return (
        <Box height='auto'>
            <FormControl
                height='3rem'
                width={full ? '100%' : ['100%', '100%', '90%', '60%', '60%']}>
                {label ? (
                    <FormLabel
                        fontWeight='600'
                        fontSize='1.2rem'
                        color='gray.500'>
                        {label}
                    </FormLabel>
                ) : null}
                <InputGroup height='3rem'>
                    <InputLeftElement fontSize='1rem' height='100%'>
                        <Icon
                            as={HiOutlineSearch}
                            fontSize='1.3rem'
                            mt='0.2rem'
                            color='gray.600'
                        />
                    </InputLeftElement>
                    <Input
                        p='10px 40px'
                        rounded='10px'
                        value={term}
                        bg='gray.100'
                        color='gray.600'
                        fontSize='lg'
                        _active={{ outline: 'none' }}
                        _focus={{
                            outline: 'none',
                        }}
                        _focusVisible={{
                            outlineColor: 'gray.400',
                            outlineWidth: '1px',
                        }}
                        placeholder={placeholder}
                        onChange={(e) => setTerm(e.target.value)}
                        height='3rem'
                    />

                    {term && (
                        <InputRightElement
                            fontSize='1rem'
                            height='100%'
                            cursor='pointer'
                            onClick={() => setTerm('')}>
                            <Icon
                                as={HiOutlineX}
                                fontSize='1.2rem'
                                color='gray.600'
                            />
                        </InputRightElement>
                    )}
                </InputGroup>
            </FormControl>
        </Box>
    )
}

SearchBar.defaultProps = {
    setTerm: () => {},
    term: '',
    label: 'Search ...',
    placeholder: 'search',
    full: false,
}

SearchBar.propTypes = {
    setTerm: PropTypes.func,
    term: PropTypes.string,
    label: PropTypes.string,
    placeholder: PropTypes.string,
    full: PropTypes.bool,
}

export default SearchBar
