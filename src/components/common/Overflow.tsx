import { Box, Stack, forwardRef, IconButton } from '@chakra-ui/react'
import React from 'react'
import { FaArrowLeft, FaArrowRight } from 'react-icons/fa'

type OverflowProps = {
    children: any
    showButtons?: boolean
    next?: () => void
    prev?: () => void
    leftDisabled?: boolean
    rightDisabled?: boolean
}

const Overflow = forwardRef(
    (
        {
            children,
            showButtons,
            next,
            prev,
            leftDisabled,
            rightDisabled,
        }: OverflowProps,
        ref
    ) => {
        return (
            <Box as='section' my='1rem' position='relative'>
                <Box overflow='hidden'>
                    {showButtons && (
                        <IconButton
                            display={leftDisabled ? 'none' : 'flex'}
                            aria-label='previous button'
                            size={['sm', 'md', 'lg']}
                            position='absolute'
                            left={['1', '1', '-1.5rem']}
                            top={['30%', '', '40%']}
                            zIndex='20'
                            bg='gray.800'
                            _hover={{ bg: 'gray.900' }}
                            color='white'
                            shadow='lg'
                            rounded='full'
                            icon={<FaArrowLeft />}
                            onClick={prev}
                        />
                    )}

                    <Stack
                        ref={ref}
                        direction='row'
                        spacing='4'
                        rounded='lg'
                        justifyContent='flex-start'
                        overflowX='scroll'
                        css={{
                            '::-webkit-scrollbar': {
                                display: 'none',
                            },
                            scrollBehavior: 'smooth',
                        }}
                        alignItems='flex-start'>
                        {children}
                    </Stack>

                    {showButtons && (
                        <IconButton
                            display={rightDisabled ? 'none' : 'flex'}
                            aria-label='previous button'
                            size={['sm', 'md', 'lg']}
                            position='absolute'
                            rounded='full'
                            top={['30%', '40%']}
                            zIndex='20'
                            bg={['blackAlpha.700', '', 'gray.800']}
                            shadow='lg'
                            _hover={{ bg: ['blackAlpha.800', '', 'gray.900'] }}
                            right={['1', '1', '-1.5rem']}
                            color='white'
                            icon={<FaArrowRight />}
                            onClick={next}
                        />
                    )}
                </Box>
            </Box>
        )
    }
)

Overflow.defaultProps = {
    color: 'gray.200',
}

export default Overflow
