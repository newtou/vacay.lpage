import React from 'react'
import rehypeRaw from 'rehype-raw'
import gfm from 'remark-gfm'
import MarkDown from 'react-markdown'
import {
    Box,
    Text,
    UnorderedList,
    OrderedList,
    ListItem,
    Heading,
    Image,
    Table,
    // Tr,
    // Td,
    // Th,
    // Thead,
} from '@chakra-ui/react'

function MarkdownReader({ content }) {
    return (
        <MarkDown
            remarkPlugins={[gfm]}
            rehypePlugins={[rehypeRaw]}
            className='paragraph'
            components={{
                p: ({ children }) => {
                    return (
                        <Text
                            as='p'
                            my='1rem'
                            width='100%'
                            fontSize='lg'
                            color={'gray.700'}>
                            {children}
                        </Text>
                    )
                },
                a: (props) => {
                    return (
                        <Text
                            as='a'
                            href={props.href}
                            target='_blank'
                            fontStyle='italic'
                            color='brand.base'
                            rel='noreferrer'>
                            {props.children}
                        </Text>
                    )
                },
                img: (props) => {
                    return (
                        <Box my='2rem' height='25vh' width='auto'>
                            <Image
                                src={props.src}
                                alt={props.alt}
                                cursor='pointer'
                                width='auto'
                                height='100%'
                                objectFit='cover'
                                rounded='lg'
                                placeholder='blur'
                            />
                        </Box>
                    )
                },

                table: (props) => {
                    return (
                        <Table
                            my='1rem'
                            fontSize='lg'
                            variant='striped'
                            border='1px solid'
                            borderColor='gray.500'>
                            {props.children}
                        </Table>
                    )
                },

                // tr: (props) => (
                //     <Tr borderTop='1px solid' borderColor='gray.200'>
                //         {props.children}
                //     </Tr>
                // ),

                // td: (props) => (
                //     <Td p='1px' border='1px solid' borderColor='gray.200'>
                //         {props.children}
                //     </Td>
                // ),

                // th: (props) => (
                //     <Th p='5px' border='1px solid' fontFamily='ubuntu' borderColor='gray.200'>
                //         {props.children}
                //     </Th>
                // ),

                // thead: (props) => (
                //     <Thead fontFamily='ubuntu'>{props.children}</Thead>
                // ),

                ul({ children }) {
                    return (
                        <UnorderedList color='gray.700' fontSize='lg'>
                            {children}
                        </UnorderedList>
                    )
                },

                ol({ children }) {
                    return (
                        <OrderedList color='gray.700' fontSize='lg'>
                            {children}
                        </OrderedList>
                    )
                },
                li({ children }) {
                    return (
                        <ListItem color='gray.700' fontSize='lg'>
                            {children}
                        </ListItem>
                    )
                },
                h1: ({ children }) => (
                    <Heading
                        as='h1'
                        fontSize='2xl'
                        my='1rem'
                        textTransform='none'>
                        {children}
                    </Heading>
                ),
                h2: ({ children }) => (
                    <Heading
                        as='h2'
                        fontSize='xl'
                        my='1rem'
                        textTransform='none'>
                        {children}
                    </Heading>
                ),
                h3: ({ children }) => (
                    <Heading
                        as='h3'
                        fontSize='lg'
                        my='1rem'
                        textTransform='none'>
                        {children}
                    </Heading>
                ),
                h4: ({ children }) => (
                    <Heading
                        as='h4'
                        fontSize='md'
                        my='1rem'
                        textTransform='none'>
                        {children}
                    </Heading>
                ),
                h5: ({ children }) => (
                    <Heading
                        as='h5'
                        fontSize='lg'
                        my='1rem'
                        textTransform='none'>
                        {children}
                    </Heading>
                ),
                u: ({ children }) => (
                    <Text as='u' textDecoration='underline'>
                        {children}
                    </Text>
                ),

                span: ({ children }) => (
                    <Text as='span' fontSize='lg'>
                        {children}
                    </Text>
                ),
            }}>
            {content}
        </MarkDown>
    )
}

export default MarkdownReader
