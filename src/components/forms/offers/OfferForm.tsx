import React from 'react'
import {
    Button,
    FormControl,
    FormErrorIcon,
    FormErrorMessage,
    FormLabel,
    Input,
} from '@chakra-ui/react'
import EditorComponent from '@/src/components/common/TinyEditor'
import { Field } from 'formik'

type OfferFormProps = {
    submit: (e: React.FormEvent<HTMLFormElement>) => void // eslint-disable-line
    errors: { [key: string]: string }
    touched: { [key: string]: boolean }
    loading: boolean
    edit?: boolean
}

function OfferForm({ loading, errors, touched, submit, edit }: OfferFormProps) {
    return (
        <form onSubmit={submit}>
            <FormControl isInvalid={errors.name && touched.name} isRequired>
                <FormLabel htmlFor='name' fontSize='lg'>
                    Name
                </FormLabel>
                <Field
                    id='name'
                    as={Input}
                    bg='gray.100'
                    border='none'
                    fontSize='lg'
                    rounded='lg'
                    _active={{ borderColor: 'gray.200', outline: 'none' }}
                    _focus={{
                        border: '1px solid',
                        borderColor: 'gray.200',
                        outline: 'none',
                    }}
                    _focusVisible={{
                        outlineColor: 'gray.400',
                        outlineWidth: '1px',
                    }}
                    name='name'
                    height='3rem'
                    placeholder='Tour around Kenya'
                />

                <FormErrorMessage>
                    <FormErrorIcon />
                    {errors.name && touched.name && errors.name}
                </FormErrorMessage>
            </FormControl>

            <FormControl
                my='1rem'
                isInvalid={errors.locations && touched.locations}
                isRequired>
                <FormLabel htmlFor='locations' fontSize='lg'>
                    Locations(s)
                </FormLabel>
                <Field
                    id='locations'
                    as={Input}
                    bg='gray.100'
                    border='none'
                    fontSize='lg'
                    rounded='lg'
                    _active={{ borderColor: 'gray.200', outline: 'none' }}
                    _focus={{
                        border: '1px solid',
                        borderColor: 'gray.200',
                        outline: 'none',
                    }}
                    _focusVisible={{
                        outlineColor: 'gray.400',
                        outlineWidth: '1px',
                    }}
                    name='locations'
                    height='3rem'
                    placeholder='Maasai Mara, Diani etc'
                />

                <FormErrorMessage>
                    <FormErrorIcon />
                    {errors.locations && touched.locations && errors.locations}
                </FormErrorMessage>
            </FormControl>

            <FormControl
                my='1rem'
                isInvalid={errors.overview && touched.overview}
                isRequired>
                <FormLabel fontSize='lg'>Overview</FormLabel>
                <Field name='overview'>
                    {({ field }) => (
                        <EditorComponent
                            value={field.value}
                            onChange={field.onChange(field.name)}
                        />
                    )}
                </Field>
                <FormErrorMessage>
                    <FormErrorIcon />
                    {errors.overview && touched.overview && errors.overview}
                </FormErrorMessage>
            </FormControl>
            <FormControl
                isInvalid={errors.itinerary && touched.itinerary}
                isRequired>
                <FormLabel fontSize='lg'>Itinerary</FormLabel>
                <Field name='itinerary'>
                    {({ field }) => (
                        <EditorComponent
                            value={field.value}
                            onChange={field.onChange(field.name)}
                        />
                    )}
                </Field>
                <FormErrorMessage>
                    <FormErrorIcon />
                    {errors.itinerary && touched.itinerary && errors.itinerary}
                </FormErrorMessage>
            </FormControl>
            <FormControl my='1rem'>
                <FormLabel fontSize='lg'>Inclusion & Exclusions</FormLabel>
                <Field name='inclexcl'>
                    {({ field }) => (
                        <EditorComponent
                            value={field.value}
                            onChange={field.onChange(field.name)}
                        />
                    )}
                </Field>
            </FormControl>
            <FormControl>
                <FormLabel fontSize='lg'>Pricing</FormLabel>
                <Field name='pricing'>
                    {({ field }) => (
                        <EditorComponent
                            value={field.value}
                            onChange={field.onChange(field.name)}
                        />
                    )}
                </Field>
            </FormControl>
            <FormControl my='1rem'>
                <FormLabel fontSize='lg'>More Information</FormLabel>
                <Field name='moreInfo'>
                    {({ field }) => (
                        <EditorComponent
                            value={field.value}
                            onChange={field.onChange(field.name)}
                        />
                    )}
                </Field>
            </FormControl>
            <Button
                position='sticky'
                bottom='0'
                zIndex='sticky'
                my='1rem'
                color='white'
                type='submit'
                width='100%'
                height='3rem'
                fontSize='lg'
                rounded='lg'
                isLoading={loading}
                isDisabled={loading}
                bg='brand.base'
                _hover={{ bg: 'brand.primary.light' }}
                _focus={{ bg: 'brand.primary.light', outline: 'none' }}
                _active={{ bg: 'brand.primary.light', outline: 'none' }}>
                {edit ? 'Update' : 'Create New'} Offer &rarr;
            </Button>
        </form>
    )
}

OfferForm.defaultProps = {
    submit: () => {},
    loading: false,
    errors: {},
    touched: {},
}

export default OfferForm
