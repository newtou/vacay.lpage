import React from 'react'
import PropTypes from 'prop-types'
import {
    Box,
    Button,
    FormControl,
    FormErrorIcon,
    FormErrorMessage,
    FormLabel,
    Icon,
    Input,
    InputGroup,
    InputRightElement,
    useDisclosure,
} from '@chakra-ui/react'
import { HiOutlineLockClosed, HiOutlineLockOpen } from 'react-icons/hi'
import { Field } from 'formik'

function EditPasswordForm({ loading, errors, touched, submit }) {
    const { isOpen, onToggle } = useDisclosure()
    const { isOpen: confirmOpen, onToggle: confirmToggle } = useDisclosure()
    return (
        <Box as='form' onSubmit={submit}>
            <FormControl
                my='1rem'
                isInvalid={errors.password && touched.password}
                isRequired>
                <FormLabel htmlFor='pass' fontSize='lg'>
                    Password
                </FormLabel>
                <InputGroup>
                    <Field
                        id='pass'
                        type={isOpen ? 'text' : 'password'}
                        bg='gray.100'
                        border='none'
                        fontSize='lg'
                        rounded='10px'
                        _active={{ borderColor: 'gray.200', outline: 'none' }}
                        _focus={{
                            border: '1px solid',
                            borderColor: 'gray.200',
                            outline: 'none',
                        }}
                        _focusVisible={{
                            outlineColor: 'gray.400',
                            outlineWidth: '1px',
                        }}
                        as={Input}
                        name='password'
                        height='3rem'
                        placeholder='password'
                    />
                    <InputRightElement>
                        <Icon
                            cursor='pointer'
                            color='gray.600'
                            as={
                                isOpen ? HiOutlineLockOpen : HiOutlineLockClosed
                            }
                            onClick={onToggle}
                        />
                    </InputRightElement>
                </InputGroup>

                <FormErrorMessage>
                    <FormErrorIcon />
                    {errors.password && touched.password && errors.password}
                </FormErrorMessage>
            </FormControl>
            <FormControl
                my='1rem'
                isInvalid={errors.confirmpass && touched.confirmpass}
                isRequired>
                <FormLabel htmlFor='confirm' fontSize='lg'>
                    Confirm Password
                </FormLabel>
                <InputGroup>
                    <Field
                        id='confirm'
                        type={confirmOpen ? 'text' : 'password'}
                        bg='gray.100'
                        border='none'
                        fontSize='lg'
                        rounded='10px'
                        _active={{ borderColor: 'gray.200', outline: 'none' }}
                        _focus={{
                            border: '1px solid',
                            borderColor: 'gray.200',
                            outline: 'none',
                        }}
                        _focusVisible={{
                            outlineColor: 'gray.400',
                            outlineWidth: '1px',
                        }}
                        as={Input}
                        name='confirmpass'
                        height='3rem'
                        placeholder='confirm password'
                    />
                    <InputRightElement>
                        <Icon
                            cursor='pointer'
                            color='gray.600'
                            as={
                                confirmOpen
                                    ? HiOutlineLockOpen
                                    : HiOutlineLockClosed
                            }
                            onClick={confirmToggle}
                        />
                    </InputRightElement>
                </InputGroup>

                <FormErrorMessage>
                    <FormErrorIcon />
                    {errors.confirmpass &&
                        touched.confirmpass &&
                        errors.confirmpass}
                </FormErrorMessage>
            </FormControl>
            <Button
                my='1rem'
                color='white'
                type='submit'
                width='100%'
                height='3rem'
                fontSize='lg'
                rounded='10px'
                isLoading={loading}
                isDisabled={loading}
                bg='brand.base'
                _hover={{ bg: 'brand.primary.light' }}
                _focus={{ bg: 'brand.primary.light', outline: 'none' }}
                _active={{ bg: 'brand.primary.light', outline: 'none' }}>
                Update Password &rarr;
            </Button>
        </Box>
    )
}

EditPasswordForm.propTypes = {
    submit: PropTypes.func.isRequired,
    Field: PropTypes.func.isRequired,
    loading: PropTypes.bool.isRequired,
    errors: PropTypes.object.isRequired,
    touched: PropTypes.object.isRequired,
}
EditPasswordForm.defaultProps = {
    submit: () => {},
    Field: () => {},
    loading: false,
    errors: {},
    touched: {},
}

export default EditPasswordForm
