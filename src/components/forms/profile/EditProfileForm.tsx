import React from 'react'
import PropTypes from 'prop-types'
import {
    Box,
    Button,
    FormControl,
    FormErrorIcon,
    FormErrorMessage,
    FormLabel,
    Input,
} from '@chakra-ui/react'
import { Field } from 'formik'

function EditProfileForm({ loading, errors, touched, submit }) {
    return (
        <Box as='form' onSubmit={submit}>
            <FormControl isInvalid={errors.name && touched.name} isRequired>
                <FormLabel htmlFor='name' fontSize='lg'>
                    Name
                </FormLabel>
                <Field
                    id='user name'
                    as={Input}
                    bg='gray.100'
                    border='none'
                    fontSize='lg'
                    rounded='10px'
                    _active={{ borderColor: 'gray.200', outline: 'none' }}
                    _focus={{
                        border: '1px solid',
                        borderColor: 'gray.200',
                        outline: 'none',
                    }}
                    _focusVisible={{
                        outlineColor: 'gray.400',
                        outlineWidth: '1px',
                    }}
                    name='name'
                    height='3rem'
                    placeholder='jdoe@gmail.com'
                />

                <FormErrorMessage>
                    <FormErrorIcon />
                    {errors.name && touched.name && errors.name}
                </FormErrorMessage>
            </FormControl>
            <FormControl
                isInvalid={errors.email && touched.email}
                isRequired
                my='1rem'>
                <FormLabel htmlFor='email' fontSize='lg'>
                    Email
                </FormLabel>
                <Field
                    id='user email'
                    as={Input}
                    bg='gray.100'
                    border='none'
                    fontSize='lg'
                    rounded='10px'
                    _active={{ borderColor: 'gray.200', outline: 'none' }}
                    _focus={{
                        border: '1px solid',
                        borderColor: 'gray.200',
                        outline: 'none',
                    }}
                    _focusVisible={{
                        outlineColor: 'gray.400',
                        outlineWidth: '1px',
                    }}
                    name='email'
                    height='3rem'
                    placeholder='jdoe@gmail.com'
                />

                <FormErrorMessage>
                    <FormErrorIcon />
                    {errors.email && touched.email && errors.email}
                </FormErrorMessage>
            </FormControl>

            <Button
                my='1rem'
                color='white'
                type='submit'
                width='100%'
                fontSize='lg'
                height='3rem'
                rounded='10px'
                isLoading={loading}
                isDisabled={loading}
                bg='brand.base'
                _hover={{ bg: 'brand.primary.light' }}
                _focus={{ bg: 'brand.primary.light', outline: 'none' }}
                _active={{ bg: 'brand.primary.light', outline: 'none' }}>
                Update Profile &rarr;
            </Button>
        </Box>
    )
}

EditProfileForm.propTypes = {
    submit: PropTypes.func.isRequired,
    Field: PropTypes.func.isRequired,
    loading: PropTypes.bool.isRequired,
    errors: PropTypes.object.isRequired,
    touched: PropTypes.object.isRequired,
}
EditProfileForm.defaultProps = {
    submit: () => {},
    Field: () => {},
    loading: false,
    errors: {},
    touched: {},
}

export default EditProfileForm
