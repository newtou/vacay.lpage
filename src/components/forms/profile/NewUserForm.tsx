import React from 'react'
import PropTypes from 'prop-types'
import {
    Box,
    Button,
    FormControl,
    FormErrorIcon,
    FormErrorMessage,
    FormLabel,
    Icon,
    Select,
    Input,
    InputGroup,
    InputRightElement,
    useDisclosure,
} from '@chakra-ui/react'
import { HiOutlineLockClosed, HiOutlineLockOpen } from 'react-icons/hi'
import { Field } from 'formik'

function UserForm({ loading, errors, touched, submit, edit }) {
    const { isOpen, onToggle } = useDisclosure()
    const { isOpen: confirmOpen, onToggle: confirmToggle } = useDisclosure()
    return (
        <Box as='form' onSubmit={submit}>
            <FormControl isInvalid={errors.name && touched.name} isRequired>
                <FormLabel htmlFor='name' fontSize='lg'>
                    Name
                </FormLabel>
                <Field
                    id='new user name'
                    as={Input}
                    bg='gray.100'
                    border='none'
                    fontSize='lg'
                    rounded='10px'
                    _active={{ borderColor: 'gray.200', outline: 'none' }}
                    _focus={{
                        border: '1px solid',
                        borderColor: 'gray.200',
                        outline: 'none',
                    }}
                    _focusVisible={{
                        outlineColor: 'gray.400',
                        outlineWidth: '1px',
                    }}
                    name='name'
                    height='3rem'
                    placeholder='someone'
                />

                <FormErrorMessage>
                    <FormErrorIcon />
                    {errors.name && touched.name && errors.name}
                </FormErrorMessage>
            </FormControl>
            <FormControl
                isInvalid={errors.email && touched.email}
                isRequired
                my='1rem'>
                <FormLabel htmlFor='email' fontSize='lg'>
                    Email
                </FormLabel>
                <Field
                    id='new user email'
                    as={Input}
                    bg='gray.100'
                    border='none'
                    fontSize='lg'
                    rounded='10px'
                    _active={{ borderColor: 'gray.200', outline: 'none' }}
                    _focus={{
                        border: '1px solid',
                        borderColor: 'gray.200',
                        outline: 'none',
                    }}
                    _focusVisible={{
                        outlineColor: 'gray.400',
                        outlineWidth: '1px',
                    }}
                    name='email'
                    height='3rem'
                    placeholder='jdoe@gmail.com'
                />

                <FormErrorMessage>
                    <FormErrorIcon />
                    {errors.email && touched.email && errors.email}
                </FormErrorMessage>
            </FormControl>
            <FormControl
                isInvalid={errors.role && touched.role}
                isRequired
                my='1rem'>
                <FormLabel htmlFor='email' fontSize='lg'>
                    Role
                </FormLabel>
                <Field
                    id='role'
                    as={Select}
                    bg='gray.100'
                    border='none'
                    fontSize='lg'
                    rounded='10px'
                    _active={{ borderColor: 'gray.200', outline: 'none' }}
                    _focus={{
                        border: '1px solid',
                        borderColor: 'gray.200',
                        outline: 'none',
                    }}
                    _focusVisible={{
                        outlineColor: 'gray.400',
                        outlineWidth: '1px',
                    }}
                    name='role'
                    height='3rem'
                    placeholder='Select role'>
                    <option value='admin'>Admin</option>
                    <option value='user'>User</option>
                    <option value='content'>Content</option>
                </Field>

                <FormErrorMessage>
                    <FormErrorIcon />
                    {errors.role && touched.role && errors.role}
                </FormErrorMessage>
            </FormControl>

            {!edit && (
                <Box>
                    <FormControl
                        my='1rem'
                        isInvalid={errors.password && touched.password}
                        isRequired>
                        <FormLabel htmlFor='password' fontSize='lg'>
                            Password
                        </FormLabel>
                        <InputGroup>
                            <Field
                                id='new user password'
                                type={isOpen ? 'text' : 'password'}
                                bg='gray.100'
                                border='none'
                                fontSize='lg'
                                rounded='10px'
                                _active={{
                                    borderColor: 'gray.200',
                                    outline: 'none',
                                }}
                                _focus={{
                                    border: '1px solid',
                                    borderColor: 'gray.200',
                                    outline: 'none',
                                }}
                                _focusVisible={{
                                    outlineColor: 'gray.400',
                                    outlineWidth: '1px',
                                }}
                                as={Input}
                                name='password'
                                height='3rem'
                                placeholder='password'
                            />
                            <InputRightElement>
                                <Icon
                                    cursor='pointer'
                                    color='gray.600'
                                    as={
                                        isOpen
                                            ? HiOutlineLockOpen
                                            : HiOutlineLockClosed
                                    }
                                    onClick={onToggle}
                                />
                            </InputRightElement>
                        </InputGroup>

                        <FormErrorMessage>
                            <FormErrorIcon />
                            {errors.password &&
                                touched.password &&
                                errors.password}
                        </FormErrorMessage>
                    </FormControl>
                    <FormControl
                        my='1rem'
                        isInvalid={errors.confirmpass && touched.confirmpass}
                        isRequired>
                        <FormLabel htmlFor='confirmpass' fontSize='lg'>
                            Confirm Password
                        </FormLabel>
                        <InputGroup>
                            <Field
                                id='new user confirmpass'
                                type={confirmOpen ? 'text' : 'password'}
                                bg='gray.100'
                                border='none'
                                fontSize='lg'
                                rounded='10px'
                                _active={{
                                    borderColor: 'gray.200',
                                    outline: 'none',
                                }}
                                _focus={{
                                    border: '1px solid',
                                    borderColor: 'gray.200',
                                    outline: 'none',
                                }}
                                _focusVisible={{
                                    outlineColor: 'gray.400',
                                    outlineWidth: '1px',
                                }}
                                as={Input}
                                name='confirmpass'
                                height='3rem'
                                placeholder='confirm password'
                            />
                            <InputRightElement>
                                <Icon
                                    cursor='pointer'
                                    color='gray.600'
                                    as={
                                        confirmOpen
                                            ? HiOutlineLockOpen
                                            : HiOutlineLockClosed
                                    }
                                    onClick={confirmToggle}
                                />
                            </InputRightElement>
                        </InputGroup>

                        <FormErrorMessage>
                            <FormErrorIcon />
                            {errors.confirmpass &&
                                touched.confirmpass &&
                                errors.confirmpass}
                        </FormErrorMessage>
                    </FormControl>
                </Box>
            )}

            <Button
                my='1rem'
                color='white'
                type='submit'
                width='100%'
                fontSize='lg'
                height='3rem'
                rounded='10px'
                isLoading={loading}
                isDisabled={loading}
                bg='brand.base'
                _hover={{ bg: 'brand.primary.light' }}
                _focus={{ bg: 'brand.primary.light', outline: 'none' }}
                _active={{ bg: 'brand.primary.light', outline: 'none' }}>
                {edit ? 'Update User' : 'Save User'} &rarr;
            </Button>
        </Box>
    )
}

UserForm.propTypes = {
    submit: PropTypes.func.isRequired,
    Field: PropTypes.func.isRequired,
    loading: PropTypes.bool.isRequired,
    errors: PropTypes.object.isRequired,
    touched: PropTypes.object.isRequired,
}
UserForm.defaultProps = {
    submit: () => {},
    Field: () => {},
    loading: false,
    errors: {},
    touched: {},
    edit: false,
}

export default UserForm
