import React from 'react'
import {
    Button,
    FormControl,
    FormErrorIcon,
    FormErrorMessage,
    FormLabel,
    Input,
    Textarea,
} from '@chakra-ui/react'
import { Field } from 'formik'

type SeoFormProps = {
    submit: (e: React.FormEvent<HTMLFormElement>) => void // eslint-disable-line
    errors: { [key: string]: string }
    touched: { [key: string]: boolean }
    loading: boolean
}

function SeoForm({ loading, errors, touched, submit }: SeoFormProps) {
    return (
        <form onSubmit={submit}>
            <FormControl isInvalid={errors.title && touched.title} isRequired>
                <FormLabel htmlFor='seo-title' fontSize='lg'>
                    Seo Title
                </FormLabel>
                <Field
                    id='seo-title'
                    as={Input}
                    bg='gray.100'
                    border='none'
                    fontSize='lg'
                    rounded='lg'
                    _active={{ borderColor: 'gray.200', outline: 'none' }}
                    _focus={{
                        border: '1px solid',
                        borderColor: 'gray.200',
                        outline: 'none',
                    }}
                    _focusVisible={{
                        outlineColor: 'gray.400',
                        outlineWidth: '1px',
                    }}
                    name='title'
                    height='3rem'
                    placeholder='Tour around Kenya'
                />

                <FormErrorMessage>
                    <FormErrorIcon />
                    {errors.title && touched.title && errors.title}
                </FormErrorMessage>
            </FormControl>

            <FormControl
                my='1rem'
                isInvalid={errors.keywords && touched.keywords}>
                <FormLabel htmlFor='seo-keywords' fontSize='lg'>
                    Seo Keywords
                </FormLabel>
                <Field
                    id='seo-keywords'
                    as={Textarea}
                    bg='gray.100'
                    border='none'
                    fontSize='lg'
                    rounded='lg'
                    _active={{ borderColor: 'gray.200', outline: 'none' }}
                    _focus={{
                        border: '1px solid',
                        borderColor: 'gray.200',
                        outline: 'none',
                    }}
                    _focusVisible={{
                        outlineColor: 'gray.400',
                        outlineWidth: '1px',
                    }}
                    name='keywords'
                    height='10vh'
                    placeholder='Maasai Mara Tour, Diani Holiday etc'
                />

                <FormErrorMessage>
                    <FormErrorIcon />
                    {errors.keywords && touched.keywords && errors.keywords}
                </FormErrorMessage>
            </FormControl>
            <FormControl isInvalid={errors.description && touched.description}>
                <FormLabel htmlFor='seo-description' fontSize='lg'>
                    Seo Description
                </FormLabel>
                <Field
                    id='seo-description'
                    as={Textarea}
                    bg='gray.100'
                    border='none'
                    fontSize='lg'
                    rounded='lg'
                    _active={{ borderColor: 'gray.200', outline: 'none' }}
                    _focus={{
                        border: '1px solid',
                        borderColor: 'gray.200',
                        outline: 'none',
                    }}
                    _focusVisible={{
                        outlineColor: 'gray.400',
                        outlineWidth: '1px',
                    }}
                    name='description'
                    height='10vh'
                    placeholder='Add a short description of the offer'
                />

                <FormErrorMessage>
                    <FormErrorIcon />
                    {errors.description &&
                        touched.description &&
                        errors.description}
                </FormErrorMessage>
            </FormControl>

            <Button
                position='sticky'
                bottom='0'
                zIndex='sticky'
                my='1rem'
                color='white'
                type='submit'
                width='100%'
                height='3rem'
                fontSize='lg'
                rounded='lg'
                isLoading={loading}
                isDisabled={loading}
                bg='brand.base'
                _hover={{ bg: 'brand.primary.light' }}
                _focus={{ bg: 'brand.primary.light', outline: 'none' }}
                _active={{ bg: 'brand.primary.light', outline: 'none' }}>
                Save Changes &rarr;
            </Button>
        </form>
    )
}

SeoForm.defaultProps = {
    submit: () => {},
    loading: false,
    errors: {},
    touched: {},
}

export default SeoForm
