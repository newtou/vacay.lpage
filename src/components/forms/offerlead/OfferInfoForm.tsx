import {
    Box,
    FormControl,
    FormLabel,
    FormErrorMessage,
    FormErrorIcon,
    Input,
    Textarea,
    Select,
    Grid,
} from '@chakra-ui/react'
import React from 'react'
import { Field } from 'formik'

function OfferInfoForm({ errors, touched, destinations }) {
    return (
        <Box>
            <FormControl
                isInvalid={errors.destination && touched.destination}
                isRequired>
                <FormLabel htmlFor='destination'>Destination</FormLabel>
                <Field
                    id='destination'
                    as={Select}
                    bg='gray.100'
                    border='none'
                    rounded='lg'
                    _active={{ borderColor: 'gray.200', outline: 'none' }}
                    _focus={{
                        border: '1px solid',
                        borderColor: 'gray.200',
                        outline: 'none',
                    }}
                    _focusVisible={{
                        outlineColor: 'gray.400',
                        outlineWidth: '1px',
                    }}
                    name='destination'
                    height='3rem'
                    placeholder='Select destination'>
                    {destinations &&
                        destinations.map((destination) => (
                            <option key={destination} value={destination}>
                                {destination}
                            </option>
                        ))}
                </Field>

                <FormErrorMessage>
                    <FormErrorIcon />
                    {errors.destination &&
                        touched.destination &&
                        errors.destination}
                </FormErrorMessage>
            </FormControl>
            <FormControl
                isInvalid={errors.departure && touched.departure}
                isRequired
                my='1rem'>
                <FormLabel htmlFor='departure'>Departure</FormLabel>
                <Field
                    id='departure'
                    as={Input}
                    type='date'
                    min={new Date().toISOString().split('T')[0]}
                    bg='gray.100'
                    border='none'
                    rounded='lg'
                    _active={{ borderColor: 'gray.200', outline: 'none' }}
                    _focus={{
                        border: '1px solid',
                        borderColor: 'gray.200',
                        outline: 'none',
                    }}
                    _focusVisible={{
                        outlineColor: 'gray.400',
                        outlineWidth: '1px',
                    }}
                    name='departure'
                    _placeholder={{ fontSize: 'sm' }}
                    height='3rem'
                    placeholder='4'
                />

                <FormErrorMessage>
                    <FormErrorIcon />
                    {errors.departure && touched.departure && errors.departure}
                </FormErrorMessage>
            </FormControl>
            <FormControl isInvalid={errors.budget && touched.budget} isRequired>
                <FormLabel htmlFor='budget'>Budget</FormLabel>
                <Field
                    id='budget'
                    as={Select}
                    bg='gray.100'
                    border='none'
                    rounded='lg'
                    _active={{ borderColor: 'gray.200', outline: 'none' }}
                    _focus={{
                        border: '1px solid',
                        borderColor: 'gray.200',
                        outline: 'none',
                    }}
                    _focusVisible={{
                        outlineColor: 'gray.400',
                        outlineWidth: '1px',
                    }}
                    name='budget'
                    height='3rem'
                    placeholder='Select your budget'>
                    {['3 Star', '4 Star', '5 Star'].map((budget) => (
                        <option key={budget} value={budget}>
                            {budget}
                        </option>
                    ))}
                </Field>

                <FormErrorMessage>
                    <FormErrorIcon />
                    {errors.budget && touched.budget && errors.budget}
                </FormErrorMessage>
            </FormControl>
            <Grid gap='1rem' templateColumns='repeat(2,1fr)' my='0.5rem'>
                <FormControl isInvalid={errors.adult && touched.adult}>
                    <FormLabel htmlFor='adult'>Adult</FormLabel>
                    <Field
                        id='adults'
                        as={Input}
                        type='number'
                        bg='gray.100'
                        border='none'
                        rounded='lg'
                        _active={{ borderColor: 'gray.200', outline: 'none' }}
                        _focus={{
                            border: '1px solid',
                            borderColor: 'gray.200',
                            outline: 'none',
                        }}
                        _focusVisible={{
                            outlineColor: 'gray.400',
                            outlineWidth: '1px',
                        }}
                        name='adult'
                        height='3rem'
                        placeholder='2'
                    />

                    <FormErrorMessage>
                        <FormErrorIcon />
                        {errors.adult && touched.adults && errors.adult}
                    </FormErrorMessage>
                </FormControl>
                <FormControl isInvalid={errors.child && touched.child}>
                    <FormLabel htmlFor='child'>Child</FormLabel>
                    <Field
                        id='child'
                        as={Input}
                        type='number'
                        bg='gray.100'
                        border='none'
                        rounded='lg'
                        _active={{ borderColor: 'gray.200', outline: 'none' }}
                        _focus={{
                            border: '1px solid',
                            borderColor: 'gray.200',
                            outline: 'none',
                        }}
                        _focusVisible={{
                            outlineColor: 'gray.400',
                            outlineWidth: '1px',
                        }}
                        name='child'
                        height='3rem'
                        placeholder='4'
                    />

                    <FormErrorMessage>
                        <FormErrorIcon />
                        {errors.child && touched.child && errors.child}
                    </FormErrorMessage>
                </FormControl>
            </Grid>

            <FormControl
                isInvalid={errors.message && touched.message}
                my='0.5rem'>
                <FormLabel htmlFor='message'>Message</FormLabel>
                <Field
                    id='name'
                    as={Textarea}
                    bg='gray.100'
                    border='none'
                    rounded='lg'
                    _active={{ borderColor: 'gray.200', outline: 'none' }}
                    _focus={{
                        border: '1px solid',
                        borderColor: 'gray.200',
                        outline: 'none',
                    }}
                    _focusVisible={{
                        outlineColor: 'gray.400',
                        outlineWidth: '1px',
                    }}
                    name='message'
                    height='20vh'
                    placeholder='Any other information you want to share with us'
                />

                <FormErrorMessage>
                    <FormErrorIcon />
                    {errors.email && touched.email && errors.email}
                </FormErrorMessage>
            </FormControl>
        </Box>
    )
}

export default OfferInfoForm
