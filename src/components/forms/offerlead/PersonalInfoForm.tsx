import {
    Box,
    FormControl,
    FormLabel,
    FormErrorMessage,
    FormErrorIcon,
    Input,
} from '@chakra-ui/react'
import React from 'react'
import { Field } from 'formik'

function PersonalInfoForm({ errors, touched }) {
    return (
        <Box>
            <FormControl isInvalid={errors.name && touched.name} isRequired>
                <FormLabel htmlFor='name' fontSize='md'>
                    Full Name
                </FormLabel>
                <Field
                    id='name'
                    as={Input}
                    bg='gray.100'
                    border='none'
                    fontSize='md'
                    rounded='lg'
                    _active={{ borderColor: 'gray.200', outline: 'none' }}
                    _focus={{
                        border: '1px solid',
                        borderColor: 'gray.200',
                        outline: 'none',
                    }}
                    _focusVisible={{
                        outlineColor: 'gray.400',
                        outlineWidth: '1px',
                    }}
                    name='name'
                    height='3rem'
                    placeholder='Someone'
                    type='text'
                />

                <FormErrorMessage>
                    <FormErrorIcon />
                    {errors.name && touched.name && errors.name}
                </FormErrorMessage>
            </FormControl>
            <FormControl
                isInvalid={errors.email && touched.email}
                isRequired
                my='1rem'>
                <FormLabel htmlFor='email' fontSize='md'>
                    Email
                </FormLabel>
                <Field
                    id='name'
                    as={Input}
                    bg='gray.100'
                    border='none'
                    fontSize='md'
                    rounded='lg'
                    _active={{ borderColor: 'gray.200', outline: 'none' }}
                    _focus={{
                        border: '1px solid',
                        borderColor: 'gray.200',
                        outline: 'none',
                    }}
                    _focusVisible={{
                        outlineColor: 'gray.400',
                        outlineWidth: '1px',
                    }}
                    name='email'
                    height='3rem'
                    placeholder='someone@mail.com'
                    type='email'
                />

                <FormErrorMessage>
                    <FormErrorIcon />
                    {errors.email && touched.email && errors.email}
                </FormErrorMessage>
            </FormControl>
            <FormControl isInvalid={errors.number && touched.number}>
                <FormLabel htmlFor='number' fontSize='md'>
                    Number
                </FormLabel>
                <Field
                    id='number'
                    as={Input}
                    type='text'
                    bg='gray.100'
                    border='none'
                    fontSize='md'
                    rounded='lg'
                    _active={{ borderColor: 'gray.200', outline: 'none' }}
                    _focus={{
                        border: '1px solid',
                        borderColor: 'gray.200',
                        outline: 'none',
                    }}
                    _focusVisible={{
                        outlineColor: 'gray.400',
                        outlineWidth: '1px',
                    }}
                    name='number'
                    height='3rem'
                    placeholder='+254 7xx xxx xxx'
                />

                <FormErrorMessage>
                    <FormErrorIcon />
                    {errors.number && touched.number && errors.number}
                </FormErrorMessage>
            </FormControl>
        </Box>
    )
}

export default PersonalInfoForm
