import React from 'react'
import {
    Box,
    Button,
    FormControl,
    FormErrorIcon,
    FormErrorMessage,
    FormLabel,
    Icon,
    Input,
    InputGroup,
    InputRightElement,
    useDisclosure,
} from '@chakra-ui/react'
import { HiOutlineLockClosed, HiOutlineLockOpen } from 'react-icons/hi'
import { Field } from 'formik'

type LoginFormProps = {
    loading: boolean
    errors: any
    touched: any
    submit: (e: React.FormEvent) => void // eslint-disable-line
}

function LoginForm({ errors, touched, submit }: LoginFormProps) {
    const { isOpen, onToggle } = useDisclosure()
    return (
        <Box as='form' onSubmit={submit}>
            <FormControl isInvalid={errors.email && touched.email} isRequired>
                <FormLabel htmlFor='email'>Email</FormLabel>
                <Field
                    id='email'
                    as={Input}
                    bg='gray.100'
                    border='none'
                    fontSize='lg'
                    rounded='lg'
                    _active={{ borderColor: 'gray.200', outline: 'none' }}
                    _focus={{
                        border: '1px solid',
                        borderColor: 'gray.200',
                        outline: 'none',
                    }}
                    _focusVisible={{
                        outlineColor: 'gray.400',
                        outlineWidth: '1px',
                    }}
                    name='email'
                    height='3rem'
                    placeholder='jdoe@gmail.com'
                />

                <FormErrorMessage>
                    <FormErrorIcon />
                    {errors.email && touched.email && errors.email}
                </FormErrorMessage>
            </FormControl>
            <FormControl
                my='1rem'
                isInvalid={errors.password && touched.password}
                isRequired>
                <FormLabel htmlFor='password'>Password</FormLabel>
                <InputGroup>
                    <Field
                        id='password'
                        type={isOpen ? 'text' : 'password'}
                        bg='gray.100'
                        border='none'
                        fontSize='lg'
                        rounded='lg'
                        _active={{ borderColor: 'gray.200', outline: 'none' }}
                        _focus={{
                            border: '1px solid',
                            borderColor: 'gray.200',
                            outline: 'none',
                        }}
                        _focusVisible={{
                            outlineColor: 'gray.400',
                            outlineWidth: '1px',
                        }}
                        as={Input}
                        name='password'
                        height='3rem'
                        placeholder='jdoe@password'
                    />
                    <InputRightElement>
                        <Icon
                            cursor='pointer'
                            color='gray.600'
                            as={
                                isOpen ? HiOutlineLockOpen : HiOutlineLockClosed
                            }
                            onClick={onToggle}
                            boxSize={6}
                        />
                    </InputRightElement>
                </InputGroup>

                <FormErrorMessage>
                    <FormErrorIcon />
                    {errors.password && touched.password && errors.password}
                </FormErrorMessage>
            </FormControl>
            <Button type='submit' hidden />
        </Box>
    )
}

LoginForm.defaultProps = {
    submit: () => {},
    loading: false,
    errors: {},
    touched: {},
}

export default LoginForm
