import React from 'react'
import {
    Box,
    FormControl,
    Icon,
    Input,
    InputGroup,
    InputLeftElement,
} from '@chakra-ui/react'
import { HiOutlineMapPin } from 'react-icons/hi2'
import { Field } from 'formik'
import Button from '../ui/atoms/Button'

type SearchFormProps = {
    submit: (e: React.FormEvent) => void | Promise<void> // eslint-disable-line
    loading: boolean
}

function SearchForm({ loading, submit }: SearchFormProps) {
    return (
        <Box as='form' onSubmit={submit}>
            <FormControl>
                <InputGroup id='Destination-input-group'>
                    <InputLeftElement>
                        <Icon
                            as={HiOutlineMapPin}
                            color='gray.500'
                            mt='0.5rem'
                            boxSize={5}
                        />
                    </InputLeftElement>
                    <Field
                        id='destination'
                        as={Input}
                        bg='gray.100'
                        border='none'
                        fontSize='lg'
                        pl='40px'
                        rounded='lg'
                        _active={{ borderColor: 'gray.200', outline: 'none' }}
                        _focus={{
                            border: 'none',
                            borderColor: 'gray.200',
                            outline: 'none',
                        }}
                        _focusVisible={{
                            outlineColor: 'gray.400',
                            outlineWidth: '1px',
                        }}
                        name='destination'
                        height='3rem'
                        placeholder='destination'
                    />
                </InputGroup>
            </FormControl>

            <Button
                my='1rem'
                color='white'
                type='submit'
                width='100%'
                height='3rem'
                fontSize='lg'
                rounded='lg'
                isLoading={loading}
                isDisabled={loading}>
                Explore
            </Button>
        </Box>
    )
}

SearchForm.defaultProps = {
    submit: () => {},
    loading: false,
}

export default SearchForm
