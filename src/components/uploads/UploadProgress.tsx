import React, { useEffect } from 'react'
import { CircularProgress, Progress, Text, VStack } from '@chakra-ui/react'
import { useUploads } from '../../hooks/useUpload'
import { setError } from '../../store/slices/Alert'
import { useDispatch } from 'react-redux'
import { AppDispatch } from '@/src/store'

type UploadsProgressProps = {
    files: string
    setFiles: (files: string[]) => void // eslint-disable-line
    updateGallery: () => void // eslint-disable-line
    map_id: string
}

function UploadsProgress({
    files,
    setFiles,
    updateGallery,
    map_id,
}: UploadsProgressProps) {
    const dispatch = useDispatch<AppDispatch>()
    const { progress, error } = useUploads(
        files,
        setFiles,
        map_id,
        updateGallery
    )

    useEffect(() => {
        if (error) {
            dispatch(setError(error))
        }
    }, [error, dispatch, updateGallery])

    return (
        <VStack spacing='3'>
            <Text fontSize='lg'>Uploading media ...</Text>
            {progress ? (
                <Progress
                    value={progress}
                    hasStripe
                    colorScheme='blue'
                    rounded='full'
                    size='md'
                    w='full'
                />
            ) : (
                <CircularProgress
                    isIndeterminate
                    size='50px'
                    thickness='14px'
                    color='brand.base'
                />
            )}
        </VStack>
    )
}

export default UploadsProgress
