import {
    Box,
    useDisclosure,
    FormControl,
    FormLabel,
    Radio,
    RadioGroup,
    Image as Img,
    Stack,
    Grid,
} from '@chakra-ui/react'
import React, { useCallback, useEffect, useState } from 'react'
import Drawr from '../../common/Drawr'
import { Formik } from 'formik'
import * as Yup from 'yup'
import { useDispatch, useSelector } from 'react-redux'
import { createSeo, updateSeo, fetchSeo } from '@/src/store/slices/Seo'
import SeoForm from '../../forms/seo/SeoForm'
import { Image } from '@/types/index'
import { AppDispatch, RootState } from '@/src/store'

type SeoDrawrProps = {
    mapId: string
    mapTitle?: string
    defaultOptions?: Image[]
    renderTrigger: (onOpen: () => void) => React.ReactNode // eslint-disable-line
}

type InitialValues = {
    title: string
    keywords: string
    description: string
    imageUrl: string
}

function SeoDrawr({
    renderTrigger,
    mapId,
    mapTitle,
    defaultOptions,
}: SeoDrawrProps) {
    const dispatch = useDispatch<AppDispatch>()
    const { isOpen, onOpen, onClose } = useDisclosure()
    const { seo } = useSelector((state: RootState) => state.seo)
    const [imageOptions, setImageOptions] = useState<Image[]>([])
    const [mappedId, setMappedId] = useState<string | null>(null)

    const initialValues: InitialValues = {
        title: seo?.title || mapTitle || '',
        keywords: seo?.keywords || '',
        description: seo?.description || '',
        imageUrl: seo?.imageUrl || imageOptions[0]?.url || '',
    }

    const validationSchema = Yup.object().shape({
        title: Yup.string().required('Title is required'),
        keywords: Yup.string(),
        description: Yup.string(),
    })

    const getMappedSeo = useCallback(() => {
        if (!mappedId) return
        dispatch(fetchSeo(mappedId))
    }, [dispatch, mappedId])

    useEffect(() => {
        if (isOpen && mapId) {
            setMappedId(mapId)
            getMappedSeo()
        }

        if (isOpen && defaultOptions) {
            setImageOptions(defaultOptions)
        }
    }, [mapId, getMappedSeo, defaultOptions, isOpen])

    return (
        <>
            {renderTrigger(onOpen)}
            <Drawr
                title='Manage SEO'
                isOpen={isOpen}
                onClose={onClose}
                size='md'>
                <Formik
                    enableReinitialize
                    initialValues={initialValues}
                    validationSchema={validationSchema}
                    onSubmit={(values: InitialValues, _) => {
                        _.setSubmitting(true)

                        if (seo?.id) {
                            dispatch(updateSeo({ update: values, id: mapId }))
                        } else {
                            dispatch(createSeo({ seo: values, mapId }))
                        }

                        _.setSubmitting(false)
                        _.resetForm()
                        onClose()
                    }}>
                    {({
                        handleSubmit,
                        errors,
                        touched,
                        isSubmitting,
                        values,
                        setFieldValue,
                    }) => (
                        <Box>
                            {imageOptions && imageOptions.length > 0 && (
                                <FormControl mb='1rem'>
                                    <FormLabel
                                        htmlFor='seo-image'
                                        fontSize='lg'>
                                        Seo Image
                                    </FormLabel>
                                    <RadioGroup
                                        as={Grid}
                                        name='imageUrl'
                                        onChange={(val) =>
                                            setFieldValue('imageUrl', val)
                                        }
                                        value={values.imageUrl}
                                        gridTemplateColumns='repeat(4, 1fr)'
                                        gap='2'>
                                        {imageOptions.map((image) => (
                                            <Box
                                                key={image?.pub_id}
                                                width='100px'
                                                height='100px'
                                                rounded='md'
                                                position='relative'>
                                                <Stack
                                                    alignItems='end'
                                                    position='absolute'
                                                    p='10px'
                                                    height='100%'
                                                    width='100%'
                                                    bg='blackAlpha.500'
                                                    rounded='md'>
                                                    <Radio
                                                        value={image?.url}
                                                        size='lg'
                                                    />
                                                </Stack>
                                                <Img
                                                    src={image?.url}
                                                    alt={`radio-${image?.pub_id}`}
                                                    width='100%'
                                                    height='100%'
                                                    rounded='md'
                                                    objectFit='cover'
                                                />
                                            </Box>
                                        ))}
                                    </RadioGroup>
                                </FormControl>
                            )}

                            <SeoForm
                                submit={handleSubmit}
                                errors={errors}
                                touched={touched}
                                loading={isSubmitting}
                            />
                        </Box>
                    )}
                </Formik>
            </Drawr>
        </>
    )
}

export default SeoDrawr
