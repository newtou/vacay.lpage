import { useDisclosure } from '@chakra-ui/react'
import React from 'react'
import Drawr from '../../common/Drawr'
import { Formik, FormikHelpers } from 'formik'
import * as Yup from 'yup'
import { useDispatch } from 'react-redux'
import { createOffer } from '@/src/store/slices/Offers'
import OfferForm from '../../forms/offers/OfferForm'
import { AppDispatch } from '@/src/store'

type OfferDrawrProps = {
    edit?: boolean
    renderTrigger: (onOpen: () => void) => React.ReactNode // eslint-disable-line
}

type InitialValues = {
    name: string
    locations: string
    moreInfo: string
    inclexcl: string
    pricing: string
    overview: string
    itinerary: string
}

function OfferDrawr({ renderTrigger }: OfferDrawrProps) {
    const dispatch = useDispatch<AppDispatch>()
    const { isOpen, onOpen, onClose } = useDisclosure()

    const initialValues: InitialValues = {
        name: '',
        locations: '',
        moreInfo: '',
        inclexcl: '',
        pricing: '',
        overview: '',
        itinerary: '',
    }

    const validationSchema = Yup.object().shape({
        name: Yup.string().required('Name is required'),
        locations: Yup.string().required('Location is required'),
        overview: Yup.string().required('Overview is required'),
        itinerary: Yup.string(),
        moreInfo: Yup.string(),
        inclexcl: Yup.string(),
        pricing: Yup.string(),
    })

    return (
        <>
            {renderTrigger(onOpen)}
            <Drawr
                title='Create New Offer'
                isOpen={isOpen}
                onClose={onClose}
                size='lg'>
                <Formik
                    initialValues={initialValues}
                    enableReinitialize
                    validationSchema={validationSchema}
                    onSubmit={(
                        values: InitialValues,
                        _: FormikHelpers<InitialValues>
                    ) => {
                        _.setSubmitting(true)

                        // modify locations
                        let newLocations: {
                            name: string
                            latitude: number
                            longitude: number
                        }[] = []
                        if (values.locations) {
                            newLocations = values.locations
                                .split(',')
                                .map((location) => {
                                    location.trim()
                                    return {
                                        name: location,
                                        latitude: 0,
                                        longitude: 0,
                                    }
                                })
                        }

                        const newValues = {
                            ...values,
                            locations: newLocations,
                        }

                        dispatch(createOffer(newValues))

                        _.setSubmitting(false)
                        _.resetForm()
                        onClose()
                    }}>
                    {({ handleSubmit, errors, touched, isSubmitting }) => (
                        <OfferForm
                            submit={handleSubmit}
                            errors={errors}
                            touched={touched}
                            loading={isSubmitting}
                        />
                    )}
                </Formik>
            </Drawr>
        </>
    )
}

export default OfferDrawr
