import {
    FormControl,
    FormLabel,
    HStack,
    Switch,
    useDisclosure,
} from '@chakra-ui/react'
import React from 'react'
import Drawr from '../../common/Drawr'
import { Offer } from '@/types/index'
import { Formik } from 'formik'
import * as Yup from 'yup'
import { useDispatch } from 'react-redux'
import { updateOffer } from '@/src/store/slices/Offers'
import OfferForm from '../../forms/offers/OfferForm'
import { AppDispatch } from '@/src/store'

type EditOfferDrawrProps = {
    offer: Offer
    renderTrigger: (onOpen: () => void) => React.ReactNode // eslint-disable-line
}

type InitialValues = {
    name: string
    locations: string
    moreInfo: string
    inclexcl: string
    pricing: string
    overview: string
    itinerary: string
}

function EditOfferDrawr({ offer, renderTrigger }: EditOfferDrawrProps) {
    const dispatch = useDispatch<AppDispatch>()
    const { isOpen, onOpen, onClose } = useDisclosure()

    const locations =
        offer?.locations?.map((location) => location.name).join(', ') || ''

    const initialValues: InitialValues = {
        name: offer?.name || '',
        locations: locations || '',
        moreInfo: offer?.moreInfo || '',
        inclexcl: offer?.inclexcl || '',
        pricing: offer?.pricing || '',
        overview: offer?.overview || '',
        itinerary: offer?.itinerary || '',
    }

    const validationSchema = Yup.object().shape({
        name: Yup.string().required('Name is required'),
        locations: Yup.string().required('Location is required'),
        overview: Yup.string().required('Overview is required'),
        itinerary: Yup.string(),
        moreInfo: Yup.string(),
        inclexcl: Yup.string(),
        pricing: Yup.string(),
    })

    return (
        <>
            {renderTrigger(onOpen)}
            <Drawr
                title='Edit Offer'
                isOpen={isOpen}
                onClose={onClose}
                size='lg'>
                <HStack
                    bg='white'
                    minH='5vh'
                    rounded='md'
                    width='full'
                    spacing='3'
                    my='1rem'>
                    <FormControl
                        display='flex'
                        alignItems='center'
                        gap='3'
                        bg='gray.50'
                        p='10px 5px'
                        rounded='md'>
                        <Switch
                            id='feature'
                            isChecked={offer.active}
                            onChange={() => {
                                dispatch(
                                    updateOffer({
                                        id: offer.id,
                                        update: {
                                            active: !offer.active,
                                        },
                                    })
                                )
                            }}
                        />
                        <FormLabel htmlFor='feature' p='0' m='0'>
                            {offer.active
                                ? 'Remove from published'
                                : 'Publish Offer'}
                        </FormLabel>
                    </FormControl>
                    <FormControl
                        display='flex'
                        alignItems='center'
                        gap='3'
                        bg='gray.50'
                        p='10px 5px'
                        rounded='md'>
                        <Switch
                            id='feature'
                            isChecked={offer.featured}
                            onChange={() => {
                                dispatch(
                                    updateOffer({
                                        id: offer.id,
                                        update: {
                                            featured: !offer.featured,
                                        },
                                    })
                                )
                            }}
                        />
                        <FormLabel htmlFor='feature' p='0' m='0'>
                            {offer.featured
                                ? 'Remove from featured'
                                : 'Feature Offer'}
                        </FormLabel>
                    </FormControl>
                </HStack>
                <Formik
                    initialValues={initialValues}
                    enableReinitialize
                    validationSchema={validationSchema}
                    onSubmit={(values: InitialValues, _) => {
                        _.setSubmitting(true)

                        // modify locations
                        let newLocations: {
                            name: string
                            latitude: number
                            longitude: number
                        }[] = []
                        if (values.locations) {
                            newLocations = values.locations
                                .split(',')
                                .map((location) => {
                                    location.trim()
                                    return {
                                        name: location,
                                        latitude: 0,
                                        longitude: 0,
                                    }
                                })
                        }

                        const updateValues = {
                            id: offer.id,
                            update: {
                                ...values,
                                locations: newLocations,
                            },
                        }
                        dispatch(updateOffer(updateValues))

                        _.setSubmitting(false)
                        _.resetForm()
                        onClose()
                    }}>
                    {({ handleSubmit, errors, touched, isSubmitting }) => (
                        <OfferForm
                            submit={handleSubmit}
                            errors={errors}
                            touched={touched}
                            loading={isSubmitting}
                            edit
                        />
                    )}
                </Formik>
            </Drawr>
        </>
    )
}

export default EditOfferDrawr
