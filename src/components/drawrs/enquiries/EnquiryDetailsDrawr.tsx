import React from 'react'
import {
    useDisclosure,
    Grid,
    Box,
    VStack,
    Text,
    Heading,
    HStack,
    Icon,
} from '@chakra-ui/react'
import { HiOutlineMapPin } from 'react-icons/hi2'
import Drawr from '../../common/Drawr'
import { Enquiry } from '@/types/index'
import moment from 'moment'

type EnquiryDetailsDrawrProps = {
    enquiry: Enquiry
    renderTrigger: (onOpen: () => void) => React.ReactNode // eslint-disable-line
}

function EnquiryDetailsDrawr({
    renderTrigger,
    enquiry,
}: EnquiryDetailsDrawrProps) {
    const { isOpen, onOpen, onClose } = useDisclosure()

    return (
        <>
            {renderTrigger(onOpen)}
            <Drawr
                isOpen={isOpen}
                onClose={onClose}
                title='Enquiry Details'
                size='md'>
                <VStack spacing='1rem' alignItems='start'>
                    <Box m='0' p='0' width='100%'>
                        <Heading fontSize='3xl' fontWeight='bold'>
                            {enquiry?.offer.name}
                        </Heading>
                        <HStack my='0.5rem' fontSize='lg' color='brand.base'>
                            <Icon as={HiOutlineMapPin} boxSize={6} />
                            <Text>{enquiry?.destination}</Text>
                        </HStack>
                    </Box>

                    <Box width='100%'>
                        <Grid gap='1rem' templateColumns='repeat(2, 1fr)'>
                            <VStack width='100%' alignItems='flex-start'>
                                <Text fontWeight='semibold'>Name</Text>
                                <Text
                                    p='5px 10px'
                                    width='100%'
                                    height='2.5rem'
                                    rounded='lg'
                                    bg='gray.100'>
                                    {enquiry?.name}
                                </Text>
                            </VStack>
                            <VStack width='100%' alignItems='flex-start'>
                                <Text fontWeight='semibold'>Email</Text>
                                <Text
                                    p='5px 10px'
                                    width='100%'
                                    height='2.5rem'
                                    rounded='lg'
                                    bg='gray.100'>
                                    {enquiry?.email}
                                </Text>
                            </VStack>
                        </Grid>
                        <Grid
                            gap='1rem'
                            templateColumns='repeat(2, 1fr)'
                            my='0.5rem'>
                            <VStack width='100%' alignItems='flex-start'>
                                <Text fontWeight='semibold'>Adults</Text>
                                <Text
                                    p='5px 10px'
                                    width='100%'
                                    height='2.5rem'
                                    rounded='lg'
                                    bg='gray.100'>
                                    {enquiry?.adult}
                                </Text>
                            </VStack>
                            <VStack width='100%' alignItems='flex-start'>
                                <Text fontWeight='semibold'>Children</Text>
                                <Text
                                    p='5px 10px'
                                    width='100%'
                                    height='2.5rem'
                                    rounded='lg'
                                    bg='gray.100'>
                                    {enquiry?.adult}
                                </Text>
                            </VStack>
                        </Grid>
                        <Grid gap='1rem' templateColumns='repeat(2, 1fr)'>
                            <VStack width='100%' alignItems='flex-start'>
                                <Text fontWeight='semibold'>Phone</Text>
                                <Text
                                    p='5px 10px'
                                    width='100%'
                                    height='2.5rem'
                                    rounded='lg'
                                    bg='gray.100'>
                                    {enquiry?.number}
                                </Text>
                            </VStack>
                            <VStack width='100%' alignItems='flex-start'>
                                <Text fontWeight='semibold'>Destination</Text>
                                <Text
                                    p='5px 10px'
                                    width='100%'
                                    height='2.5rem'
                                    rounded='lg'
                                    bg='gray.100'>
                                    {enquiry?.destination}
                                </Text>
                            </VStack>
                        </Grid>

                        <Grid
                            gap='1rem'
                            templateColumns='repeat(2, 1fr)'
                            my='0.5rem'>
                            <VStack width='100%' alignItems='flex-start'>
                                <Text fontWeight='semibold'>Departure</Text>
                                <Text
                                    p='5px 10px'
                                    width='100%'
                                    height='2.5rem'
                                    rounded='lg'
                                    bg='gray.100'>
                                    {moment(enquiry?.departure).format(
                                        'DD MMM, YYYY'
                                    )}
                                </Text>
                            </VStack>
                            <VStack width='100%' alignItems='flex-start'>
                                <Text fontWeight='semibold'>Budget</Text>
                                <Text
                                    p='5px 10px'
                                    width='100%'
                                    height='2.5rem'
                                    rounded='lg'
                                    bg='gray.100'>
                                    {enquiry?.budget}
                                </Text>
                            </VStack>
                        </Grid>

                        {enquiry.message && (
                            <VStack width='100%' alignItems='flex-start'>
                                <Text fontWeight='semibold'>Message</Text>
                                <Text
                                    as='p'
                                    p='5px 10px'
                                    width='100%'
                                    minHeight='10vh'
                                    rounded='lg'
                                    bg='gray.100'>
                                    {enquiry?.message}
                                </Text>
                            </VStack>
                        )}
                    </Box>

                    {/* <Box my='1rem'>
                        <Heading size='md'>Notes</Heading>
                    </Box> */}
                </VStack>
            </Drawr>
        </>
    )
}

export default EnquiryDetailsDrawr
