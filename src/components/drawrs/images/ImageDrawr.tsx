import React, { useCallback, useEffect, useState } from 'react'
import {
    Alert,
    AlertIcon,
    Box,
    Center,
    Grid,
    GridItem,
    IconButton,
    Stack,
    Text,
    useDisclosure,
    Heading,
    Divider,
    CircularProgress,
} from '@chakra-ui/react'
import useMeasure from 'react-cool-dimensions'
import Drawr from '../../common/Drawr'
import DropZone from '../../common/Dropzone'
import { Image } from '@/types/index'
import { invoke } from '@/src/lib/axios.config'
import CustomImage from '../../common/CustomImage'
import DeleteModal from '../../common/DeleteModal'
import { HiOutlineTrash } from 'react-icons/hi2'
import { useDispatch } from 'react-redux'
import { fetchOffers } from '@/src/store/slices/Offers'
import { AppDispatch } from '@/src/store'
import { useEdgeStore } from '@/src/lib/edgestore'

type ImageDrawrProps = {
    map_id: string
    renderTrigger: (onOpen: () => void) => React.ReactNode // eslint-disable-line
}

type ImageWrapperProps = {
    image: Image
    setError: (error: string) => void // eslint-disable-line
    setSuccess: (success: string) => void // eslint-disable-line
    updateGallery: () => void // eslint-disable-line
}

function ImageWrapper({
    image,
    setError,
    setSuccess,
    updateGallery,
}: ImageWrapperProps) {
    const { edgestore } = useEdgeStore()
    const [loading, setLoading] = useState<boolean>(false)
    const { observe, width, height } = useMeasure()
    const [showDelete, setShowDelete] = useState<boolean>(false)

    // check if image is from edgestore
    const isEdgestore = image.url.includes('edgestore')

    const deleteImage = async (id: string) => {
        const { res, error } = await invoke('DELETE', `media/${id}`)
        if (error) {
            setError(error)
            return
        }

        setSuccess(res.message)
        updateGallery()
    }

    return (
        <Box
            ref={observe}
            rounded='md'
            width='140px'
            height='120px'
            cursor='pointer'
            onMouseEnter={() => setShowDelete(true)}
            onMouseLeave={() => setShowDelete(false)}
            position='relative'>
            <Stack
                w='full'
                h='full'
                position='absolute'
                _hover={{ bg: 'blackAlpha.300' }}
                justifyContent='start'
                alignItems='end'
                p='5px'
                rounded='md'
                zIndex='10'>
                {showDelete && !loading ? (
                    <DeleteModal
                        id={image.id}
                        loading={loading}
                        delFunc={async (id) => {
                            setLoading(true)
                            if (isEdgestore) {
                                // delete from edgestore
                                await edgestore.publicFiles.delete({
                                    url: image.url,
                                })
                            }

                            deleteImage(id)
                            setLoading(false)
                        }}
                        renderTrigger={(onOpen) => (
                            <Box>
                                <IconButton
                                    aria-label='Delete Image Button'
                                    onClick={onOpen}
                                    icon={<HiOutlineTrash size='15px' />}
                                    size='sm'
                                    rounded='full'
                                    color='red.500'
                                    shadow='md'
                                    bg='red.50'
                                    _hover={{ bg: 'red.100' }}
                                    _active={{ bg: 'red.100' }}
                                    _focus={{ boxShadow: 'none' }}
                                />
                            </Box>
                        )}
                    />
                ) : loading ? (
                    <CircularProgress
                        size={8}
                        isIndeterminate
                        color='red.500'
                    />
                ) : null}
            </Stack>
            <CustomImage
                src={image.url}
                alt={`media-${image.pub_id}`}
                width={width}
                height={height}
                rounded='xl'
                shadow='lg'
                objectFit='cover'
            />
        </Box>
    )
}

function ImageDrawr({ map_id, renderTrigger }: ImageDrawrProps) {
    const dispatch = useDispatch<AppDispatch>()
    const { isOpen, onOpen, onClose } = useDisclosure()
    const [error, setError] = useState<string | null>(null)
    const [success, setSuccess] = useState<string | null>(null)
    const [images, setImages] = useState<Image[] | null>(null)

    const fetchMappedImages = useCallback(async () => {
        const { res, error } = await invoke('GET', `media/mapped/${map_id}`)
        if (error) {
            setError(error)
            return
        }
        setImages(res.images)
    }, [map_id])

    useEffect(() => {
        fetchMappedImages()
    }, [fetchMappedImages])

    useEffect(() => {
        if (error) {
            setTimeout(() => {
                setError(null)
            }, 3000)
        }

        if (success) {
            setTimeout(() => {
                setSuccess(null)
            }, 3000)
        }
    }, [error, success])

    const updateGallery = useCallback(async () => {
        fetchMappedImages()
        dispatch(fetchOffers({}))
    }, [dispatch, fetchMappedImages])

    return (
        <>
            {renderTrigger(onOpen)}
            <Drawr isOpen={isOpen} onClose={onClose} title='Manage media'>
                <Stack spacing='4' direction='column'>
                    {error && (
                        <Alert status='error' my='1rem' rounded='md'>
                            <AlertIcon />
                            {error}
                        </Alert>
                    )}

                    {success && (
                        <Alert status='success' my='1rem' rounded='md'>
                            <AlertIcon />
                            {success}
                        </Alert>
                    )}

                    <DropZone updateGallery={updateGallery} map_id={map_id} />

                    <Heading size='lg'>Gallery</Heading>

                    <Divider />

                    <Grid gap='3' templateColumns='repeat(3, 1fr)' w='full'>
                        {images &&
                            images.length > 0 &&
                            images.map((image) => (
                                <GridItem key={image.id}>
                                    <ImageWrapper
                                        image={image}
                                        setError={setError}
                                        setSuccess={setSuccess}
                                        updateGallery={updateGallery}
                                    />
                                </GridItem>
                            ))}
                    </Grid>
                    {images && images.length === 0 && (
                        <Center>
                            <Text>No images found 😧 </Text>
                        </Center>
                    )}
                </Stack>
            </Drawr>
        </>
    )
}

export default ImageDrawr
