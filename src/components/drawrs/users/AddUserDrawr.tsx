import React from 'react'
import { Box, useDisclosure } from '@chakra-ui/react'
import { Formik } from 'formik'
import * as Yup from 'yup'
import { useDispatch } from 'react-redux'
import Drawr from '../../common/Drawr'
import { createUser } from '@/src/store/slices/Users'
import NewUserForm from '@/src/components/forms/profile/NewUserForm'
import { AppDispatch } from '@/src/store'

type AddUserDrawrProps = {
    renderTrigger: (onOpen: () => void) => React.ReactNode // eslint-disable-line
}

type InitialValues = {
    name: string
    email: string
    role: string
    password: string
    confirmpass?: string
}

function AddUserDrawr({ renderTrigger }: AddUserDrawrProps) {
    const dispatch = useDispatch<AppDispatch>()
    const { isOpen, onOpen, onClose } = useDisclosure()

    const initialValues: InitialValues = {
        name: '',
        email: '',
        role: '',
        password: '',
        confirmpass: '',
    }

    const validationSchema = Yup.object().shape({
        name: Yup.string().required('Name is required'),
        email: Yup.string()
            .email('Invalid email')
            .required('Email is required'),
        role: Yup.string().required('Role is required'),
        password: Yup.string().min(6).required('Password is required'),
        confirmpass: Yup.string()
            .min(6)
            .oneOf([Yup.ref('password'), ''], 'Passwords must match'),
    })

    return (
        <>
            {renderTrigger(onOpen)}
            <Drawr title='New User' isOpen={isOpen} onClose={onClose} size='md'>
                <Formik
                    enableReinitialize
                    initialValues={initialValues}
                    validationSchema={validationSchema}
                    onSubmit={(values: InitialValues, _) => {
                        _.setSubmitting(true)
                        const params = {
                            ...values,
                        }
                        dispatch(createUser(params))
                        _.setSubmitting(false)
                        _.resetForm()
                    }}>
                    {({ handleSubmit, errors, touched, isSubmitting }) => (
                        <Box>
                            <NewUserForm
                                submit={handleSubmit}
                                errors={errors}
                                touched={touched}
                                loading={isSubmitting}
                            />
                        </Box>
                    )}
                </Formik>
            </Drawr>
        </>
    )
}

export default AddUserDrawr
