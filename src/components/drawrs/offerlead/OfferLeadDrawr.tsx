import { useDisclosure } from '@chakra-ui/react'
import React from 'react'
import Drawr from '../../common/Drawr'

import OfferLead from '../../offer/OfferLead'

type OfferLeadDrawrProps = {
    renderTrigger: (onOpen: () => void) => React.ReactNode // eslint-disable-line
}

function OfferLeadDrawr({ renderTrigger }: OfferLeadDrawrProps) {
    const { isOpen, onOpen, onClose } = useDisclosure()

    return (
        <>
            {renderTrigger(onOpen)}
            <Drawr
                title='Send a request'
                isOpen={isOpen}
                onClose={onClose}
                placement='bottom'
                size='lg'>
                <OfferLead />
            </Drawr>
        </>
    )
}

export default OfferLeadDrawr
