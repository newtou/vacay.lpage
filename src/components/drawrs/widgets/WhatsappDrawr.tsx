import React from 'react'
import Drawr from '../../common/Drawr'
import WhatsappContact from '../../common/WhatsappContact'

function WhatsappDrawr({ isOpen, onClose }) {
    return (
        <Drawr
            flat
            isOpen={isOpen}
            onClose={onClose}
            placement='bottom'
            title='Whatsapp Support'>
            <WhatsappContact />
        </Drawr>
    )
}

export default WhatsappDrawr
