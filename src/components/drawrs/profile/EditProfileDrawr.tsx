import {
    Heading,
    Tab,
    TabList,
    TabPanel,
    TabPanels,
    Tabs,
    useDisclosure,
} from '@chakra-ui/react'
import React from 'react'
import Drawr from '../../common/Drawr'
import { Formik, FormikHelpers } from 'formik'
import * as Yup from 'yup'
import { useDispatch, useSelector } from 'react-redux'
import EditProfileForm from '../../forms/profile/EditProfileForm'
import { updateUser } from '@/src/store/slices/Users'
import EditPasswordForm from '../../forms/profile/EditPasswordForm'
import { fetchMe } from '@/src/store/slices/Auth'
import { AppDispatch, RootState } from '@/src/store'

type EditProfileDrawrProps = {
    renderTrigger: (onOpen: () => void) => React.ReactNode // eslint-disable-line
}

type userInitialValues = {
    name: string
    email: string
}

type passInitialValues = {
    password: string
    confirmpass: string
}

function EditProfileDrawr({ renderTrigger }: EditProfileDrawrProps) {
    const dispatch = useDispatch<AppDispatch>()
    const { isOpen, onOpen, onClose } = useDisclosure()
    const { me } = useSelector((state: RootState) => state.auth)

    const userInitialValues = {
        name: me?.name || '',
        email: me?.email || '',
    }

    const passInitialValues = {
        password: '',
        confirmpass: '',
    }

    const userValidator = Yup.object().shape({
        name: Yup.string().required('Name is required'),
        email: Yup.string()
            .email('Invalid email')
            .required('Email is required'),
    })

    const passValidator = Yup.object().shape({
        password: Yup.string().min(6).required('Password is required'),
        confirmpass: Yup.string()
            .min(6)
            .oneOf([Yup.ref('password'), null], 'Passwords must match'),
    })

    return (
        <>
            {renderTrigger(onOpen)}
            <Drawr
                title='Update Profile'
                isOpen={isOpen}
                onClose={onClose}
                size='md'>
                <Tabs variant='unstyled' my='1rem'>
                    <TabList
                        borderBottom='2px solid'
                        pb='10px'
                        borderColor='gray.50'>
                        <Tab
                            _selected={{
                                bg: 'brand.primary.50',
                                color: 'brand.primary.700',
                                rounded: 'full',
                                border: '1.5px solid',
                                borderColor: 'brand.primary.100',
                            }}>
                            <Heading
                                display={['block']}
                                fontSize={['md', '', 'lg']}>
                                Profile
                            </Heading>
                        </Tab>
                        <Tab
                            _selected={{
                                bg: 'brand.primary.50',
                                color: 'brand.primary.700',
                                rounded: 'full',
                                border: '1.5px solid',
                                borderColor: 'brand.primary.100',
                            }}>
                            <Heading
                                display={['block']}
                                fontSize={['md', '', 'lg']}>
                                Password
                            </Heading>
                        </Tab>
                    </TabList>
                    <TabPanels>
                        <TabPanel px='0'>
                            <Formik
                                initialValues={userInitialValues}
                                enableReinitialize
                                validationSchema={userValidator}
                                onSubmit={(
                                    values: userInitialValues,
                                    _: FormikHelpers<userInitialValues>
                                ) => {
                                    _.setSubmitting(true)
                                    dispatch(updateUser({ ...me, ...values }))
                                    dispatch(fetchMe(me.id))
                                    _.setSubmitting(false)
                                    _.resetForm()
                                }}>
                                {({
                                    handleSubmit,
                                    errors,
                                    touched,
                                    isSubmitting,
                                }) => (
                                    <EditProfileForm
                                        submit={handleSubmit}
                                        errors={errors}
                                        touched={touched}
                                        loading={isSubmitting}
                                    />
                                )}
                            </Formik>
                        </TabPanel>

                        <TabPanel px='0'>
                            <Formik
                                initialValues={passInitialValues}
                                enableReinitialize
                                validationSchema={passValidator}
                                onSubmit={(
                                    values: passInitialValues,
                                    _: FormikHelpers<passInitialValues>
                                ) => {
                                    _.setSubmitting(true)
                                    dispatch(
                                        updateUser({
                                            ...me,
                                            password: values.password,
                                        })
                                    )
                                    _.setSubmitting(false)
                                    _.resetForm()
                                }}>
                                {({
                                    handleSubmit,
                                    errors,
                                    touched,
                                    isSubmitting,
                                }) => (
                                    <EditPasswordForm
                                        submit={handleSubmit}
                                        errors={errors}
                                        touched={touched}
                                        loading={isSubmitting}
                                    />
                                )}
                            </Formik>
                        </TabPanel>
                    </TabPanels>
                </Tabs>
            </Drawr>
        </>
    )
}

export default EditProfileDrawr
