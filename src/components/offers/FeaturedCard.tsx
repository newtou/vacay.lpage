import React, { useMemo } from 'react'
import Link from 'next/link'
import useDimensions from 'react-cool-dimensions'
import { Box, Heading, HStack, Icon, Text, VStack } from '@chakra-ui/react'
import { HiOutlineMapPin } from 'react-icons/hi2'
import CustomImage from '../common/CustomImage'
import { Offer } from '@/types/index'

type FeaturedCardProps = {
    offer: Offer
    size?: 'sm' | 'md' | 'lg'
}

function FeaturedCard({ offer, size = 'md' }: FeaturedCardProps) {
    const { observe, width, height } = useDimensions()

    const offerImage = useMemo(() => {
        if (!offer?.images) return '/images/imgplaceholder.png'
        return offer?.images[0]?.url
    }, [offer?.images])

    return (
        <Link href={`/offers/${offer?.slug}`} passHref>
            <VStack
                ref={observe}
                height={
                    size === 'sm' ? '20vh' : size === 'md' ? '30vh' : '40vh'
                }
                width='100%'
                rounded='2xl'
                cursor='pointer'
                alignItems='flex-start'
                overflow='hidden'
                position='relative'>
                <Box width={width} height={height} rounded='lg'>
                    <CustomImage
                        src={offerImage}
                        alt={offer?.name}
                        width={width}
                        height={height}
                        objectFit='cover'
                    />
                </Box>
                <VStack
                    top='0'
                    left='0'
                    spacing='2'
                    width='100%'
                    height='100%'
                    p='20px 15px'
                    color='gray.100'
                    position='absolute'
                    alignItems='flex-start'
                    justifyContent='flex-end'
                    bgGradient='linear(to-b, blackAlpha.50, blackAlpha.400, blackAlpha.900)'>
                    <HStack
                        fontSize='lg'
                        spacing='1'
                        alignItems='center'
                        color='gray.300'>
                        <Icon as={HiOutlineMapPin} boxSize={5} />

                        <HStack spacing='1'>
                            <Text>{offer?.locations[0].name}</Text>
                            {offer?.locations.length > 1 && <Text>...</Text>}
                        </HStack>
                    </HStack>
                    <Heading
                        fontSize='2xl'
                        textTransform='capitalize'
                        noOfLines={2}
                        cursor='pointer'>
                        {offer?.name}
                    </Heading>
                </VStack>
            </VStack>
        </Link>
    )
}

FeaturedCard.defaultProps = {
    offer: {},
}

export default FeaturedCard
