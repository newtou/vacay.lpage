import React, { useMemo } from 'react'
import PropTypes from 'prop-types'
import Link from 'next/link'
import { Box, Heading, HStack, Icon, Text, VStack } from '@chakra-ui/react'
import { HiOutlineMapPin } from 'react-icons/hi2'
import CustomImage from '../common/CustomImage'
import useDimensions from 'react-cool-dimensions'
import { Offer } from '@/types/index'

type OfferCardProps = {
    offer: Offer
    rounded?: boolean
    size?: 'sm' | 'md' | 'lg'
}

function OfferCard({ offer, size = 'md', rounded = true }: OfferCardProps) {
    const { observe, width, height } = useDimensions()

    const offerImage = useMemo(() => {
        if (!offer?.images) return '/images/imgplaceholder.png'
        return offer?.images[0]?.url
    }, [offer?.images])

    return (
        <Link href={`/offers/${offer?.slug}`} passHref>
            <VStack
                height='auto'
                minHeight='20vh'
                rounded='lg'
                spacing='3'
                transition='all 0.2s ease-in-out'
                alignItems='flex-start'
                position='relative'>
                <Box
                    ref={observe}
                    width='100%'
                    position='relative'
                    overflow='hidden'
                    rounded={rounded ? 'lg' : 'none'}
                    height={
                        size === 'sm' ? '20vh' : size === 'md' ? '30vh' : '40vh'
                    }
                    _hover={{
                        _after: {
                            content: '""',
                            position: 'absolute',
                            top: '0',
                            left: '0',
                            width: '100%',
                            height: '100%',
                            bgGradient:
                                'linear(to-b, blackAlpha.50, blackAlpha.400, blackAlpha.700)',
                        },
                    }}>
                    <CustomImage
                        src={offerImage}
                        alt={offer?.name}
                        width={width}
                        height={height}
                        objectFit='cover'
                        shadow='xl'
                    />
                </Box>
                <VStack
                    justifyContent='flex-start'
                    alignItems='flex-start'
                    width='100%'
                    px='5px'
                    spacing='2'>
                    <HStack
                        alignItems='center'
                        justifyContent='space-between'
                        color='gray.600'>
                        <HStack fontSize='lg' spacing='1'>
                            <Icon as={HiOutlineMapPin} boxSize={5} />
                            <HStack spacing='1'>
                                <Text>{offer?.locations[0].name}</Text>
                                {offer?.locations.length > 1 && (
                                    <Text>...</Text>
                                )}
                            </HStack>
                        </HStack>
                    </HStack>
                    <Heading
                        fontSize='2xl'
                        textTransform='capitalize'
                        noOfLines={2}
                        cursor='pointer'>
                        {offer?.name}
                    </Heading>
                </VStack>
            </VStack>
        </Link>
    )
}

OfferCard.propTypes = {
    offer: PropTypes.object,
}

OfferCard.defaultProps = {
    offer: {},
}

export default OfferCard
