import React from 'react'
import Link from 'next/link'
import {
    Box,
    Heading,
    HStack,
    Icon,
    Image,
    Text,
    VStack,
} from '@chakra-ui/react'
import { FaMapMarkerAlt } from 'react-icons/fa'
import { Offer } from '@/types/index'

type SearchCardProps = {
    offer: Offer
}

function SearchCard({ offer }: SearchCardProps) {
    return (
        <HStack
            bg='white'
            height={['auto', 'auto', '15vh', '15vh']}
            minHeight='15vh'
            rounded='2xl'
            p='5px'
            shadow='md'
            spacing='3'
            alignItems='flex-start'
            position='relative'>
            <Box width='30%' height='100%'>
                <Image
                    src={offer?.images[0]?.url}
                    alt={offer?.name}
                    fallbackSrc={'/images/imgplaceholder.png'}
                    objectFit='cover'
                    objectPosition='center'
                    width='100%'
                    height='100%'
                    rounded='lg'
                />
            </Box>
            <VStack
                justifyContent='space-between'
                alignItems='flex-start'
                width='70%'
                spacing='2'
                p='10px 0'
                height='100%'>
                <Link href={`/offers/${offer?.slug}`} passHref>
                    <Heading
                        fontSize='md'
                        textTransform='capitalize'
                        cursor='pointer'>
                        {offer?.name}
                    </Heading>
                </Link>
                <HStack
                    my='0.3rem'
                    alignItems='center'
                    justifyContent='space-between'>
                    <HStack spacing='1'>
                        <Icon as={FaMapMarkerAlt} fontSize='lg' />
                        <Text fontSize='lg'>{offer?.destinations[0]}</Text>
                    </HStack>
                </HStack>
            </VStack>
        </HStack>
    )
}

SearchCard.defaultProps = {
    offer: {},
}

export default SearchCard
