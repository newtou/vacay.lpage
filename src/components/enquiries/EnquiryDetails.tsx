import {
    Box,
    Grid,
    Heading,
    HStack,
    Icon,
    Text,
    VStack,
} from '@chakra-ui/react'
import moment from 'moment'
import { FaMapMarkerAlt } from 'react-icons/fa'
import Modall from '../common/Modall'

function EnquiryDetails({ enquiry, onClose, isOpen }) {
    return (
        <Modall
            isOpen={isOpen}
            onClose={onClose}
            title={enquiry?.name}
            size='lg'>
            <Box>
                <Heading fontSize='lg'>{enquiry?.offer}</Heading>
                <HStack my='0.5rem' fontSize='lg'>
                    <Icon as={FaMapMarkerAlt} />
                    <Text>{enquiry?.destination}</Text>
                </HStack>

                <Grid gap='1rem' templateColumns='repeat(2, 1fr)'>
                    <VStack width='100%' alignItems='flex-start'>
                        <Text fontSize='lg'>Adults</Text>
                        <Text
                            fontSize='lg'
                            p='5px 10px'
                            width='100%'
                            height='2.5rem'
                            rounded='5px'
                            bg='gray.100'>
                            {enquiry?.adult}
                        </Text>
                    </VStack>
                    <VStack width='100%' alignItems='flex-start'>
                        <Text fontSize='lg'>Children</Text>
                        <Text
                            fontSize='lg'
                            p='5px 10px'
                            width='100%'
                            height='2.5rem'
                            rounded='10px'
                            bg='gray.100'>
                            {enquiry?.adult}
                        </Text>
                    </VStack>
                </Grid>
                <VStack width='100%' alignItems='flex-start' my='0.5rem'>
                    <Text fontSize='lg'>Phone</Text>
                    <Text
                        fontSize='lg'
                        p='5px 10px'
                        width='100%'
                        height='2.5rem'
                        rounded='5px'
                        bg='gray.100'>
                        {enquiry?.number}
                    </Text>
                </VStack>
                <Grid gap='1rem' templateColumns='repeat(2, 1fr)' my='0.5rem'>
                    <VStack width='100%' alignItems='flex-start'>
                        <Text fontSize='lg'>Departure</Text>
                        <Text
                            fontSize='lg'
                            p='5px 10px'
                            width='100%'
                            height='2.5rem'
                            rounded='5px'
                            bg='gray.100'>
                            {moment(enquiry?.departure).format('DD-MMMM-YYYY')}
                        </Text>
                    </VStack>
                    <VStack width='100%' alignItems='flex-start'>
                        <Text fontSize='lg'>Budget</Text>
                        <Text
                            fontSize='lg'
                            p='5px 10px'
                            width='100%'
                            height='2.5rem'
                            rounded='10px'
                            bg='gray.100'>
                            {enquiry?.budget}
                        </Text>
                    </VStack>
                </Grid>
                <VStack width='100%' alignItems='flex-start'>
                    <Text fontSize='lg'>Message</Text>
                    <Text
                        as='p'
                        fontSize='lg'
                        p='5px 10px'
                        width='100%'
                        minHeight='15vh'
                        rounded='10px'
                        bg='gray.100'>
                        {enquiry?.message}
                    </Text>
                </VStack>
            </Box>
        </Modall>
    )
}

export default EnquiryDetails
