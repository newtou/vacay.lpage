import usePagination from '@/src/hooks/usePagination'
import { Enquiry } from '@/types/index'
import { Box, Grid, GridItem, Skeleton, Text } from '@chakra-ui/react'
import React, { lazy, Suspense } from 'react'
import Paginate from '../common/Paginate'
import TableComponent from '../common/TableComponent'

const EnquiryItem = lazy(() => import('./EnquiryItem'))

type EnquiryListProps = {
    enquiries: Enquiry[]
}

function EnquiryList({ enquiries }: EnquiryListProps) {
    const headers = [
        { name: 'Name', colSpan: 1 },
        { name: 'Offer', colSpan: 2 },
        { name: 'Created', colSpan: 1 },
        { name: 'Actions', colSpan: 1 },
    ]
    const { nextPage, previousPage, pages, currentPage, sliceStart, sliceEnd } =
        usePagination(10, enquiries)
    return (
        <Box rounded='lg'>
            <TableComponent
                renderHeaders={() => (
                    <Grid
                        gridTemplateColumns='repeat(5, 1fr)'
                        bg='white'
                        p='25px'
                        gap='1rem'
                        rounded='xl'
                        mb='1rem'>
                        {headers.map((header) => (
                            <GridItem
                                key={header?.name}
                                colSpan={header?.colSpan}>
                                <Text
                                    fontWeight='bold'
                                    fontSize='md'
                                    textTransform='uppercase'>
                                    {header.name}
                                </Text>
                            </GridItem>
                        ))}
                    </Grid>
                )}
                showHeaders>
                {enquiries.slice(sliceStart, sliceEnd).map((enquiry) => (
                    <Suspense
                        key={enquiry.id}
                        fallback={<Skeleton my='1rem' w='full' h='15vh' />}>
                        <EnquiryItem enquiry={enquiry} />
                    </Suspense>
                ))}
            </TableComponent>
            <Paginate
                pages={pages}
                currentPage={currentPage}
                next={nextPage}
                prev={previousPage}
            />
        </Box>
    )
}

EnquiryList.defaultProps = {
    enquiries: [],
}

export default EnquiryList
