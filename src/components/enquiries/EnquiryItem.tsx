import { Grid, GridItem, Text, HStack } from '@chakra-ui/react'
import React from 'react'
import moment from 'moment'
import { HiOutlineTrash, HiOutlineEye } from 'react-icons/hi2'
import DeleteModal from '../common/DeleteModal'
import Menu from '../common/Menu'
import { useDispatch, useSelector } from 'react-redux'
import { removeEnquiry } from '@/src/store/slices/Enquiries'
import { Enquiry } from '@/types/index'
import EnquiryDetailsDrawr from '../drawrs/enquiries/EnquiryDetailsDrawr'
import { AppDispatch, RootState } from '@/src/store'

type EnquiryItemProps = {
    enquiry: Enquiry
}

function EnquiryItem({ enquiry }: EnquiryItemProps) {
    const dispatch = useDispatch<AppDispatch>()
    const { loading } = useSelector((state: RootState) => state.enquiries)

    const deleteEnquiry = async (id: string) => {
        dispatch(removeEnquiry(id))
    }

    return (
        <Grid
            gridTemplateColumns='repeat(5, 1fr)'
            bg='white'
            p='25px 30px'
            w='full'
            gap='1rem'
            shadow='sm'
            minH='10vh'
            rounded='xl'>
            <GridItem as={HStack}>
                <Text fontSize='md' fontWeight='bold'>
                    {enquiry?.name}
                </Text>
            </GridItem>

            <GridItem colSpan={2} as={HStack}>
                <Text fontSize='md'>{enquiry?.offer?.name}</Text>
            </GridItem>
            <GridItem as={HStack}>
                <Text fontSize='md'>
                    {moment(enquiry?.created).format('DD MMMM, YYYY')}
                </Text>
            </GridItem>

            <GridItem as={HStack}>
                <Menu placement='left'>
                    <EnquiryDetailsDrawr
                        enquiry={enquiry}
                        renderTrigger={(onOpen) => (
                            <Menu.Item
                                icon={<HiOutlineEye size='20px' />}
                                onClick={onOpen}
                                rounded='lg'
                                fontSize='md'
                                _hover={{
                                    bg: 'blue.50',
                                    color: 'brand.base',
                                }}>
                                View details
                            </Menu.Item>
                        )}
                    />
                    <DeleteModal
                        label={enquiry?.name}
                        id={enquiry?.id}
                        delFunc={deleteEnquiry}
                        loading={loading}
                        renderTrigger={(onOpen) => (
                            <Menu.Item
                                icon={<HiOutlineTrash size='20px' />}
                                onClick={onOpen}
                                rounded='lg'
                                fontSize='md'
                                _hover={{ bg: 'red.50', color: 'red.500' }}>
                                Delete
                            </Menu.Item>
                        )}
                    />
                </Menu>
            </GridItem>
        </Grid>
    )
}

EnquiryItem.defaultProps = {
    enquiry: {},
}

export default EnquiryItem
