import { Box, Heading, useMediaQuery } from '@chakra-ui/react'
import React from 'react'
import Unavailable from '../common/Unavailable'
import { Offer } from '@/types/index'
import Slider from '../common/Slider'
import OfferCard from '../offers/OfferCard'

type FeaturedListProps = {
    featured: Offer[]
}

function FeaturedList({ featured }: FeaturedListProps) {
    const [isLargeScreen] = useMediaQuery('(min-width: 768px)')

    return (
        <Box position='relative' my={['0', '', '2rem']}>
            <Heading my={['1rem', '', '2rem']} size='lg'>
                Featured Offers
            </Heading>

            <Slider
                slidesToShow={isLargeScreen ? 4 : 1.2}
                slidesToScroll={isLargeScreen ? 2 : 1}
                speed={500}
                arrows={isLargeScreen ? true : false}>
                {featured.map((offer, index) => {
                    const isLastIndex = index === featured.length - 1
                    return (
                        <Box key={offer?.id}>
                            <Box mr={isLastIndex ? '' : ['0.5rem', '', '1rem']}>
                                <OfferCard offer={offer} size='md' />
                            </Box>
                        </Box>
                    )
                })}
            </Slider>

            {!featured.length && (
                <Unavailable message='No featured offers available' />
            )}
        </Box>
    )
}

FeaturedList.defaultProps = {
    featured: [],
}

export default FeaturedList
