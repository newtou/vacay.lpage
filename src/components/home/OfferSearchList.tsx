import {
    Box,
    Button,
    Heading,
    HStack,
    Icon,
    Text,
    useMediaQuery,
} from '@chakra-ui/react'
import React from 'react'
import { HiOutlineXMark } from 'react-icons/hi2'
import { useDispatch, useSelector } from 'react-redux'
import Slider from '../common/Slider'
import FeaturedCard from '../offers/FeaturedCard'
import { clearResults } from '@/src/store/slices/Offers'
import { Offer } from '@/types/index'
import Overflow from '../common/Overflow'
import { RootState, AppDispatch } from '@/src/store'

function OffersSearchList() {
    const dispatch = useDispatch<AppDispatch>()
    const [isLargeScreen] = useMediaQuery('(min-width: 768px)')
    const { results } = useSelector((state: RootState) => state.offers)

    return results.length > 0 ? (
        <Box position='relative' mb='2rem' mt={['', '', '2rem']} width='100%'>
            <HStack alignItems='center' justifyContent='space-between'>
                <Heading fontSize={['xl', '2xl']}>
                    Showing {results.length}{' '}
                    {results.length > 1 ? 'results' : 'result'}
                </Heading>

                <Button
                    rounded='full'
                    p='10px'
                    onClick={() => dispatch(clearResults())}
                    _hover={{ shadow: 'base' }}
                    _focus={{ shadow: 'none', color: 'gray.50' }}
                    cursor='pointer'
                    shadow='md'
                    display='flex'
                    flexDirection='row'
                    alignItems='center'
                    gap='2'
                    bg='white'>
                    <Icon as={HiOutlineXMark} boxSize={5} />
                    <Text display={['none', 'none', 'block']}>
                        Clear search
                    </Text>
                </Button>
            </HStack>

            {results && results.length > 4 && (
                <Slider
                    slidesToShow={isLargeScreen ? 4 : 1.2}
                    slidesToScroll={isLargeScreen ? 2 : 1}
                    speed={500}
                    slideSize='sm'
                    arrows={isLargeScreen ? true : false}>
                    {results.map((item: Offer, index: number) => (
                        <Box key={item?.id}>
                            <Box
                                mr={
                                    index === results.length - 1
                                        ? ''
                                        : ['0.5rem', '', '1rem']
                                }>
                                <FeaturedCard offer={item} size='sm' />
                            </Box>
                        </Box>
                    ))}
                </Slider>
            )}
            {results && results.length <= 4 && (
                <Overflow>
                    {results.map((item: Offer) => (
                        <Box
                            key={item?.id}
                            minWidth={['100%', '80%', '50%', '30%']}>
                            <FeaturedCard offer={item} size='sm' />
                        </Box>
                    ))}
                </Overflow>
            )}
        </Box>
    ) : null
}

export default OffersSearchList
