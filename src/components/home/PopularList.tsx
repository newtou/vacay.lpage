import React, { useEffect, lazy, Suspense } from 'react'
import { Box, Heading, useMediaQuery } from '@chakra-ui/react'
import { ErrorBoundary } from 'react-error-boundary'
import ErrorHandler from '../common/ErrorHandler'
import { useDispatch, useSelector } from 'react-redux'
import { fetchPopular } from '@/src/store/slices/Offers'
import Slider from '../common/Slider'
import { RootState, AppDispatch } from '@/src/store'

const OfferCard = lazy(() => import('../offers/OfferCard'))
function PopularList() {
    const dispatch = useDispatch<AppDispatch>()
    const [isLargeScreen] = useMediaQuery('(min-width: 768px)')
    const { popular } = useSelector((state: RootState) => state.offers)

    useEffect(() => {
        dispatch(fetchPopular({}))
    }, [dispatch])

    return (
        <ErrorBoundary FallbackComponent={ErrorHandler}>
            {popular?.length > 0 && (
                <Box
                    position='relative'
                    minHeight='30vh'
                    mt='2rem'
                    mb={['2rem', '', '5rem']}>
                    <Heading size='lg' my={['1rem', '', '2rem']}>
                        Popular Offers
                    </Heading>

                    <Slider
                        slidesToShow={isLargeScreen ? 4 : 1.2}
                        slidesToScroll={isLargeScreen ? 2 : 1}
                        speed={500}
                        arrows={isLargeScreen ? true : false}>
                        {popular &&
                            popular.map((item, index: number) => {
                                const lastIndex = index === popular.length - 1
                                return (
                                    <Suspense
                                        key={item?.offer?.id}
                                        fallback={<Box>Loading ...</Box>}>
                                        <Box
                                            mr={
                                                lastIndex
                                                    ? '0'
                                                    : ['0.5rem', '', '1rem']
                                            }>
                                            <OfferCard
                                                offer={item?.offer}
                                                size='md'
                                            />
                                        </Box>
                                    </Suspense>
                                )
                            })}
                    </Slider>
                </Box>
            )}
        </ErrorBoundary>
    )
}

PopularList.defaultProps = {
    featured: [],
}

export default PopularList
