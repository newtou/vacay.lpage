import { Box, Heading } from '@chakra-ui/react'
import React from 'react'
import { Formik } from 'formik'
import * as yup from 'yup'
import SearchForm from '../forms/SearchForm'
import { useDispatch, useSelector } from 'react-redux'
import { search } from '@/src/store/slices/Offers'
import { RootState, AppDispatch } from '@/src/store'

function SearchBar() {
    const dispatch = useDispatch<AppDispatch>()
    const { loading } = useSelector((state: RootState) => state.offers)

    const validator = yup.object().shape({
        destination: yup.string().required('Destination is required'),
    })
    
    return (
        <Box height='auto' width='auto'>
            <Heading fontSize='lg' mb='1rem' color='gray.600'>
                Explore Offers
            </Heading>
            <Formik
                initialValues={{
                    destination: '',
                }}
                validationSchema={validator}
                onSubmit={async (values) => {
                    dispatch(search({ search: values.destination }))
                }}>
                {({ handleSubmit }) => (
                    <SearchForm submit={handleSubmit} loading={loading} />
                )}
            </Formik>
        </Box>
    )
}

export default SearchBar
