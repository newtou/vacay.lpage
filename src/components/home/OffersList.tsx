import { Box, Center, Grid, Heading, Skeleton } from '@chakra-ui/react'
import React, { Suspense, lazy, useCallback, useEffect, useState } from 'react'
import Unavailable from '../common/Unavailable'
import { useDispatch, useSelector } from 'react-redux'
import { AppDispatch, RootState } from '@/src/store'
import { fetchActiveOffers } from '@/src/store/slices/Offers'
import Button from '../ui/atoms/Button'

const FeaturedCard = lazy(() => import('../offers/FeaturedCard'))

function OffersList() {
    const dispatch = useDispatch<AppDispatch>()

    const {
        activeOffers: { offers, currentPage, totalPages },
        fetching,
    } = useSelector((state: RootState) => state.offers)
    const [page, setPage] = useState(1)
    const [limit] = useState(8)

    const fetcher = useCallback(
        ({ page, limit }) => dispatch(fetchActiveOffers({ page, limit })),
        [dispatch]
    )

    const hasMore = currentPage && totalPages ? currentPage < totalPages : false

    useEffect(() => {
        if (hasMore) {
            fetcher({ page, limit })
        }
    }, [hasMore, fetcher, page, limit])

    return (
        <Box
            position='relative'
            minHeight='40vh'
            my={['1rem', '', '4rem']}
            width='100%'>
            <Heading my={['1rem', '', '2rem']} size='lg'>
                Explore More Offers{' '}
            </Heading>

            <Grid
                gap={['1rem', '1rem', '1rem', '3rem 1rem']}
                templateColumns={[
                    'repeat(1, 1fr)',
                    'repeat(2, 1fr)',
                    'repeat(2, 1fr)',
                    'repeat(2, 1fr)',
                    'repeat(3, 1fr)',
                    'repeat(4, 1fr)',
                ]}>
                {offers &&
                    offers.map((offer) => (
                        <Suspense
                            key={offer?.id}
                            fallback={
                                <Skeleton
                                    height='40vh'
                                    width='full'
                                    rounded='xl'
                                />
                            }>
                            <FeaturedCard offer={offer} size='lg' />
                        </Suspense>
                    ))}

                {fetching &&
                    !offers.length &&
                    Array.from([1, 2, 3, 4, 5, 6]).map((id) => (
                        <Skeleton
                            key={id * 434}
                            height='40vh'
                            width='full'
                            rounded='xl'
                        />
                    ))}
            </Grid>

            <Center width='full' my='2rem'>
                {offers.length > 0 && hasMore && (
                    <Button
                        width={['full', 'full', '30%', '20%']}
                        variant='outline'
                        text='Load More'
                        isLoading={fetching}
                        disabled={fetching}
                        loadingText='fetching...'
                        onClick={() => setPage((prev) => prev + 1)}
                    />
                )}
                {!offers.length && !fetching && (
                    <Unavailable message='No  offers available' />
                )}
            </Center>
        </Box>
    )
}

OffersList.defaultProps = {
    offers: [],
}

export default OffersList
