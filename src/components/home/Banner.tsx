import { Box, Heading, VStack } from '@chakra-ui/react'
import React from 'react'

function Banner() {
    return (
        <VStack
            alignItems='flex-start'
            justifyContent='center'
            minHeight='60vh'
            rounded='10px'
            bgImage='/images/images/lioncub.jpg'
            bgPosition='center'
            p='20px 60px'
            bgSize='cover'>
            <Box width={['100%', '100%', '40%']}>
                <Heading size='3xl'> More Travel Less Excuses</Heading>
            </Box>
        </VStack>
    )
}

export default Banner
