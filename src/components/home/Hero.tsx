import { Box, VStack } from '@chakra-ui/react'
import React from 'react'
import useDimensions from 'react-cool-dimensions'
import CustomImage from '../common/CustomImage'
import SearchBar from './SearchBar'

function Hero() {
    const { observe, width, height } = useDimensions({})
    return (
        <Box width='100%' height='auto' position='relative'>
            <Box
                ref={observe}
                width='100%'
                mx='auto'
                height={['35vh', '40vh', '50vh']}
                maxH={['35vh', '40vh', '50vh']}
                position='relative'>
                <CustomImage
                    src='/images/images/more-travel.png'
                    alt='Vacay Holiday Deals'
                    width={width}
                    height={height ? height : 500}
                    objectFit={['fill', 'fill', 'cover', 'cover', 'cover']}
                    layout='responsive'
                    priority
                />
                <VStack
                    position='absolute'
                    top='0'
                    left='0'
                    zIndex='10'
                    bgGradient='linear(to-b, blackAlpha.50, blackAlpha.200, blackAlpha.300)'
                    width='100%'
                    display={['none', 'none', 'none', 'flex', 'flex']}
                    alignItems='flex-start'
                    justifyContent='center'
                    height='100%'>
                    <Box width={['75%']} mx='auto'>
                        <Box
                            p='20px'
                            minHeight='20vh'
                            bg='whiteAlpha.800'
                            width={['100%', '100%', '70%', '60%', '30%']}
                            rounded='xl'
                            backdropFilter='blur(5px)'>
                            <SearchBar />
                        </Box>
                    </Box>
                </VStack>
            </Box>
            <Box
                position='relative'
                top='-3rem'
                p='20px'
                minHeight='auto'
                bg='white'
                width='95%'
                boxShadow='xl'
                mx='auto'
                display={['block', 'block', 'block', 'none']}
                rounded='xl'>
                <SearchBar />
            </Box>
        </Box>
    )
}

export default Hero
