import { Box, Heading, HStack, Skeleton, useMediaQuery } from '@chakra-ui/react'
import React, { lazy, Suspense, useEffect, useMemo } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { ErrorBoundary } from 'react-error-boundary'
import { Offer } from '@/types/index'
import { fetchRecommended } from '@/src/store/slices/Offers'
import Slider from '../common/Slider'
import ErrorHandler from '../common/ErrorHandler'
import { RootState, AppDispatch } from '@/src/store'

const FeaturedCard = lazy(() => import('../offers/FeaturedCard'))

type RecommendedListProps = {
    offer: Offer
}
function RecommendedList({ offer }: RecommendedListProps) {
    const dispatch = useDispatch<AppDispatch>()
    const [isLargeScreen] = useMediaQuery('(min-width: 768px)')
    const { recommended } = useSelector((state: RootState) => state.offers)

    useEffect(() => {
        if (offer?.overview) {
            dispatch(fetchRecommended(offer?.overview))
        }
    }, [offer?.overview, dispatch])

    const newRecommended = useMemo(() => {
        if (!recommended) return []
        return recommended && recommended.slice(1, recommended.length)
    }, [recommended])

    return (
        <ErrorBoundary FallbackComponent={ErrorHandler}>
            {newRecommended.length > 0 && (
                <Box position='relative' minHeight='10vh' my='2rem'>
                    <HStack
                        alignItems='center'
                        justifyContent='space-between'
                        mb='1rem'>
                        <Heading fontSize={['xl', '', '2xl']}>
                            Similar Offers
                        </Heading>
                    </HStack>

                    <Slider
                        slidesToShow={isLargeScreen ? 3 : 1.2}
                        slidesToScroll={isLargeScreen ? 2 : 1}
                        speed={500}
                        slideSize='md'
                        arrows={isLargeScreen ? true : false}>
                        {newRecommended.map((result: Offer, index: number) => {
                            const lastIndex =
                                index === newRecommended.length - 1
                            return (
                                <Suspense
                                    key={result?.id}
                                    fallback={
                                        <Skeleton
                                            height='20vh'
                                            width='100%'
                                            rounded='xl'
                                            mr={
                                                lastIndex
                                                    ? ''
                                                    : ['0.5rem', '', '0.5rem']
                                            }
                                        />
                                    }>
                                    <Box
                                        mr={
                                            lastIndex
                                                ? ''
                                                : ['0.5rem', '', '0.5rem']
                                        }>
                                        <FeaturedCard
                                            offer={result}
                                            size='md'
                                        />
                                    </Box>
                                </Suspense>
                            )
                        })}
                    </Slider>
                </Box>
            )}
        </ErrorBoundary>
    )
}

export default RecommendedList
