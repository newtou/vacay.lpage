import React from 'react'
import { Box } from '@chakra-ui/react'
import useDimensions from 'react-cool-dimensions'
import Slider from '@/src/components/common/Slider'
import CustomImage from '../common/CustomImage'

type Image = {
    id: string
    url: string
    pub_id: string
    alt: string
}

type ImageSlider = {
    images: Image[]
    initialSlide?: number
}

function ImageSlider({ images, initialSlide }: ImageSlider) {
    const { observe, width, height } = useDimensions()
    return (
        <Slider
            arrows
            infinite
            autoplay
            dots={false}
            speed={1000}
            slidesToShow={1}
            initialSlide={initialSlide}
            slidesToScroll={1}>
            {images &&
                images.map((img) => (
                    <Box
                        ref={observe}
                        key={img?.id}
                        height={['40vh', '40vh', '40vh', '50vh']}
                        width='100%'
                        rounded={['0', '0', 'xl']}>
                        <CustomImage
                            src={img?.url}
                            width={width ? width : 200}
                            height={height ? height : 200}
                            alt={img.pub_id}
                            objectFit='cover'
                            quality={100}
                            priority
                            rounded={['0', '0', 'xl']}
                        />
                    </Box>
                ))}
        </Slider>
    )
}

export default ImageSlider
