import React, { useEffect, useMemo, useState } from 'react'
import { Formik } from 'formik'
import * as yup from 'yup'
import {
    Alert,
    AlertIcon,
    Box,
    Button,
    Divider,
    Heading,
    HStack,
    Text,
} from '@chakra-ui/react'
import { useRouter } from 'next/router'
import { invoke } from '@/src/lib/axios.config'
import { useMultiStep } from '@/src/hooks/useMultiStep'
import { PersonalInfoForm, OfferInfoForm } from '../forms/offerlead'
import { Offer } from '@/types/index'
import ResizablePanel from '../common/ResizablePanel'
import Confirmation from './Confirmation'

type OfferLeadProps = {
    offer: Offer
}

type InitialValues = {
    name: string
    email: string
    number: string
    message?: string
    adult?: number
    child?: number
    departure?: string
    destination?: string
    budget?: string
}

function OfferLead({ offer }: OfferLeadProps) {
    const router = useRouter()
    const [error, setError] = useState<string | null>(null)

    const token = useMemo(() => {
        const { token } = router?.query
        if (!token) return null
        return token
    }, [router?.query])

    const initialValues: InitialValues = {
        name: '',
        destination: '',
        email: '',
        adult: 0,
        child: 0,
        message: '',
        departure: new Date().toISOString().split('T')[0],
        number: '',
        budget: '',
    }

    const validator = [
        yup.object().shape({
            name: yup.string().required('Name is required'),
            email: yup
                .string()
                .email('Invalid email')
                .required('Email is required'),
            number: yup.string(),
        }),
        yup.object().shape({
            message: yup.string(),
            adult: yup.number(),
            child: yup.number(),
            departure: yup.date().required('Departure is required'),
            destination: yup.string().required('Destination is required'),
            budget: yup.string().required('Budget is required'),
        }),
    ]

    useEffect(() => {
        if (error) {
            setTimeout(() => {
                setError(null)
            }, 4000)
        }
    }, [error])

    const { Active, activeStepIndex, next, prev, isLastStep, isFirstStep } =
        useMultiStep([PersonalInfoForm, OfferInfoForm])

    return (
        <Box>
            <Heading
                display={['none', 'none', 'none', 'block']}
                fontSize='2xl'
                mb='1rem'
                textAlign={token ? 'center' : 'left'}>
                {token ? 'Your enquiry has been sent' : 'Send a request'}
            </Heading>
            <Divider my='1rem' display={['none', 'none', 'none', 'block']} />
            {error && (
                <Alert status='error' rounded='10px' my='1rem'>
                    <AlertIcon />
                    <Text>{error}</Text>
                </Alert>
            )}
            <Formik
                initialValues={initialValues}
                validationSchema={validator[activeStepIndex]}
                enableReinitialize
                onSubmit={async (
                    values: InitialValues,
                    _: {
                        setSubmitting: (arg0: boolean) => void // eslint-disable-line
                        resetForm: () => void
                    }
                ) => {
                    if (!isLastStep) {
                        next()
                        return
                    }

                    _.setSubmitting(true)
                    const params = {
                        ...values,
                        offer: offer?.name,
                        offerID: offer?.id,
                    }

                    const { res, error } = await invoke(
                        'POST',
                        'enquiries',
                        params
                    )

                    if (error) {
                        _.setSubmitting(false)
                        setError('Enquiry was not sent successfully')
                        return
                    }

                    if (!res.token) {
                        _.setSubmitting(false)
                        setError('Enquiry was not sent successfully')
                        return
                    }

                    router.push(
                        `/offers/${offer?.slug}?token=${res?.token}`,
                        undefined,
                        { shallow: true }
                    )
                    _.setSubmitting(false)
                    _.resetForm()
                }}>
                {({ handleSubmit, errors, touched, isSubmitting }) => (
                    <ResizablePanel duration={0.2}>
                        {token ? (
                            <Confirmation token={token} />
                        ) : (
                            <form onSubmit={handleSubmit}>
                                <Active
                                    errors={errors}
                                    touched={touched}
                                    destinations={offer?.destinations}
                                />
                                <HStack>
                                    {!isFirstStep && (
                                        <Button
                                            my='1rem'
                                            type='button'
                                            width='100%'
                                            height='3rem'
                                            fontSize='lg'
                                            rounded='lg'
                                            onClick={prev}
                                            colorScheme='gray'>
                                            &larr; Back
                                        </Button>
                                    )}
                                    <Button
                                        my='1rem'
                                        color='white'
                                        type='submit'
                                        width='100%'
                                        height='3rem'
                                        fontSize='lg'
                                        rounded='lg'
                                        isLoading={isLastStep && isSubmitting}
                                        isDisabled={isLastStep && isSubmitting}
                                        bg='brand.base'
                                        _hover={{ bg: 'brand.primary.light' }}
                                        _focus={{
                                            bg: 'brand.primary.light',
                                            outline: 'none',
                                        }}
                                        _active={{
                                            bg: 'brand.primary.light',
                                            outline: 'none',
                                        }}>
                                        {isLastStep
                                            ? 'Send Request'
                                            : 'Continue'}{' '}
                                        &rarr;
                                    </Button>
                                </HStack>
                            </form>
                        )}
                    </ResizablePanel>
                )}
            </Formik>
        </Box>
    )
}

OfferLead.defaultProps = {
    offer: {},
}

export default OfferLead
