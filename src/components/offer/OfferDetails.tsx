import {
    Heading,
    Icon,
    Text,
    VStack,
    Tabs,
    TabList,
    TabPanels,
    Tab,
    TabPanel,
    Box,
    Badge,
} from '@chakra-ui/react'
import React from 'react'
import { HiOutlineMapPin } from 'react-icons/hi2'
import MarkdownReader from '../common/MarkdownReader'
import { Offer } from '@/types/index'
import Overflow from '../common/Overflow'

type OfferDetails = {
    offer: Offer
}

function OfferDetails({ offer }: OfferDetails) {
    const fields = [
        {
            label: 'Overview',
            content: offer?.overview,
        },
        {
            label: 'Itinerary',
            content: offer?.itinerary,
        },
        {
            label: 'Inclusions',
            content: offer?.inclexcl,
        },
        {
            label: 'Pricing',
            content: offer?.pricing,
        },
    ]

    return (
        <VStack
            my='1rem'
            alignItems='flex-start'
            spacing='3'
            width={['95%', '95%', '95%', '100%']}
            mx='auto'>
            <Box
                bg='white'
                p={['10px', '10px', '10px 20px']}
                width='full'
                rounded='xl'>
                <Heading size='lg' textTransform='capitalize' my='1rem'>
                    {offer?.name}
                </Heading>
                {offer?.locations && (
                    <Overflow>
                        {offer?.locations.map((location: { name: string }) => (
                            <Badge
                                key={location.name}
                                alignItems='center'
                                display='flex'
                                flexDir='row'
                                rounded='full'
                                p='10px 15px'
                                gap='2'
                                fontSize='md'
                                textTransform='capitalize'
                                bg='brand.primary.50'
                                color='brand.primary.700'
                                borderColor='brand.primary.100'
                                width='fit-content'>
                                <Icon as={HiOutlineMapPin} boxSize={5} />
                                <Text>{location.name}</Text>
                            </Badge>
                        ))}
                    </Overflow>
                )}
            </Box>

            <Box
                bg='#fff'
                p={['10px', '10px', '20px']}
                rounded='xl'
                width='full'>
                <Tabs isLazy variant='unstyled' borderColor='gray.100'>
                    <TabList
                        overflowX={['auto', 'auto', 'auto', 'hidden']}
                        borderBottom='2px solid'
                        pb='20px'
                        pt='10px'
                        borderColor='gray.50'>
                        {fields.map((field, index) => (
                            <Tab
                                _active={{ outline: 'none' }}
                                _focus={{ outline: 'none' }}
                                fontWeight='medium'
                                width='fit-content'
                                maxWidth='100%'
                                height='3rem'
                                _selected={{
                                    bg: 'brand.primary.50',
                                    color: 'brand.primary.700',
                                    rounded: 'full',
                                }}
                                alignSelf='flex-start'
                                key={index}>
                                <Heading
                                    display={['block']}
                                    fontSize={['md', '', 'lg']}>
                                    {field.label}
                                </Heading>
                            </Tab>
                        ))}
                    </TabList>

                    <TabPanels rounded='10px' bg='white'>
                        {fields.map((field) => (
                            <TabPanel
                                key={field.label}
                                minHeight='20vh'
                                p='15px 10px'
                                overflowX='auto'
                                width='full'>
                                {field.content ? (
                                    <MarkdownReader content={field.content} />
                                ) : (
                                    <Text
                                        as='p'
                                        textAlign='justify'
                                        fontSize='lg'
                                        mt='1rem'>
                                        Information not currently available
                                    </Text>
                                )}
                            </TabPanel>
                        ))}
                    </TabPanels>
                </Tabs>
            </Box>

            {offer?.moreInfo && (
                <Box
                    bg='white'
                    width='100%'
                    p={['10px', '10px', '20px']}
                    rounded='xl'>
                    <Heading fontSize={['xl', '', '2xl']} mb='1rem'>
                        More Information
                    </Heading>
                    <MarkdownReader content={offer?.moreInfo} />
                </Box>
            )}
        </VStack>
    )
}

OfferDetails.defaultProps = {
    offer: {},
}

export default OfferDetails
