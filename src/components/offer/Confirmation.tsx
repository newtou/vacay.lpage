import React, { useEffect, useState } from 'react'
import { useRouter } from 'next/router'
import { Button, Center, Heading, HStack, Image, Text } from '@chakra-ui/react'
import jwtDecode from 'jwt-decode'

type ConfirmationProps = {
    token: string | string[] | undefined
}

type Token = {
    name: string
    offer: string
    confirmed: boolean
}

function Confirmation({ token }: ConfirmationProps) {
    const router = useRouter()
    const [name, setName] = useState<string>('')
    const [offer, setOffer] = useState<string>('')
    const [confirmed, setConfirmed] = useState<boolean>(false)

    useEffect(() => {
        if (token && typeof token === 'string') {
            const { name, offer, confirmed } = jwtDecode<Token>(token)
            setName(name)
            setOffer(offer)
            setConfirmed(confirmed)
        }
    }, [token])

    if (!token) return null

    return (
        <Center
            flexDirection='column'
            width='100%'
            mx='auto'
            bg='whiteAlpha.700'
            p='20px'
            gap='3'
            rounded='lg'
            minHeight='50vh'>
            <Image
                src='/images/svg/success.svg'
                alt='Success image'
                objectFit='contain'
                height='30%'
                width='30%'
            />
            {router.basePath}
            <Heading
                fontWeight='bold'
                my='1rem'
                textAlign='center'
                fontSize={['2xl', '3xl', '2xl']}>
                {confirmed
                    ? 'Your Enquiry has been sent'
                    : 'Something went wrong, Try again'}
            </Heading>
            {confirmed ? (
                <Text fontSize='md' textAlign='center' width='100%'>
                    Hi <strong>{name}</strong>, your enquiry about the {offer}{' '}
                    offer, has been sent. Someone from our reservations team
                    with revert back, about your enquiry.
                </Text>
            ) : (
                <Text fontSize='md' textAlign='center' width='70%'>
                    Hi, your enquiry was not sent. Something went wrong, please
                    try and sent it again.
                </Text>
            )}

            <HStack justify='space-between' gap='4'>
                <Button
                    my='1rem'
                    colorScheme='gray'
                    color='black'
                    rounded='md'
                    onClick={() => {
                        router.back()
                    }}>
                    &larr; Resend enquiry
                </Button>
                <Button
                    my='1rem'
                    bg='brand.base'
                    color='white'
                    rounded='md'
                    onClick={() => {
                        router.push('/')
                    }}
                    _hover={{ bg: 'brand.primary.light' }}
                    _focus={{ bg: 'brand.primary.light', outline: 'none' }}
                    _active={{ bg: 'brand.primary.light', outline: 'none' }}>
                    More offers &rarr;
                </Button>
            </HStack>
        </Center>
    )
}

export default Confirmation
