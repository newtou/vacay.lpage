import React from 'react'
import {
    Icon,
    Button as ChakraBtn,
    ButtonProps as ChakraBtnProps,
    chakra,
} from '@chakra-ui/react'

interface ButtonProps extends ChakraBtnProps {
    children?: React.ReactNode | string
    text?: string
    icon?: any
    iconPosition?: 'left' | 'right'
}

const CustomBtn = chakra(ChakraBtn, {})

function Button({ text, icon, iconPosition, children, ...props }: ButtonProps) {
    return (
        <CustomBtn
            gap='1'
            width='fit-content'
            rounded='lg'
            fontSize='md'
            display='flex'
            minW='fit-content'
            alignItems='center'
            colorScheme={props.colorScheme || 'brand.primary'}
            flexDirection={iconPosition === 'right' ? 'row-reverse' : 'row'}
            {...props}>
            {icon && <Icon as={icon} mr='0.5rem' boxSize={6} />}
            <span>{text || children}</span>
        </CustomBtn>
    )
}

export default Button
