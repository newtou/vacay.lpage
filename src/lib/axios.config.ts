import axios, { Method } from 'axios'
import Cookies from 'js-cookie'

export async function invoke(
    method: Method,
    url: string,
    data?: { [key: string]: any },
    progressCb?: (progress: number) => void // eslint-disable-line
) {
    let config = {
        headers: {
            'Content-Type': 'application/json',
            SameSite: 'Secure',
        },
    }

    const baseUrl = process.env.SITE_URL
    const requestURL = `${baseUrl}/api/v1/${url}`

    const token = Cookies.get('auth-token')
    if (token) {
        config.headers['x-access-token'] = token
    }

    try {
        const { data: res, status } = await axios({
            method,
            url: requestURL,
            data,
            headers: config.headers,
            onUploadProgress: (progressEvent) => {
                if (progressCb) {
                    const percentCompleted = Math.round(
                        (progressEvent.loaded * 100) / progressEvent.total
                    )
                    progressCb(percentCompleted)
                }
            },
        })

        return { res, status, error: null }
    } catch (error) {
        if (error.response) {
            return { res: null, status: 500, error: error.response.data }
        } else if (error.request) {
            return {
                res: null,
                status: 500,
                error: 'Error: No response received from the request',
            }
        } else {
            return { res: null, status: 500, error: error.message }
        }
    }
}
