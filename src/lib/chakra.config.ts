import { extendTheme } from '@chakra-ui/react'
import { mode } from '@chakra-ui/theme-tools'

const breakpoints = {
    sm: '480px',
    md: '768px',
    lg: '976px',
    xl: '1440px',
}

const colors = {
    brand: {
        primary: {
            50: '#dcf6ff',
            100: '#aee0ff',
            200: '#7ecaff',
            300: '#4db4ff',
            400: '#229ffe',
            500: '#0d85e5',
            600: '#0068b3',
            700: '#004a81',
            800: '#002c50',
            900: '#001020',
        },
        accent: '#ffd416',
        dark: '#000411',
        base: '#0068b3',
        light: '#0086e6',
        darkPrimary: '#004a80',
    },
}

const config = {
    initialColorMode: 'light',
    useSystemColorMode: false,
}

const styles = {
    global: (props: Record<string, any>) => ({
        body: {
            color: mode('gray.800', 'whiteAlpha.900')(props),
            bg: mode('gray.100', 'gray.800')(props),
        },
    }),
}

export const theme = extendTheme({
    ...config,
    breakpoints,
    colors,
    styles,
    fonts: {
        heading: 'Figtree Variable, sans-serif',
        body: 'Figtree Variable, sans-serif',
        mono: 'monospace',
    },
})
