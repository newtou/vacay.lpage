import AlertProvider from './AlertProvider'
import CookieProvider from './CookieProvider'

function ContextProvider({ children }) {
    return (
        <CookieProvider>
            <AlertProvider>{children}</AlertProvider>
        </CookieProvider>
    )
}

export default ContextProvider
