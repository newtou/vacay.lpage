import React, { createContext, useEffect } from 'react'
import { useToast } from '@chakra-ui/react'
import PropTypes from 'prop-types'
import { useDispatch, useSelector } from 'react-redux'
import { clearAlerts } from '../store/slices/Alert'
import { RootState, AppDispatch } from '../store'

const AlertContext = createContext({})

function AlertProvider({ children }) {
    const toast = useToast()
    const dispatch = useDispatch<AppDispatch>()
    const { error, message } = useSelector((state: RootState) => state.alert)

    useEffect(() => {
        if (error) {
            toast({
                title: 'Error Notification',
                description: error,
                status: 'error',
                isClosable: true,
                position: 'bottom-left',
                duration: 4000,
            })
        }

        if (message) {
            toast({
                title: 'Success Notification',
                description: message,
                status: 'success',
                isClosable: true,
                position: 'bottom-left',
                duration: 4000,
            })
        }
        setTimeout(() => {
            dispatch(clearAlerts())
        }, 4000)
    }, [error, message, dispatch, toast])
    return <AlertContext.Provider value={{}}>{children}</AlertContext.Provider>
}

AlertProvider.propTypes = {
    children: PropTypes.element,
}

export default AlertProvider
