import React, { createContext, useState, useEffect, useContext } from 'react'

type CookieProviderProps = {
    children: React.ReactNode
}

type CookieContextType = {
    showCookieBanner: boolean
    setCookieConsent: (consent: string) => void // eslint-disable-line no-unused-vars
    consent: string
    setShowCookieBanner: (show: boolean) => void // eslint-disable-line no-unused-vars
}

const CookieContext = createContext<CookieContextType>({
    showCookieBanner: false,
    setCookieConsent: () => {}, // eslint-disable-line no-empty-function
    consent: '',
    setShowCookieBanner: () => {}, // eslint-disable-line no-empty-function
})

function CookieProvider({ children }: CookieProviderProps) {
    const [consent, setConsent] = useState<string>('')
    const [showCookieBanner, setShowCookieBanner] = useState(false)

    const setCookieConsent = (consent: string) => {
        localStorage.setItem('cookieConsent', consent)
        setConsent(consent)
        setShowCookieBanner(false)
    }

    useEffect(() => {
        if (consent) {
            setShowCookieBanner(false)
        } else {
            setShowCookieBanner(true)
        }
    }, [consent])

    const cookieConsent =
        typeof window !== 'undefined' && localStorage.getItem('cookieConsent')

    useEffect(() => {
        if (cookieConsent) {
            setConsent(cookieConsent)
        }
    }, [cookieConsent])

    const value: CookieContextType = {
        showCookieBanner,
        consent,
        setCookieConsent,
        setShowCookieBanner,
    }

    return (
        <CookieContext.Provider value={value}>
            {children}
        </CookieContext.Provider>
    )
}

export const useCookie = () => useContext(CookieContext)

export default CookieProvider
