import { configureStore } from '@reduxjs/toolkit'
import OfferReducer from './slices/Offers'
import GalleryReducer from './slices/Media'
import UserReducer from './slices/Users'
import EnquiriesReducer from './slices/Enquiries'
import AlertReducer from './slices/Alert'
import SeoReducer from './slices/Seo'
import AuthReducer from './slices/Auth'

const store = configureStore({
    reducer: {
        offers: OfferReducer,
        gallery: GalleryReducer,
        users: UserReducer,
        enquiries: EnquiriesReducer,
        alert: AlertReducer,
        seo: SeoReducer,
        auth: AuthReducer,
    },
})

export type RootState = ReturnType<typeof store.getState>
export type AppDispatch = typeof store.dispatch
export default store
