import { createSlice, createAsyncThunk } from '@reduxjs/toolkit'
import { setError, setMessage } from './Alert'
import { invoke } from '@/src/lib/axios.config'

export const createEnquiry = createAsyncThunk(
    'Enquiry/create',
    async (params: { [key: string]: any }, thunk) => {
        const { res, error } = await invoke('POST', 'enquiries', params)

        if (error) {
            thunk.dispatch(setError(error || 'Something went wrong'))
            return thunk.rejectWithValue(error)
        }

        thunk.dispatch(setMessage(res.message))
        return thunk.fulfillWithValue(res.enquiry)
    }
)

export const getEnquiries = createAsyncThunk(
    'Enquiries/getAll',
    async (params: {}, thunk) => {
        const search = new URLSearchParams({
            ...params,
        })

        const query = search.toString()
        const url = `enquiries?${query}`
        const { res, error } = await invoke('GET', url)

        if (error) {
            thunk.dispatch(setError(error || 'Something went wrong'))
            return thunk.rejectWithValue(error)
        }

        return thunk.fulfillWithValue(res.enquiries)
    }
)

export const removeEnquiry = createAsyncThunk(
    'Enquiries/delete',
    async (id: string, thunk) => {
        const { res, error } = await invoke('DELETE', `enquiries/${id}`)
        if (error) {
            thunk.dispatch(
                setError(error.response?.data || 'Something went wrong')
            )
            return thunk.rejectWithValue(error)
        }
        thunk.dispatch(setMessage(res.message))
        return thunk.fulfillWithValue(id)
    }
)

const enquiriesSlice = createSlice({
    name: 'enquiries',
    initialState: {
        enquiries: [],
        loading: false,
    },
    extraReducers: (builder) => {
        builder.addCase(createEnquiry.pending, (state) => {
            return {
                ...state,
                loading: true,
            }
        })

        builder.addCase(createEnquiry.fulfilled, (state, action) => {
            return {
                ...state,
                loading: false,
                enquiries: [action.payload, ...state.enquiries],
            }
        })

        builder.addCase(
            createEnquiry.rejected,
            (state: any, action: { payload: any }) => {
                return {
                    ...state,
                    loading: false,
                    error: action.payload,
                }
            }
        )

        builder.addCase(getEnquiries.pending, (state) => {
            return {
                ...state,
                loading: true,
            }
        })

        builder.addCase(getEnquiries.fulfilled, (state, action) => {
            return {
                ...state,
                enquiries: action.payload,
                loading: false,
            }
        })

        builder.addCase(getEnquiries.rejected, (state) => {
            return {
                ...state,
                loading: false,
            }
        }),
            builder.addCase(removeEnquiry.pending, (state) => {
                return {
                    ...state,
                    loading: true,
                }
            })

        builder.addCase(removeEnquiry.fulfilled, (state, action) => {
            return {
                ...state,
                enquiries: state.enquiries.filter(
                    (enquiry) => enquiry.id !== action.payload
                ),
                loading: false,
            }
        })

        builder.addCase(removeEnquiry.rejected, (state) => {
            return {
                ...state,
                loading: false,
            }
        })
    },
    reducers: undefined,
})

export default enquiriesSlice.reducer
