import { createSlice, createAsyncThunk } from '@reduxjs/toolkit'
import { invoke } from '@/src/lib/axios.config'
import { Seo } from '@/types/index'
import { setError, setMessage } from './Alert'

export type InitialState = {
    seo: Seo
    loading: boolean
}

const initialState: InitialState = {
    seo: null,
    loading: false,
}

export const createSeo = createAsyncThunk(
    'Seo/create',
    async (params: { [key: string]: any }, thunk) => {
        const { res, error } = await invoke(
            'POST',
            `seo/${params.mapId}`,
            params.seo
        )

        if (error) {
            thunk.dispatch(setError(error || 'Something went wrong'))
            return thunk.rejectWithValue(error)
        }

        thunk.dispatch(setMessage(res.message))
        return thunk.fulfillWithValue(res.seo)
    }
)

export const fetchSeo = createAsyncThunk(
    'Seo/fetch',
    async (id: string, thunk) => {
        const { res, error } = await invoke('GET', `seo/${id}`)

        if (error) {
            thunk.dispatch(setError(error || 'Something went wrong'))
            return thunk.rejectWithValue(error)
        }

        return thunk.fulfillWithValue(res.seo)
    }
)

export const updateSeo = createAsyncThunk(
    'Seo/update',
    async (params: { [key: string]: any }, thunk) => {
        const { res, error } = await invoke(
            'PUT',
            `seo/${params.id}`,
            params.update
        )
        if (error) {
            thunk.dispatch(
                setError(error.response?.data || 'Something went wrong')
            )
            return thunk.rejectWithValue(error)
        }
        thunk.dispatch(setMessage(res.message))
        return thunk.fulfillWithValue(res.seo)
    }
)
export const removeSeo = createAsyncThunk(
    'Seo/delete',
    async (id: string, thunk) => {
        const { error, res } = await invoke('DELETE', `seo/${id}`)
        if (error) {
            thunk.dispatch(
                setError(error.response?.data || 'Something went wrong')
            )
            return thunk.rejectWithValue(error)
        }
        thunk.dispatch(setMessage(res.message))
        return thunk.fulfillWithValue(id)
    }
)

export const offerSlice = createSlice({
    name: 'SEO',
    initialState,
    reducers: {
        clearSeo: (state, action) => {
            const { payload } = action
            return {
                ...state,
                offers: payload,
                loading: false,
            }
        },
    },

    extraReducers: (builder) => {
        builder.addCase(createSeo.pending, (state) => {
            return {
                ...state,
                loading: true,
            }
        })

        builder.addCase(createSeo.fulfilled, (state, action) => {
            return {
                ...state,
                loading: false,
                seo: action.payload,
            }
        })

        builder.addCase(createSeo.rejected, (state) => {
            return {
                ...state,
                loading: false,
            }
        })

        builder.addCase(fetchSeo.pending, (state) => {
            return {
                ...state,
                loading: true,
            }
        })

        builder.addCase(fetchSeo.fulfilled, (state, action) => {
            return {
                ...state,
                seo: action.payload,
                loading: false,
            }
        })

        builder.addCase(fetchSeo.rejected, (state) => {
            return {
                ...state,
                loading: false,
            }
        })

        builder.addCase(updateSeo.pending, (state) => {
            return {
                ...state,
                loading: true,
            }
        })
        builder.addCase(updateSeo.fulfilled, (state, action) => {
            return {
                ...state,
                offers:
                    state.seo.id === action.payload.id
                        ? action.payload
                        : state.seo,
                loading: false,
            }
        })
        builder.addCase(updateSeo.rejected, (state) => {
            return {
                ...state,
                loading: false,
            }
        })
        builder.addCase(removeSeo.pending, (state) => {
            return {
                ...state,
                loading: true,
            }
        })
        builder.addCase(removeSeo.fulfilled, (state, action) => {
            return {
                ...state,
                seo: state.seo.id === action.payload ? null : state.seo,
                loading: false,
            }
        })
        builder.addCase(removeSeo.rejected, (state) => {
            return {
                ...state,
                loading: false,
            }
        })
    },
})

export const { clearSeo } = offerSlice.actions

export default offerSlice.reducer
