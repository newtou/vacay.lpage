import { createSlice } from '@reduxjs/toolkit'

const initialState = {
    gallery: [],
    progress: 0,
}

export const gallerySlice = createSlice({
    name: 'Gallery',
    initialState,
    reducers: {
        setGallery: (state, action) => {
            const { payload } = action
            return {
                ...state,
                gallery: [...payload, ...state.gallery],
            }
        },

        deleteFromGallery: (state, action) => {
            const { payload } = action

            return {
                ...state,
                gallery: state.gallery.filter(
                    (image) => image?.pubId !== payload
                ),
            }
        },
        setProgress: (state, action) => {
            const { payload } = action
            return {
                ...state,
                progress: payload,
            }
        },

        clearProgress: (state) => {
            return {
                ...state,
                progress: 0,
            }
        },

        clearGallery: () => ({
            gallery: [],
            progress: 0,
        }),
    },
})

export const {
    setGallery,
    clearGallery,
    deleteFromGallery,
    setProgress,
    clearProgress,
} = gallerySlice.actions

export default gallerySlice.reducer
