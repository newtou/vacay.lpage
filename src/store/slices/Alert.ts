import { createSlice } from '@reduxjs/toolkit'

type InitialState = {
    message: string | null
    error: string | null
    authError: string | null
}

const initialState: InitialState = {
    message: null,
    error: null,
    authError: null,
}

const AlertSlice = createSlice({
    name: 'alert',
    initialState,
    reducers: {
        setMessage: (state, action) => {
            return {
                ...state,
                message: action.payload,
            }
        },
        setError: (state, action) => {
            return {
                ...state,
                error: action.payload,
            }
        },
        setAuthError: (state, action) => {
            return {
                ...state,
                authError: action.payload,
            }
        },
        clearAlerts: (state) => {
            return {
                ...state,
                message: null,
                error: null,
                authError: null,
            }
        },
    },
})

export const { setMessage, setError, setAuthError, clearAlerts } =
    AlertSlice.actions
export default AlertSlice.reducer
