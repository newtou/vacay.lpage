import { createAsyncThunk, createSlice } from '@reduxjs/toolkit'
import { invoke } from '@/src/lib/axios.config'
import { User } from '@/types/index'

type InitialState = {
    me: User | null
    loading: boolean
}

const initialState: InitialState = {
    me: null,
    loading: false,
}

export const fetchMe = createAsyncThunk(
    'Auth/fetchMe',
    async (id: string, thunk) => {
        const { res, error } = await invoke('GET', `auth/me/${id}`)
        if (error) {
            return thunk.rejectWithValue(error)
        }

        return thunk.fulfillWithValue(res.user)
    }
)

const AuthSlice = createSlice({
    name: 'Auth',
    initialState,
    reducers: {
        clearAuth: (state) => ({
            ...state,
            me: null,
            loading: false,
        }),
    },
    extraReducers: (builder) => {
        builder.addCase(fetchMe.pending, (state) => {
            return {
                ...state,
                loading: true,
            }
        })
        builder.addCase(fetchMe.fulfilled, (state, action) => {
            return {
                ...state,
                me: action.payload,
                loading: false,
            }
        })
        builder.addCase(fetchMe.rejected, (state) => {
            return {
                ...state,
                loading: false,
            }
        })
    },
})

export const { clearAuth } = AuthSlice.actions
export default AuthSlice.reducer
