import { invoke } from '@/src/lib/axios.config'
import { Offer, OffersQueryParams } from '@/types/index'
import { createSlice, createAsyncThunk } from '@reduxjs/toolkit'
import { setError, setMessage } from './Alert'

type ActiveOffers = {
    offers: Offer[]
    count: number
    totalPages: number
    hasMore: boolean
    currentPage: number
}

export type InitialState = {
    offers: Offer[]
    results: Offer[]
    popular: { offer: Offer; count: number }[]
    recommended: Offer[]
    loading: boolean
    fetching: boolean
    activeOffers: ActiveOffers
}

const initialState: InitialState = {
    offers: [],
    results: [],
    popular: [],
    recommended: [],
    loading: false,
    fetching: false,
    activeOffers: {
        offers: [],
        count: Infinity,
        totalPages: Infinity,
        hasMore: false,
        currentPage: 1,
    },
}

export const createOffer = createAsyncThunk(
    'Offer/create',
    async (params: { [key: string]: any }, thunk) => {
        const { res, error } = await invoke('POST', 'offers', params)

        if (error) {
            thunk.dispatch(setError(error || 'Something went wrong'))
            return thunk.rejectWithValue(error)
        }

        thunk.dispatch(setMessage(res.message))
        return thunk.fulfillWithValue(res.offer)
    }
)

export const fetchOffers = createAsyncThunk(
    'Offers/fetch',
    async (params: OffersQueryParams, thunk) => {
        const search = new URLSearchParams(
            JSON.parse(JSON.stringify(params))
        ).toString()

        const url = `offers?${search}`
        const { res, error } = await invoke('GET', url)

        if (error) {
            thunk.dispatch(setError(error || 'Something went wrong'))
            return thunk.rejectWithValue(error)
        }

        return thunk.fulfillWithValue(res.offers)
    }
)

export const fetchActiveOffers = createAsyncThunk(
    'Offers/active',
    async (params: OffersQueryParams, thunk) => {
        const query = new URLSearchParams(
            JSON.parse(JSON.stringify(params))
        ).toString()
        const url = `offers/active?${query}`
        const { res, error } = await invoke('GET', url)

        if (error) {
            thunk.dispatch(setError(error || 'Something went wrong'))
            return thunk.rejectWithValue(error)
        }

        return thunk.fulfillWithValue(res)
    }
)

export const updateOffer = createAsyncThunk(
    'Offers/update',
    async (params: { [key: string]: any }, thunk) => {
        const { res, error } = await invoke(
            'PUT',
            `offers/${params.id}`,
            params.update
        )
        if (error) {
            thunk.dispatch(
                setError(error.response?.data || 'Something went wrong')
            )
            return thunk.rejectWithValue(error)
        }
        thunk.dispatch(setMessage(res.message))
        return thunk.fulfillWithValue(res.offer)
    }
)
export const removeOffer = createAsyncThunk(
    'Offers/delete',
    async (id: string, thunk) => {
        const { error, res } = await invoke('DELETE', `offers/${id}`)
        if (error) {
            thunk.dispatch(
                setError(error.response?.data || 'Something went wrong')
            )
            return thunk.rejectWithValue(error)
        }
        thunk.dispatch(setMessage(res.message))
        return thunk.fulfillWithValue(id)
    }
)

export const search = createAsyncThunk(
    'Offers/search',
    async (params: { [key: string]: any }, thunk) => {
        const query = new URLSearchParams({ ...params }).toString()
        const { error, res } = await invoke('GET', `offers/search?${query}`)
        if (error) {
            return thunk.fulfillWithValue([])
        }

        return thunk.fulfillWithValue(res.results)
    }
)

export const fetchPopular = createAsyncThunk(
    'Offers/popular',
    async (_: any, thunk) => {
        const { error, res } = await invoke('GET', `offers/popular`)
        if (error) {
            return thunk.fulfillWithValue([])
        }
        return thunk.fulfillWithValue(res.popular)
    }
)

export const fetchRecommended = createAsyncThunk(
    'Offers/recommended',
    async (text: string, thunk) => {
        const { error, res } = await invoke('POST', `offers/recommended`, {
            text,
        })
        if (error) {
            return thunk.fulfillWithValue([])
        }
        return thunk.fulfillWithValue(res.recommended)
    }
)

export const offerSlice = createSlice({
    name: 'offers',
    initialState,
    reducers: {
        searchOffers: (state, action) => {
            const { payload } = action

            return {
                ...state,
                results: payload,
            }
        },

        resetSearchOffers: (state) => {
            return {
                ...state,
                results: [],
            }
        },

        setLoading: (state, action) => {
            const { payload } = action

            return {
                ...state,
                loading: payload,
            }
        },

        clearResults: (state) => ({
            ...state,
            results: [],
        }),

        clearOffers: (state) => ({
            ...state,
            offers: [],
            loading: false,
        }),
    },

    extraReducers: (builder) => {
        builder.addCase(createOffer.pending, (state) => {
            return {
                ...state,
                loading: true,
            }
        })

        builder.addCase(createOffer.fulfilled, (state, action) => {
            return {
                ...state,
                loading: false,
                offers: [action.payload, ...state.offers],
            }
        })

        builder.addCase(createOffer.rejected, (state) => {
            return {
                ...state,
                loading: false,
            }
        })

        builder.addCase(fetchOffers.pending, (state) => {
            return {
                ...state,
                fetching: true,
            }
        })

        builder.addCase(fetchOffers.fulfilled, (state, action) => {
            return {
                ...state,
                offers: action.payload,
                fetching: false,
            }
        })

        builder.addCase(fetchOffers.rejected, (state) => {
            return {
                ...state,
                fetching: false,
            }
        })

        builder.addCase(updateOffer.pending, (state) => {
            return {
                ...state,
                loading: true,
            }
        })
        builder.addCase(updateOffer.fulfilled, (state, action) => {
            return {
                ...state,
                offers: state.offers.map((offer) => {
                    if (offer.id === action.payload.id) {
                        return action.payload
                    }
                    return offer
                }),
                loading: false,
            }
        })
        builder.addCase(updateOffer.rejected, (state) => {
            return {
                ...state,
                loading: false,
            }
        })
        builder.addCase(removeOffer.pending, (state) => {
            return {
                ...state,
                loading: true,
            }
        })
        builder.addCase(removeOffer.fulfilled, (state, action) => {
            return {
                ...state,
                offers: state.offers.filter(
                    (offer) => offer.id !== action.payload
                ),
                loading: false,
            }
        })

        builder.addCase(removeOffer.rejected, (state) => {
            return {
                ...state,
                loading: false,
            }
        })

        builder.addCase(search.pending, (state) => {
            return {
                ...state,
                loading: true,
            }
        })
        builder.addCase(search.fulfilled, (state, action) => {
            return {
                ...state,
                results: action.payload,
                loading: false,
            }
        })
        builder.addCase(search.rejected, (state) => {
            return {
                ...state,
                loading: false,
                results: [],
            }
        })
        builder.addCase(fetchPopular.pending, (state) => {
            return {
                ...state,
                fetching: true,
            }
        })
        builder.addCase(fetchPopular.fulfilled, (state, action) => {
            return {
                ...state,
                fetching: false,
                popular: action.payload,
            }
        })
        builder.addCase(fetchPopular.rejected, (state) => {
            return {
                ...state,
                popular: [],
                fetching: false,
            }
        })
        builder.addCase(fetchRecommended.pending, (state) => {
            return {
                ...state,
                fetching: true,
            }
        })
        builder.addCase(fetchRecommended.fulfilled, (state, action) => {
            return {
                ...state,
                fetching: false,
                recommended: action.payload,
            }
        })
        builder.addCase(fetchRecommended.rejected, (state) => {
            return {
                ...state,
                recommended: [],
                fetching: false,
            }
        })
        builder.addCase(fetchActiveOffers.pending, (state) => {
            return {
                ...state,
                fetching: true,
            }
        })

        builder.addCase(fetchActiveOffers.fulfilled, (state, action) => {
            return {
                ...state,
                fetching: false,
                activeOffers: {
                    ...state.activeOffers,
                    offers: [
                        ...state.activeOffers.offers,
                        ...action.payload.offers,
                    ],
                    hasMore: action.payload.hasMore,
                    totalPages: action.payload.totalPages,
                    count: action.payload.count,
                    currentPage: action.payload.currentPage,
                },
            }
        })

        builder.addCase(fetchActiveOffers.rejected, (state) => {
            return {
                ...state,
                fetching: false,
            }
        })
    },
})

export const { clearOffers, clearResults, searchOffers, resetSearchOffers } =
    offerSlice.actions

export default offerSlice.reducer
