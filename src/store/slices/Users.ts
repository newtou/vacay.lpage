import { createSlice, createAsyncThunk } from '@reduxjs/toolkit'
import { invoke } from '@/src/lib/axios.config'
import { setMessage } from './Alert'
import { User } from '@/types/index'

type InitialState = {
    users: User[]
    loading: boolean
    fetching: boolean
}

const initialState: InitialState = {
    users: [],
    loading: false,
    fetching: false,
}

export const registerAllowedUsers = createAsyncThunk(
    'Users/register',
    async (params: User, thunk) => {
        const { error, res } = await invoke('POST', 'users/register', params)

        if (error) {
            thunk.dispatch(setMessage(error || 'Something went wrong'))
            return thunk.rejectWithValue(error)
        }

        thunk.dispatch(setMessage(res.message))
        return thunk.fulfillWithValue(res.user)
    }
)

export const createUser = createAsyncThunk(
    'Users/create',
    async (params: User, thunk) => {
        const { error, res } = await invoke('POST', 'users', params)

        if (error) {
            thunk.dispatch(setMessage(error || 'Something went wrong'))
            return thunk.rejectWithValue(error)
        }

        thunk.dispatch(setMessage(res.message))
        return thunk.fulfillWithValue(res.user)
    }
)
export const fetchUsers = createAsyncThunk(
    'Users/fetchUsers',
    async (params: { [key: string]: any }, thunk) => {
        const query = new URLSearchParams(params).toString()
        const { error, res } = await invoke('GET', `users?${query}`, {})

        if (error) {
            thunk.dispatch(setMessage(error || 'Something went wrong'))
            return thunk.rejectWithValue(error)
        }

        return thunk.fulfillWithValue(res.users)
    }
)

export const updateUser = createAsyncThunk(
    'Users/update',
    async (params: User, thunk) => {
        const { error, res } = await invoke('PUT', `users/${params.id}`, params)

        if (error) {
            thunk.dispatch(setMessage(error || 'Something went wrong'))
            return thunk.rejectWithValue(error)
        }

        thunk.dispatch(setMessage(res.message))
        return thunk.fulfillWithValue(res.user)
    }
)
export const deleteUser = createAsyncThunk(
    'Users/delete',
    async (id: string, thunk) => {
        const { error, res } = await invoke('DELETE', `users/${id}`)

        if (error) {
            thunk.dispatch(setMessage(error || 'Something went wrong'))
            return thunk.rejectWithValue(error)
        }

        thunk.dispatch(setMessage(res.message))
        return thunk.fulfillWithValue(id)
    }
)

export const userSlice = createSlice({
    name: 'Users',
    initialState,
    reducers: {
        clearUsers: () => ({
            users: [],
            loading: false,
            fetching: false,
        }),
    },
    extraReducers: (builder) => {
        builder.addCase(createUser.pending, (state) => {
            return {
                ...state,
                loading: true,
            }
        })

        builder.addCase(createUser.fulfilled, (state, action) => {
            return {
                ...state,
                loading: false,
                users: [action.payload, ...state.users],
            }
        })

        builder.addCase(createUser.rejected, (state) => {
            return {
                ...state,
                fetching: false,
            }
        })

        builder.addCase(fetchUsers.pending, (state) => {
            return {
                ...state,
                fetching: true,
            }
        })

        builder.addCase(fetchUsers.fulfilled, (state, action) => {
            return {
                ...state,
                fetching: false,
                users: action.payload,
            }
        })

        builder.addCase(fetchUsers.rejected, (state) => {
            return {
                ...state,
                fetching: false,
            }
        })

        builder.addCase(updateUser.pending, (state) => {
            return {
                ...state,
                loading: true,
            }
        })

        builder.addCase(updateUser.fulfilled, (state, action) => {
            return {
                ...state,
                loading: false,
                users: state.users.map((user: User) => {
                    if (user.id === action.payload.id) {
                        return action.payload
                    }
                    return user
                }),
            }
        })

        builder.addCase(updateUser.rejected, (state) => {
            return {
                ...state,
                loading: false,
            }
        })

        // delete user
        builder.addCase(deleteUser.pending, (state) => {
            return {
                ...state,
                deleting: true,
            }
        })

        builder.addCase(deleteUser.fulfilled, (state, action) => {
            return {
                ...state,
                deleting: false,
                users: state.users.filter(
                    (user: User) => user.id !== action.payload
                ),
            }
        })

        builder.addCase(deleteUser.rejected, (state) => {
            return {
                ...state,
                deleting: false,
            }
        })
    },
})

export const { clearUsers } = userSlice.actions

export default userSlice.reducer
