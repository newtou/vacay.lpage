import React from 'react'
import { Box } from '@chakra-ui/react'
import Script from 'next/script'
import { NextSeo } from 'next-seo'
import { motion } from 'framer-motion'
import Navbar from '../src/components/common/Navbar'
import Footer from '../src/components/common/Footer'
import WhatsappWidget from '@/src/components/common/WhatsappWidget'
import { OpenGraphMedia } from 'next-seo/lib/types'
// import { useCookie } from '@/src/context/CookieProvider'
// import CookieBanner from '../src/components/common/CookieBanner'

export type LayoutProps = {
    children: React.ReactNode
    title: string
    keywords?: string
    description?: string
    url?: string
    imageurl?: string | OpenGraphMedia
    hasHeader?: boolean
    hasFooter?: boolean
}

function Layout({
    children,
    title,
    keywords,
    description,
    url,
    imageurl,
    hasHeader,
    hasFooter,
}: LayoutProps) {
    // const { showCookieBanner } = useCookie()

    return (
        <Box
            as={motion.div}
            width='100%'
            position='relative'
            initial={{ opacity: 0.3 }}
            animate={{ opacity: 1 }}>
            <NextSeo
                title={title}
                description={description}
                twitter={{
                    handle: '@Vacay_Deals',
                    cardType: 'summary_large_image',
                    site: url,
                }}
                canonical={url}
                openGraph={{
                    url,
                    title,
                    description,
                    type: 'website',
                    locale: 'en_IE',
                    images: [imageurl] as OpenGraphMedia[],
                    site_name: 'Vacay Holiday Deals',
                }}
                facebook={{
                    appId: '2270052476457207',
                }}
                additionalMetaTags={[
                    {
                        property: 'keywords',
                        content: keywords,
                    },
                    {
                        property: 'google-site-verification',
                        content: 'hT25o7UHiE7SnvKbaR_AtYgDyWH8D9BBJaeTIBGPhE8',
                    },
                    {
                        property: 'og:image',
                        content: imageurl as string,
                    },
                    {
                        property: 'twitter:image',
                        content: imageurl as string,
                    },
                ]}
                additionalLinkTags={[
                    {
                        rel: 'icon',
                        type: 'image/svg',
                        href: '/vacay.svg',
                    },
                ]}
            />
            <Script
                strategy='afterInteractive'
                src={`https://www.googletagmanager.com/gtag/js?id=${process.env.GA_TRACKING_ID}`}
            />
            <Script
                id='gtm'
                strategy='afterInteractive'
                dangerouslySetInnerHTML={{
                    __html: `
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());
            gtag('config', '${process.env.GA_TRACKING_ID}', {
              page_path: window.location.pathname,
            });
          `,
                }}
            />
            {hasHeader && <Navbar />}
            <Box width='100%' height='auto' minHeight='60vh'>
                {/* {showCookieBanner && <CookieBanner />} */}
                {children}
            </Box>

            <Box>
                {hasFooter && <Footer />}
                <WhatsappWidget />
            </Box>
        </Box>
    )
}

Layout.defaultProps = {
    title: 'Vacay Holiday Deals',
    description:
        'Vacay holiday deals provide a wide range of vacation packages and total destination management to clients. But unlike all other tour companies in the market we have a product that provides an easy, secure and affordable pre-payment scheme through which the client can pay for the service in installments',
    keywords:
        'Vacay, Vacay Holiday Deals, Travel, Tour Kenya, travel packages, visit Kenya, Tour company, Kenya, Tourism, Maasai Mara, Mombasa, Kenyan Beach, Dubai, Singapore, Safari, travel agencies in Kenya',
    url: 'https://offers.vacay.co.ke',
    imageurl:
        'https://firebasestorage.googleapis.com/v0/b/newtou-portfolio.appspot.com/o/projects%2Fvacay3.png?alt=media&token=996a6a4a-49ff-4e3e-8397-9f538cae056f',
    hasHeader: true,
    hasFooter: true,
}

export default Layout
