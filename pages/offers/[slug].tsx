import React from 'react'
import Layout from '../Layout'
import {
    Box,
    Breadcrumb,
    BreadcrumbItem,
    Button,
    Stack,
    Icon,
    Text,
} from '@chakra-ui/react'
import OfferDetails from '@/src/components/offer/OfferDetails'
import OfferLead from '@/src/components/offer/OfferLead'
import RecommendedList from '@/src/components/offer/RecommendedList'
import prisma from '../api/lib/prisma'
import { Offer } from '@/types/index'
import OfferLeadDrawr from '@/src/components/drawrs/offerlead/OfferLeadDrawr'
import { ErrorBoundary } from 'react-error-boundary'
import ErrorHandler from '@/src/components/common/ErrorHandler'
import ImageGrid from '@/src/components/common/ImageGrid'

import { HiOutlineHome, HiChevronRight } from 'react-icons/hi2'
import Link from 'next/link'
import { motion } from 'framer-motion'

type OfferPageProps = {
    offer: Offer
}
function OfferPage({ offer }: OfferPageProps) {
    return (
        <Layout
            title={offer?.seo?.title}
            url={`${process.env.SITE_URL}/offers/${offer?.slug}`}
            description={offer?.seo?.description}
            keywords={offer?.seo?.keywords}
            imageurl={offer?.seo?.imageUrl}>
            <Box width={['100%', '100%', '95%', '95%', '90%', '75%']} mx='auto'>
                <Box my='1rem' px={['10px', '5px', '0']}>
                    <Breadcrumb
                        alignItems='center'
                        spacing='8px'
                        separator={<Icon as={HiChevronRight} boxSize={4} />}>
                        <BreadcrumbItem cursor='pointer'>
                            <Link href='/' passHref>
                                <Icon as={HiOutlineHome} boxSize={5} />
                            </Link>
                        </BreadcrumbItem>

                        <BreadcrumbItem>
                            <Text
                                color='gray.500'
                                fontSize='lg'
                                mb='0.5rem'
                                textUnderlineOffset={2}>
                                {offer?.name}
                            </Text>
                        </BreadcrumbItem>
                    </Breadcrumb>
                </Box>
                <ErrorBoundary FallbackComponent={ErrorHandler}>
                    <Stack
                        direction={[
                            'column',
                            'column',
                            'column',
                            'column',
                            'row',
                        ]}
                        gap='1rem'
                        mx='auto'
                        my={['0', '0', '1rem']}>
                        <ErrorBoundary FallbackComponent={ErrorHandler}>
                            <Box
                                width={['100%', '100%', '100%', '100%', '65%']}>
                                <ImageGrid
                                    images={offer?.images}
                                    modalTitle={offer?.name}
                                />
                                <OfferDetails offer={offer} />
                                <Box
                                    display={[
                                        'none',
                                        'none',
                                        'none',
                                        'block',
                                        'block',
                                    ]}>
                                    <RecommendedList offer={offer} />
                                </Box>
                            </Box>
                        </ErrorBoundary>

                        <Box
                            position='relative'
                            height='auto'
                            width={['100%', '100%', '100%', '100%', '35%']}
                            mx='auto'>
                            <Box
                                display={['none', 'none', 'none', 'block']}
                                bg='white'
                                position='sticky'
                                top='8rem'
                                left='0'
                                zIndex={10}
                                p='20px 30px'
                                rounded='xl'
                                shadow='lg'
                                borderColor='gray.100'>
                                <OfferLead offer={offer} />
                            </Box>
                        </Box>
                    </Stack>

                    <OfferLeadDrawr
                        renderTrigger={(onOpen) => (
                            <Button
                                as={motion.button}
                                whileHover={{ scale: 1.1 }}
                                whileTap={{ scale: 0.9 }}
                                display={['block', 'block', 'block', 'none']}
                                position='fixed'
                                zIndex='20'
                                bottom='2.5rem'
                                left={['1rem', '2rem', '4rem']}
                                width='70%'
                                rounded='full'
                                height='3.5rem'
                                boxShadow='2xl'
                                colorScheme='brand.primary'
                                fontSize='lg'
                                color='white'
                                onClick={onOpen}>
                                Send Enquiry
                            </Button>
                        )}
                    />
                </ErrorBoundary>
            </Box>
        </Layout>
    )
}

export async function getStaticPaths() {
    const offers = await prisma.offers.findMany({
        where: {
            active: true,
        },
    })
    const paths = offers.map((offer) => ({
        params: { slug: offer.slug },
    }))

    return {
        paths,
        fallback: true,
    }
}

export async function getStaticProps({ params }) {
    const { slug } = params
    let offer = await prisma.offers.findFirst({
        where: { slug },
        include: {
            images: true,
            seo: true,
        },
    })

    offer = JSON.parse(JSON.stringify(offer))

    if (!offer.id) {
        return {
            redirect: {
                destination: '/404',
                permanent: false,
            },
        }
    }
    return {
        props: {
            offer,
        },
        revalidate: 60,
    }
}

OfferPage.defaultProps = {
    offer: {},
    seoIMG: 'https://firebasestorage.googleapis.com/v0/b/newtou-portfolio.appspot.com/o/projects%2Fvacay3.png?alt=media&token=996a6a4a-49ff-4e3e-8397-9f538cae056f',
}

export default OfferPage
