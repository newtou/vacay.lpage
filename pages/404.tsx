import React from 'react'
import Link from 'next/link'
import { Heading, Text, Button, Image, HStack, VStack } from '@chakra-ui/react'
import Layout from './Layout'
import { useRouter } from 'next/router'

function NotFound() {
    const router = useRouter()

    if (router.asPath === '/sitemap') {
        router.push('/sitemap-0.xml')
    }

    return (
        <Layout
            title='404 - Not found'
            description='The page you are looking for does not exist'>
            <HStack
                alignItems='center'
                justifyContent='center'
                height='50vh'
                width='100%'>
                <VStack spacing='8'>
                    <Image
                        src='/images/svg/travel.svg'
                        alt='not found'
                        objectFit='contain'
                        height='30%'
                        width='30%'
                    />

                    <VStack spacing='5'>
                        <Heading size='3xl'>404 Not Found!!</Heading>
                        <Text fontSize='md'>
                            Could not find the page you were looking for
                        </Text>

                        <Link href='/' passHref>
                            <Button
                                fontSize='lg'
                                height='3rem'
                                bg='brand.base'
                                _hover={{ bg: 'brand.primary.light' }}
                                _focus={{
                                    bg: 'brand.primary.light',
                                    outline: 'none',
                                }}
                                _active={{
                                    bg: 'brand.primary.light',
                                    outline: 'none',
                                }}
                                color='#fff'
                                rounded='10px'>
                                Go back home &rarr;
                            </Button>
                        </Link>
                    </VStack>
                </VStack>
            </HStack>
        </Layout>
    )
}

export default NotFound
