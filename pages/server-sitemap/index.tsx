import { GetServerSideProps } from 'next'
import { getServerSideSitemapLegacy, ISitemapField } from 'next-sitemap'
import prisma from 'pages/api/lib/prisma'

export const getServerSideProps: GetServerSideProps = async (ctx) => {
    const items = await prisma.offers.findMany({})

    const offers: ISitemapField[] =
        items &&
        items.map((item: { slug: string; updated: Date }) => ({
            loc: `${process.env.SITE_URL}/offers/${item.slug}`,
            lastmod: item?.updated
                ? new Date(item?.updated).toISOString()
                : new Date().toISOString(),
        }))
    return getServerSideSitemapLegacy(ctx, offers)
}

function Sitemap() {
    return null
}

export default Sitemap
