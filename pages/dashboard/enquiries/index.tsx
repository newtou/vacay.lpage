import Layout from '../Layout'
import EnquiryList from '@/src/components/enquiries/EnquiryList'
import { getEnquiries } from '@/src/store/slices/Enquiries'
import { Box } from '@chakra-ui/react'
import React from 'react'
import { useEffect } from 'react'
import {  useDispatch, useSelector } from 'react-redux'
import { AppDispatch, RootState } from '@/src/store'

function Enquiries() {
    const dispatch = useDispatch<AppDispatch>()

    useEffect(() => {
        dispatch(getEnquiries({}))
    }, [dispatch])

    const { enquiries } = useSelector(
        (state: RootState) => state.enquiries
    )
    return (
        <Layout title='Dashboard Enquiries'>
            <Box width='auto' mx='auto' my='2rem'>
                <EnquiryList enquiries={enquiries} />
            </Box>
        </Layout>
    )
}

Enquiries.defaultProps = {
    enquiries: [],
}

export default Enquiries
