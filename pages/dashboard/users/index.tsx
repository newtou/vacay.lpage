import React, { useEffect, useState } from 'react'
import Layout from '../Layout'
import { Box } from '@chakra-ui/react'
import { useDebounce } from 'use-debounce'
import { useDispatch } from 'react-redux'
import { fetchUsers } from '@/src/store/slices/Users'
import UserList from '@/src/components/dashboard/users/UserList'
import { AppDispatch } from '@/src/store'

function Users() {
    const dispatch = useDispatch<AppDispatch>()
    const [searchTerm, setSearchTerm] = useState('')
    const [value] = useDebounce(searchTerm, 2000)

    useEffect(() => {
        if (value) {
            dispatch(fetchUsers({ search: value }))
        } else {
            dispatch(fetchUsers({}))
        }
    }, [dispatch, value])

    return (
        <Layout title='Users'>
            <Box width='auto' mx='auto' my='1rem'>
                <UserList
                    searchTerm={searchTerm}
                    setSearchTerm={setSearchTerm}
                />
            </Box>
        </Layout>
    )
}

export default Users
