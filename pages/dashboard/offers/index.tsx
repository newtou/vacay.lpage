import { Box } from '@chakra-ui/react'
import React, { useState } from 'react'
import { useDebounce } from 'use-debounce'
import Layout from '../Layout'
import OffersList from '@/src/components/dashboard/offers/OffersList'
import useOffers from '@/src/hooks/useOffers'

function Offers() {
    const [searchTerm, setSearchTerm] = useState('')
    const [value] = useDebounce(searchTerm, 1000)
    const { loading, offers } = useOffers(value)

    return (
        <Layout title='Dashboard - offers'>
            <Box minHeight='60vh' width='auto' mx='auto' p='20px 0'>
                <Box>
                    <OffersList
                        offers={offers}
                        loading={loading}
                        searchTerm={searchTerm}
                        setSearchTerm={setSearchTerm}
                    />
                </Box>
            </Box>
        </Layout>
    )
}

export default Offers
