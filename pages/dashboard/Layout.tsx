import React, { useEffect } from 'react'
import Head from 'next/head'
import { useRouter } from 'next/router'
import { useDispatch } from 'react-redux'
import { Box, Container, Grid, GridItem } from '@chakra-ui/react'
import { useSession } from 'next-auth/react'
import Navbar from '@/src/components/common/Navbar'
import { fetchMe } from '@/src/store/slices/Auth'
import { AppDispatch } from '@/src/store'
import Sidebar from '@/src/components/common/Sidebar'

type LayoutProps = {
    children: React.ReactNode
    title?: string
}

function Layout({ children, title }: LayoutProps) {
    const router = useRouter()
    const dispatch = useDispatch<AppDispatch>()
    const { data: session, status } = useSession()

    useEffect(() => {
        if (session?.user?.user_id) {
            dispatch(fetchMe(session.user.user_id))
        }
    }, [dispatch, session?.user?.user_id])

    useEffect(() => {
        if (status === 'unauthenticated') {
            router.replace('/auth')
        }
    }, [status, router])

    return (
        <>
            <Head>
                <title>{title}</title>
            </Head>
            <Box width='100%' position='relative'>
                <Navbar />

                <Container maxW='75%' width='100%'>
                    <Grid templateColumns='repeat(4, 1fr)' mt='3rem'>
                        <GridItem colSpan={1}>
                            <Sidebar />
                        </GridItem>
                        <GridItem colSpan={3}>{children}</GridItem>
                    </Grid>
                </Container>
            </Box>
        </>
    )
}

Layout.defaultProps = {
    title: 'Vacay | Dashboard',
}

export default Layout
