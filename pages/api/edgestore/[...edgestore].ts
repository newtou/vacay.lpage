import { z } from 'zod'
import { initEdgeStore } from '@edgestore/server'
import { createEdgeStoreNextHandler } from '@edgestore/server/adapters/next/pages'

const es = initEdgeStore.create()

/**
 * This is the main router for the edgestore buckets.
 */
const edgeStoreRouter = es.router({
    publicFiles: es
        .fileBucket({
            maxSize: 1024 * 1024 * 10, // 10MB
            accept: ['image/jpeg', 'image/png'], // wildcard also works: ['image/*']
        })
        .input(z.object({ itemId: z.string() }))
        .path(({ input }) => [{ item: input.itemId }])

        .beforeDelete(() => {
            return true // allow delete
        }),
})

export default createEdgeStoreNextHandler({
    router: edgeStoreRouter,
})

/**
 * This type is used to create the type-safe client for the frontend.
 */
export type EdgeStoreRouter = typeof edgeStoreRouter
