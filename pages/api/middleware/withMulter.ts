import { NextApiRequest, NextApiResponse } from 'next'
import { Request, Response } from 'express'
import upload from '../resources/multer'
import logger from '../utils/logger'

// eslint-disable-next-line no-unused-vars
type Handler = (req: NextApiRequest, res: NextApiResponse) => Promise<void>

type NextApiRequestWithFormData = NextApiRequest &
    Request & {
        files: any[]
    }

type NextApiResponseCustom = NextApiResponse & Response

const withMulter = (handler: Handler) => {
    return async (
        req: NextApiRequestWithFormData,
        res: NextApiResponseCustom
    ): Promise<any> => {
        const uploadHandler = upload.array('files', 5)

        uploadHandler(req, res, async (error: any) => {
            if (error) {
                logger.error(error)
                res.status(400).send(error.message)
                return
            }

            await handler(req, res)
        })
    }
}

export default withMulter
