import { NextApiRequest, NextApiResponse } from 'next'
import { getServerSession } from 'next-auth/next'
import { authOptions } from 'pages/api/auth/[...nextauth]'

// eslint-disable-next-line no-unused-vars
type Handler = (req: NextApiRequest, res: NextApiResponse) => Promise<void>

const withAuth = (handler: Handler, methods: string[]) => {
    return async (req: NextApiRequest, res: NextApiResponse) => {
        if (methods.includes(req.method)) {
            const session = await getServerSession(req, res, authOptions)

            if (!session) {
                res.status(401).send({ message: 'Unauthorized' })
                return
            }
            await handler(req, res)
        } else {
            await handler(req, res)
        }
    }
}

export default withAuth
