/**
    @description Includes all auth helper methods
*/
// import { NextApiHandler, NextApiRequest, NextApiResponse } from 'next'
import { sign } from 'jsonwebtoken'
import { genSalt, hash } from 'bcryptjs'
// import jwt from 'jsonwebtoken'

// hash a password
export async function createHashPass(password: string) {
    const salt = await genSalt(12)
    const hashedPass = await hash(password, salt)
    return hashedPass
}

// create a token
export async function generateToken(params = {}, opts = {}) {
    const secret = process.env.JWT_SECRET
    const token = sign(params, secret, opts)
    return token
}

// export const authMiddleware = (
//     req: NextApiRequest,
//     res: NextApiRequest,
//     next: () => void
// ) => {
//     const secret = process.env.JWT_SECRET
//     const token =
//         req.headers['x-access-token'] || req.headers['authorization'] || null

//     if (!token) {
//         res.user.authenticated = false
//         next()
//     } else {
//         try {
//             const decoded = jwt.verify(token, secret)
//             req.user = decoded
//             req.user.authenticated = true
//             next()
//         } catch (error) {
//             return res.status(401).send('Authentication token is invalid')
//         }
//     }
// }
