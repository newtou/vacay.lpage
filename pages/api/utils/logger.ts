import { createLogger, format, transports, addColors } from 'winston'
import 'winston-daily-rotate-file'
import { sendToSlack } from './slack'

addColors({
    error: 'red',
    warn: 'yellow',
    info: 'cyan',
    debug: 'green',
})

const getLogger = () => {
    const consoleTransport = new transports.Console({
        level: 'debug',
        handleExceptions: false,
        format: format.printf(
            (info) => `${info.timestamp} ${info.level}: ${info.message}`
        ),
    })

    const logger = createLogger({
        level: 'info',
        format: format.combine(
            format.timestamp({
                format: 'YYYY-MM-DD HH:mm:ss',
            }),
            format.errors({ stack: true }),
            format.splat(),
            format.prettyPrint(),
            format.printf((info) => {
                if (info.level === 'error') {
                    sendToSlack(info)
                }
                info.label = process.env.NODE_ENV
                return `${info.timestamp}[${info.label}] ${info.level}: ${info.message}`
            })
        ),
        defaultMeta: { service: 'vacay' },
        transports: [consoleTransport],
    })

    return logger
}

export default getLogger()
