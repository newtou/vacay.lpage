import axios from 'axios'
import { TransformableInfo } from 'logform';

const slackWebhook = process.env.SLACK_WEBHOOK

export async function sendInfoToSlack(text: { message: any; title: any; value: any }) {
    const payload = {
        attachments: [
            {
                fallback: text.message,
                pretext: 'You have a message from the API',
                title: text.title,
                color: '#805AD5',
                fields: [
                    {
                        title: 'Message',
                        value: text.message,
                        short: false,
                    },
                    {
                        title: 'Values',
                        value: JSON.stringify(text.value),
                        short: false,
                    },
                    {
                        title: 'Project',
                        value: 'Vacay API',
                        short: true,
                    },
                    {
                        title: 'Environment',
                        value: process.env.NODE_ENV,
                        short: true,
                    },
                ],
            },
        ],
    }
    await axios.post(slackWebhook, payload, {
        headers: { 'Content-Type': 'application/json' },
    })
}

export async function sendToSlack(error: TransformableInfo) {
    const payload = {
        attachments: [
            {
                fallback:
                    error.message ||
                    error.description ||
                    'Something went wrong!',
                pretext: error.message || error.description,
                title: error.toString(),
                color: '#F35A00',
                fields: [
                    {
                        title: 'Error Stack',
                        value: error.stack || error.description,
                        short: false,
                    },
                    {
                        title: 'Project',
                        value: 'Vacay API',
                        short: true,
                    },
                    {
                        title: 'Environment',
                        value: process.env.NODE_ENV,
                        short: true,
                    },
                ],
            },
        ],
    }
    await axios.post(slackWebhook, payload, {
        headers: { 'Content-Type': 'application/json' },
    })
}
