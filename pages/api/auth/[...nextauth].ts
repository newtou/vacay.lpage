import NextAuth, { NextAuthOptions } from 'next-auth'
import Credentials from 'next-auth/providers/credentials'
import Email from 'next-auth/providers/email'
import { compare } from 'bcryptjs'
import prisma from '../lib/prisma'
import { PrismaAdapter } from '@next-auth/prisma-adapter'

export const authOptions: NextAuthOptions = {
    session: {
        strategy: 'jwt',
    },
    adapter: PrismaAdapter(prisma),
    providers: [
        Credentials({
            name: 'Credentials',
            credentials: {
                email: {
                    label: 'Email',
                    type: 'text',
                    placeholder: 'someone@mail.com',
                },
                password: { label: 'Password', type: 'password' },
            },
            async authorize(credentials) {
                const { email, password } = credentials

                const user = await prisma.users.findFirst({
                    where: { email },
                })

                if (!user) {
                    throw new Error('Credentials not valid')
                }

                const validPassword = await compare(password, user.password)

                if (!validPassword) {
                    throw new Error('Credentials not valid')
                }

                const authenticatedUser = {
                    ...user,
                }

                delete authenticatedUser.password

                // console.log({ authenticatedUser })
                return authenticatedUser
            },
        }),
        Email({
            server: {
                host: process.env.EMAIL_SERVER_HOST,
                port: Number(process.env.EMAIL_SERVER_PORT),
                auth: {
                    user: process.env.EMAIL_SERVER_USER,
                    pass: process.env.EMAIL_SERVER_PASSWORD,
                },
                from: process.env.EMAIL_FROM,
            },
        }),
    ],
    pages: {
        signIn: '/auth',
    },
    callbacks: {
        jwt({ token, user }) {
            if (user) {
                token.user_id = user?.id as string
            }
            return token
        },

        session({ session, token }) {
            if (token.user_id) {
                session.user.user_id = token?.user_id as string
            }
            return session
        },
    },

    jwt: {
        secret: process.env.NEXTAUTH_SECRET,
    },
}

export default NextAuth(authOptions)
