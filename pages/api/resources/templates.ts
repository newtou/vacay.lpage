import { Enquiry } from '@/types/index'
import moment from 'moment'

// export const userTemplate = (enquiry: Enquiry) => `
//     <!DOCTYPE html>
// <html lang="en">
//     <head>
//         <meta charset="UTF-8" />
//         <meta name="viewport" content="width=device-width, initial-scale=1.0" />
//         <meta http-equiv="X-UA-Compatible" content="ie=edge" />
//         <script src="https://cdn.tailwindcss.com"></script>
//         <title>${enquiry.offer}</title>
//     </head>
//     <body>
//         <main class="mx-auto w-full md:w-3/4 lg:w-1/3">
//             <section class="mx-auto p-10 text-center" style="margin:0 auto; padding: 10px; text-align: center;">
//                 <img
//                     src="https://res.cloudinary.com/devtact/image/upload/v1676730468/vacay-media/logo-transparent_irgg5i.png"
//                     alt="Vacay Logo"
//                     class="mx-auto w-1/2 h-[70px] object-contain"
//                 />
//                 <h2 class="text-3xl font-bold my-4 text-blue-700 my-2">
//                     ${enquiry.offer.name}
//                 </h2>
//                 <img
//                     src=${enquiry.offer.images[0].url}
//                     alt="Vacay Logo"
//                     class="mx-auto w-full h-[200px] object-cover rounded-lg"
//                 />
//                 <h4 class="text-lg font-bold my-4">Hi ${enquiry.name}</h4>

//                 <div class="flex flex-col gap-3">
//                     <p>
//                         Thank you for your enquiry about ${enquiry.offer.name} offer.
//                         Someone from the reservations team will be in touch
//                         shortly.
//                     </p>
//                     <div class="my-2">
//                         <a
//                             href="https://offers.vacay.co.ke"
//                             target="_blank"
//                             rel="noopener noreferer"
//                             class="btn bg-blue-500 px-4 py-2 text-white rounded-lg"
//                             >View More Offers</a
//                         >
//                     </div>
//                     <p class="text-gray-600">
//                         For more enquiries, please contact us at
//                         <a href="MAILTO:info@vacay.co.ke" class="text-blue-500"
//                             >info@vacay.co.ke</a
//                         >
//                     </p>
//                 </div>
//             </section>
//         </main>
//     </body>
// </html>

// `

export const adminTemplate = (params: Enquiry) => `
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <meta http-equiv="X-UA-Compatible" content="ie=edge" />
        <script src="https://cdn.tailwindcss.com"></script>
        <title>${params.offer.name}</title>
    </head>
    <body>
        <main
            class="mx-auto w-full md:w-3/4 lg:w-1/3"
            style="margin: 0 auto; width: 90%; max-width: 768px; padding: 10px;"
        >
            <section
                class="container mx-auto p-10 text-center"
                style="
                    margin: 0 auto;
                    text-align: center;
                    width: 100%;
                    max-width: 768px;
                    font-size: 18px;
                "
            >
                <div
                    class="my-3 flex flex-col gap-2"
                    style="
                        margin: 12px 0;
                        display: flex;
                        flex-direction: column;
                        gap: 5px;
                    "
                >
                    <p>
                        ${params.name} has enquired about ${
    params.offer.name
} offer.
                        Details are listed below and can also be found on the
                        dashboard
                    </p>
                    <div
                        class="bg-gray-50 p-2 rounded-lg grid grid-cols-2 gap-2"
                        style="
                            background-color: #f9fafb;
                            padding: 8px;
                            border-radius: 0.375rem;
                            display: grid;
                            grid-template-columns: repeat(2, minmax(0, 1fr));
                            gap: 5px;
                        "
                    >
                        <div
                            class="border-2 border-dashed p-2 text-left flex flex-col"
                            style="
                                border: 2px dashed #e5e7eb;
                                padding: 8px;
                                display: flex;
                                flex-direction: column;
                            "
                        >
                            <span class="font-bold" style="font-weight: bold"
                                >Name:</span
                            >
                            ${params.name}
                        </div>
                        <div
                            class="border-2 border-dashed p-2 text-left flex flex-col"
                            style="
                                border: 2px dashed #e5e7eb;
                                padding: 8px;
                                display: flex;
                                flex-direction: column;
                            "
                        >
                            <span class="font-bold" style="font-weight: bold"
                                >Email:</span
                            >
                            ${params.email}
                        </div>
                        <div
                            class="border-2 border-dashed p-2 text-left flex flex-col"
                            style="
                                border: 2px dashed #e5e7eb;
                                padding: 8px;
                                display: flex;
                                flex-direction: column;
                            "
                        >
                            <span class="font-bold" style="font-weight: bold"
                                >Phone Number:</span
                            >
                            ${params.number}
                        </div>
                        <div
                            class="border-2 border-dashed p-2 text-left flex flex-col"
                            style="
                                border: 2px dashed #e5e7eb;
                                padding: 8px;
                                display: flex;
                                flex-direction: column;
                            "
                        >
                            <span class="font-bold" style="font-weight: bold"
                                >Destination:</span
                            >
                            ${params.destination}
                        </div>
                        <div
                            class="border-2 border-dashed p-2 text-left flex flex-col"
                            style="
                                border: 2px dashed #e5e7eb;
                                padding: 8px;
                                display: flex;
                                flex-direction: column;
                            "
                        >
                            <span class="font-bold" style="font-weight: bold"
                                >Departure:</span
                            >
                            ${moment(params.departure).format('DD/MMM/YYYY')}
                        </div>
                        <div
                            class="border-2 border-dashed p-2 text-left flex flex-col"
                            style="
                                border: 2px dashed #e5e7eb;
                                padding: 8px;
                                display: flex;
                                flex-direction: column;
                            "
                        >
                            <span class="font-bold" style="font-weight: bold"
                                >Budget:</span
                            >
                            ${params.budget}
                        </div>
                        <div
                            class="border-2 border-dashed p-2 text-left flex flex-col"
                            style="
                                border: 2px dashed #e5e7eb;
                                padding: 8px;
                                display: flex;
                                flex-direction: column;
                            "
                        >
                            <span class="font-bold" style="font-weight: bold"
                                >Adults:</span
                            >
                            ${params.adult}
                        </div>
                        <div
                            class="border-2 border-dashed p-2 text-left flex flex-col"
                            style="
                                border: 2px dashed #e5e7eb;
                                padding: 8px;
                                display: flex;
                                flex-direction: column;
                            "
                        >
                            <span class="font-bold" style="font-weight: bold"
                                >Children:</span
                            >
                            ${params.child}
                        </div>
                    </div>
                    <div
                        class="border-2 border-dashed p-2 text-left flex flex-col"
                        style="
                            border: 2px dashed #e5e7eb;
                            padding: 8px;
                            display: flex;
                            flex-direction: column;
                            "
                        >
                            <span class="font-bold" style="font-weight: bold">Comments from client:</span>
                            ${params.message}
                    </div>
                    <p>
                        For more details, please contact the client at
                        <a
                            href="MAILTO:${params.email}"
                            class="text-blue-600"
                            style="color: #2563eb"
                            >${params.email}</a
                        >
                    </p>
                </div>
            </section>
        </main>
    </body>
</html> `
