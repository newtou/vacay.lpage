import logger from '../utils/logger'
import sendgrid from '@sendgrid/mail'

sendgrid.setApiKey(process.env.SENDGRID_API_KEY)

type MailOptions = {
    from?: string
    to?: string
    subject: string
    text?: string
    templateData?: {
        name: string
        offer: string
        image: string
        link: string
    }
}

export const sendMail = async (params: MailOptions) => {
    const mailOptions = {
        from: `info@vacay.co.ke`,
        to: params.to,
        subject: params.subject,
        templateId: 'd-a82b5e0cc7904b0093bd9ddfc14be08a',
        dynamic_template_data: {
            ...params.templateData,
        },
    }

    await sendgrid.send(mailOptions, false, (err, res) => {
        if (err) {
            logger.error(err)
        } else {
            logger.info(
                `mail sent to ${
                    params.to
                } at ${new Date().toISOString()}.Status: ${res[0].statusCode}`
            )
        }
    })
}

export const sendAdminMail = async (params: MailOptions) => {
    const mailOptions = {
        from: 'booking@vacay.co.ke',
        to: 'info@vacay.co.ke',
        subject: params.subject,
        html: params.text,
    }

    await sendgrid.send(mailOptions, false, (err, res) => {
        if (err) {
            logger.error(err)
        } else {
            logger.info(
                `mail sent to admin at ${new Date().toISOString()}.Status: ${
                    res[0].statusCode
                }`
            )
        }
    })
}
