import multer from 'multer'

const upload = multer({
    storage: multer.diskStorage({}),
    fileFilter: (
        req,
        file,
        cb: (error: any, res: boolean) => void // eslint-disable-line no-unused-vars
    ) => {
        if (!file.mimetype.match(/jpg|jpeg|png|gif$i/)) {
            return cb(new Error('Only image files are allowed!'), false)
        }

        cb(null, true)
    },
})

export default upload
