import cloudinary from 'cloudinary'

const cloudinaryInstance = cloudinary.v2

cloudinaryInstance.config({
    cloud_name: process.env.CLOUD_NAME,
    api_key: process.env.CLOUD_KEY,
    api_secret: process.env.CLOUD_SECRET,
})

// upload function
export const upload = async (file: string) => {
    const resp = await cloudinaryInstance.uploader.upload(file, {
        upload_preset: process.env.CLOUD_UPLOAD_PRESET,
    })

    return { url: resp.secure_url, pubId: resp.public_id }
}

// delete function
export const deleteImage = async (pubId: string) => {
    await cloudinaryInstance.uploader.destroy(pubId)
}

