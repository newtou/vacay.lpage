import { NextApiRequest, NextApiResponse } from 'next'
import logger from '../../utils/logger'
import { sendAdminMail, sendMail } from '../../resources/mailer'
import { adminTemplate } from 'pages/api/resources/templates'
import { generateToken } from 'pages/api/utils/authMethods'
import prisma from '../../lib/prisma'

const handler = async (req: NextApiRequest, res: NextApiResponse) => {
    const { method } = req

    switch (method) {
        case 'GET':
            try {
                const { query } = req as { [key: string]: any }

                const count = await prisma.enquiries.count()

                let limit = query.limit ? parseInt(query.limit) : count
                let page = query.page ? parseInt(query.page) : 1

                const offset = (page - 1) * limit

                const enquiries = await prisma.enquiries.findMany({
                    include: {
                        offer: true,
                    },
                    take: limit,
                    skip: offset,
                    orderBy: {
                        created: 'desc',
                    },
                })

                res.status(200).send({ enquiries })
            } catch (error) {
                logger.error(error)
                res.status(error.statusCode || 500).send(error.message)
            }

            break

        case 'POST':
            try {
                const data = req.body

                const offer_id = data.offerID
                delete data.offerID

                const enquiry = await prisma.enquiries.create({
                    data: {
                        ...data,
                        departure: new Date(data.departure),
                        offer: {
                            connect: {
                                id: offer_id,
                            },
                        },
                    },
                    include: {
                        offer: {
                            include: {
                                images: true,
                            },
                        },
                    },
                })

                const userParams = {
                    from: 'info@vacay.co.ke',
                    to: data.email,
                    subject: 'Thank you for your enquiry',
                    templateData: {
                        name: data.name,
                        offer: enquiry.offer.name,
                        image: enquiry.offer.images[0].url,
                        link: `https://offers.vacay.co.ke/offers/${enquiry.offer.slug}`,
                    },
                }

                const adminParams = {
                    subject: `New enquiry from ${data.name} for ${enquiry.offer.name}`,
                    text: adminTemplate({
                        ...JSON.parse(JSON.stringify(enquiry)),
                    }),
                }

                // send email to user
                await sendMail(userParams)

                // send email to admin
                await sendAdminMail(adminParams)

                const params = {
                    name: data.name,
                    confirmed: true,
                    offer: data.offer,
                }

                const token = await generateToken(params, {
                    expiresIn: 10 * 60 * 1000,
                })

                res.status(201).send({
                    token,
                    enquiry: enquiry,
                    message: 'Successfully sent your enquiry.',
                })
            } catch (error) {
                logger.error(error)
                res.status(error.statusCode || 500).send(error.message)
            }
            break
        default:
            res.status(405).send(`You can't do that!`)
    }
}

export default handler
