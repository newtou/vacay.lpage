import { NextApiRequest, NextApiResponse } from 'next'
import logger from '../../utils/logger'
import prisma from '../../lib/prisma'

const handler = async (req: NextApiRequest, res: NextApiResponse) => {
    const { method } = req

    switch (method) {
        case 'DELETE':
            try {
                const { id } = req.query as { id: string }
                const existingEnquiry = await prisma.enquiries.findFirst({
                    where: { id },
                })

                if (!existingEnquiry.id) {
                    return res.status(404).send('Enquiry not found')
                }

                const deletedProduct = await prisma.enquiries.delete({
                    where: { id },
                })

                res.status(200).send({
                    enquiry: deletedProduct,
                    message: 'Enquiry deleted',
                })
            } catch (error) {
                logger.error(error)
                res.status(error.statusCode || 500).send(error.message)
            }
            break

        default:
            res.status(405).send(`You can't do that!`)
    }
}

export default handler
