import logger from '../../utils/logger'
import { NextApiResponse, NextApiRequest } from 'next'
import prisma from 'pages/api/lib/prisma'

function cleanText(htmlText: string) {
    return htmlText.replace(/<\/?[^>]+(>|$)/g, '')
}

const handler = async (req: NextApiRequest, res: NextApiResponse) => {
    const { method } = req

    switch (method) {
        case 'POST':
            try {
                const { text } = req.body

                if (!text) {
                    res.status(400).send('Missing text')
                    return
                }

                const cleanedText = cleanText(text as string)

                // calculate meta score to get similar offers
                const recommended = await prisma.offers.findRaw({
                    filter: {
                        $text: {
                            $search: cleanedText,
                        },
                        active: true,
                    },
                    options: {
                        $meta: 'textScore',
                        $sort: { score: { $meta: 'textScore' } },
                        limit: 8,
                        projection: {
                            id: true,
                            name: true,
                            slug: true,
                            images: true,
                            locations: true,
                        },
                    },
                })

                res.status(200).send({ recommended })
            } catch (error) {
                logger.error(error)
                res.status(error.statusCode || 500).send(error.message)
            }

            break

        default:
            res.status(405).send(`You can't do that!`)
    }
}

export default handler
