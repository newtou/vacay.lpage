import logger from '../../utils/logger'
import { NextApiResponse, NextApiRequest } from 'next'
import prisma from 'pages/api/lib/prisma'

const handler = async (req: NextApiRequest, res: NextApiResponse) => {
    const { method } = req

    switch (method) {
        case 'GET':
            try {
                const { query } = req as {
                    query: { [key: string]: any }
                }
                const count = await prisma.offers.count({
                    where: { active: true },
                })

                // calculate all pages based on count
                const totalPages = Math.ceil(count / parseInt(query.limit))

                let limit = query.limit ? parseInt(query.limit) : count
                let page = query.page ? parseInt(query.page) : 1

                const offset = (page - 1) * limit

                const hasMore = page < totalPages

                if (query.search) {
                    const offers = await prisma.offers.findMany({
                        include: {
                            images: true,
                            seo: true,
                        },
                        take: limit,
                        skip: offset,
                        where: {
                            name: {
                                contains: query.search,
                                mode: 'insensitive',
                            },
                            active: true,
                        },
                        orderBy: {
                            created: 'desc',
                        },
                    })

                    return res
                        .status(200)
                        .send({
                            offers,
                            count,
                            totalPages,
                            hasMore,
                            currentPage: page,
                        })
                }

                const offers = await prisma.offers.findMany({
                    take: limit,
                    skip: offset,
                    where: { active: true },
                    select: {
                        id: true,
                        name: true,
                        slug: true,
                        images: true,
                        locations: true,
                    },
                    orderBy: {
                        created: 'desc',
                    },
                })

                res.status(200).send({
                    offers,
                    count,
                    totalPages,
                    hasMore,
                    currentPage: page,
                })
            } catch (error) {
                logger.error(error)
                res.status(error.statusCode || 500).send(error.message)
            }
            break
        default:
            res.status(405).send(`You can't do that!`)
    }
}

export default handler
