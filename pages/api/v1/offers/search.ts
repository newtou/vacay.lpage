import logger from '../../utils/logger'
import { NextApiResponse, NextApiRequest } from 'next'
import prisma from 'pages/api/lib/prisma'

const handler = async (req: NextApiRequest, res: NextApiResponse) => {
    const { method } = req

    switch (method) {
        case 'GET':
            try {
                const { search } = req.query as { search: string }

                const results = await prisma.offers.findMany({
                    where: {
                        active: true,
                        OR: [
                            {
                                name: {
                                    contains: search,
                                },
                            },
                            {
                                locations: {
                                    some: {
                                        name: {
                                            contains: search,
                                        },
                                    },
                                },
                            },
                        ],
                    },
                    include: {
                        images: true,
                    },
                    take: 8,
                })

                res.status(200).send({ results })
            } catch (error) {
                logger.error(error)
                res.status(error.statusCode || 500).send(error.message)
            }

            break

        default:
            res.status(405).send(`You can't do that!`)
    }
}

export default handler
