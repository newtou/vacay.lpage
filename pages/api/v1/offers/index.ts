import { NextApiRequest, NextApiResponse } from 'next'
import logger from '../../utils/logger'
import slugify from 'slugify'
import prisma from '../../lib/prisma'
import withAuth from 'pages/api/middleware/withAuth'

const handler = async (req: NextApiRequest, res: NextApiResponse) => {
    const { method } = req

    switch (method) {
        case 'GET':
            try {
                const { query } = req as {
                    query: { [key: string]: any }
                }
                const count = await prisma.offers.count()

                let limit = query.limit ? parseInt(query.limit) : count
                let page = query.page ? parseInt(query.page) : 1

                const offset = (page - 1) * limit

                if (query.search) {
                    const offers = await prisma.offers.findMany({
                        include: {
                            images: true,
                            seo: true,
                        },
                        take: limit,
                        skip: offset,
                        where: {
                            name: {
                                contains: query.search,
                                mode: 'insensitive',
                            },
                        },
                        orderBy: {
                            created: 'desc',
                        },
                    })

                    return res.status(200).send({ offers })
                }

                const offers = await prisma.offers.findMany({
                    include: {
                        images: true,
                        seo: true,
                    },
                    take: limit,
                    skip: offset,
                    orderBy: {
                        created: 'desc',
                    },
                })

                res.status(200).send({ offers })
            } catch (error) {
                logger.error(error)
                res.status(error.statusCode || 500).send(error.message)
            }
            break

        case 'POST':
            try {
                const data = req.body
                const slug = slugify(data.name, { lower: true })

                const offer = await prisma.offers.create({
                    data: {
                        slug,
                        ...data,
                    },
                })

                res.status(201).send({
                    message: 'Offer created successfully',
                    offer,
                })
            } catch (error) {
                logger.error(error)
                res.status(error.statusCode || 500).send(error.message)
            }
            break
        default:
            res.status(405).send(`You can't do that!`)
    }
}

export default withAuth(handler, ['POST'])
