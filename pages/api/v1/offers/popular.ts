import { NextApiResponse, NextApiRequest } from 'next'
import logger from '../../utils/logger'
import prisma from 'pages/api/lib/prisma'

const handler = async (req: NextApiRequest, res: NextApiResponse) => {
    const { method } = req
    switch (method) {
        case 'GET':
            try {
                // find to distinct values
                const offerIDs = await prisma.enquiries.findMany({
                    distinct: ['offersId'],
                    select: {
                        offersId: true,
                    },
                })

                // get the count of each offer
                const popular = await Promise.all(
                    offerIDs.map(async (item) => {
                        const count = await prisma.enquiries.count({
                            where: {
                                offersId: item.offersId as unknown as string,
                            },
                        })

                        const offer = await prisma.offers.findUnique({
                            where: {
                                id: item.offersId as unknown as string,
                            },

                            select: {
                                id: true,
                                name: true,
                                slug: true,
                                images: true,
                                locations: true,
                            },
                        })
                        return { offer, count }
                    })
                )

                // sort the array by the count
                const sorted = popular
                    .sort((a: { count: number }, b: { count: number }) => {
                        return b.count - a.count
                    })
                    .splice(0, 4)

                res.status(200).send({ popular: sorted })
            } catch (error) {
                logger.error(error)
                res.status(error.statusCode || 500).send(error.message)
            }
            break
        default:
            res.status(405).send(`You can't perform this action here`)
    }
}

export default handler
