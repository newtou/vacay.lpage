import { NextApiRequest, NextApiResponse } from 'next'
import logger from '../../utils/logger'
import { deleteImage } from '../../resources/cloudinary'
import prisma from '../../lib/prisma'
import slugify from 'slugify'


const handler = async (req: NextApiRequest, res: NextApiResponse) => {
    const { method } = req

    switch (method) {
        case 'PUT':
            try {
                const { id } = req.query as { id: string }

                const data = req.body as { [key: string]: any }

                const existing = await prisma.offers.findFirst({
                    where: { id: id },
                })

                if (!existing.id) {
                    return res.status(404).send('Offer not found')
                }

                if (data?.name) {
                    data.slug = slugify(data.name, { lower: true })
                }

                const update = {
                    ...data,
                    updated: new Date(),
                }

                const updatedOffer = await prisma.offers.update({
                    where: { id: id },
                    data: update,
                })

                res.status(200).send({
                    message: 'Offer successfully updated',
                    offer: updatedOffer,
                })
            } catch (error) {
                logger.error(error)
                res.status(error.statusCode || 500).send(error.message)
            }
            break

        case 'DELETE':
            try {
                const { id } = req.query as { id: string }

                const existingOffer = await prisma.offers.findFirst({
                    where: { id: id },
                })

                if (!existingOffer.id) {
                    return res.status(404).send('Offer not found in database')
                }

                const deletedProduct = await prisma.offers.delete({
                    where: { id },
                })

                const productImages = await prisma.images.findMany({
                    where: { map_id: id },
                })

                // Delete images from cloudinary and database
                if (productImages.length > 0) {
                    productImages.forEach(async (image) => {
                        // Delete from cloudinary if it's a cloudinary image
                        if (image.url.includes('res.cloudinary.com')) {
                            await deleteImage(image.pub_id)
                        }

                        await prisma.images.delete({
                            where: { id: image.id },
                        })
                    })
                }

                res.status(200).send({
                    offer: deletedProduct,
                    message: 'Offer deleted',
                })
            } catch (error) {
                logger.error(error)
                res.status(error.statusCode || 500).send(error.message)
            }
            break

        default:
            res.status(405).send(`You can't do that!`)
    }
}

export default handler
