import { NextApiRequest, NextApiResponse } from 'next'
import logger from '../../utils/logger'
import prisma from '../../lib/prisma'

const handler = async (req: NextApiRequest, res: NextApiResponse) => {
    const { method } = req
    switch (method) {
        case 'GET':
            try {
                const exists = await prisma.users.findFirst()
                res.status(200).send({ exists: !!exists })
            } catch (error) {
                logger.error(error)
                res.status(error.statusCode || 500).send(error.message)
            }
            break
        default:
            res.status(405).send(`You can't do that!`)
    }
}

export default handler
