import { NextApiRequest, NextApiResponse } from 'next'
import prisma from 'pages/api/lib/prisma'
import logger from 'pages/api/utils/logger'

const handler = async (req: NextApiRequest, res: NextApiResponse) => {
    const { method } = req

    switch (method) {
        case 'PUT':
            try {
                const userID = req.query.userID as string

                const user = await prisma.users.findFirst({
                    where: { id: userID },
                })

                if (!user) {
                    return res.status(404).send('User not found')
                }

                await prisma.users.update({
                    where: { id: userID },
                    data: {
                        lastLogin: new Date(),
                        updated: new Date(),
                    },
                })

                res.status(200).send({ message: 'ok' })
            } catch (error) {
                logger.error(error)
                res.status(error.statusCode || 500).send(error.message)
            }
            break
        default:
            res.status(405).send(`You can't do that!`)
    }
}

export default handler
