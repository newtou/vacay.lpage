import { NextApiRequest, NextApiResponse } from 'next'
import logger from '../../../utils/logger'
import prisma from '../../../lib/prisma'
import withAuth from '../../../middleware/withAuth'

const handler = async (req: NextApiRequest, res: NextApiResponse) => {
    const { method } = req
    switch (method) {
        case 'GET':
            try {
                const { id } = req.query
                const authedUser = await prisma.users.findFirst({
                    where: { id: id as string },
                    select: {
                        id: true,
                        name: true,
                        email: true,
                        permissions: true,
                        role: true,
                        created: true,
                        updated: true,
                        lastLogin: true,
                        password: false,
                    },
                })

                if (!authedUser) {
                    res.status(401).send({ message: 'Unauthorized user' })
                }

                res.status(200).send({ user: authedUser })
            } catch (error) {
                logger.error(error)
                res.status(error.statusCode || 500).send(error.message)
            }
            break
        default:
            res.status(405).send(`You can't do that!`)
    }
}

export default withAuth(handler, ['GET'])
