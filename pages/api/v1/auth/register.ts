import { NextApiRequest, NextApiResponse } from 'next'
import logger from '../../utils/logger'
import prisma from '../../lib/prisma'
import { createHashPass } from '../../utils/authMethods'

const AuthorizedEmails = process.env.AUTHORIZED_EMAILS

const handler = async (req: NextApiRequest, res: NextApiResponse) => {
    const { method } = req
    switch (method) {
        case 'POST':
            try {
                const data = req.body

                const authorizedUsers = AuthorizedEmails.split(',')

                if (!authorizedUsers.includes(data.email)) {
                    res.status(401).send('Unauthorized')
                    return
                }

                const existingUser = await prisma.users.findFirst({
                    where: { email: data.email },
                })

                if (existingUser) {
                    res.status(401).send('Unauthorized')
                }

                const hashPass = await createHashPass(data.password)

                const user = await prisma.users.create({
                    data: {
                        email: data.email,
                        password: hashPass,
                        role: 'content',
                    },
                })

                res.status(200).send({
                    user,
                })
            } catch (error) {
                logger.error(error)
                res.status(error.statusCode || 500).send(error.message)
            }
            break
        default:
            res.status(405).send(`You can't do that!`)
    }
}

export default handler
