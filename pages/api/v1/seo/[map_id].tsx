import { NextApiRequest, NextApiResponse } from 'next'
import logger from '../../utils/logger'
import prisma from 'pages/api/lib/prisma'

const handler = async (req: NextApiRequest, res: NextApiResponse) => {
    const { method } = req

    switch (method) {
        case 'GET':
            try {
                const { map_id } = req.query as { map_id: string }
                const seoData = await prisma.seo.findFirst({
                    where: { map_id },
                })

                return res.status(200).send({ seo: seoData })
            } catch (error) {
                logger.error(error)
                res.status(error.statusCode || 500).send(error)
            }
            break

        case 'POST':
            try {
                const { map_id } = req.query as { map_id: string }
                const data = req.body

                const created = await prisma.seo.create({
                    data: {
                        title: data.title,
                        imageUrl: data?.imageUrl,
                        keywords: data.keywords,
                        description: data.description,
                        offer: {
                            connect: {
                                id: map_id,
                            },
                        },
                    },
                })

                return res
                    .status(200)
                    .send({ seo: created, message: 'Seo details added' })
            } catch (error) {
                logger.error(error)
                res.status(error.statusCode || 500).send(error)
            }

        case 'PUT':
            try {
                const { map_id } = req.query as { map_id: string }
                const data = req.body

                const updated = await prisma.seo.update({
                    where: { map_id },
                    data: {
                        ...data,
                        updated: new Date(),
                    },
                })

                return res.status(200).send({ seo: updated, message:'Seo details updated' })
            } catch (error) {
                logger.error(error)
                res.status(error.statusCode || 500).send(error)
            }
        default:
            res.status(405).send(`You can't do that!`)
    }
}

export default handler
