import { NextApiRequest, NextApiResponse } from 'next'
import logger from '../../utils/logger'
import prisma from 'pages/api/lib/prisma'

const handler = async (req: NextApiRequest, res: NextApiResponse) => {
    const { method } = req

    switch (method) {
        case 'GET':
            try {
                const categories = await prisma.category.findMany({
                    orderBy: {
                        name: 'asc',
                    },
                    include: {
                        offers: {
                            orderBy: {
                                name: 'asc',
                            },
                        },
                    },
                })

                return res.status(200).send({ categories })
            } catch (error) {
                logger.error(error)
                res.status(error.statusCode || 500).send(error)
            }
            break

        case 'POST':
            try {
                const data = req.body as { name: string }

                const created = await prisma.category.create({
                    data: {
                        name: data.name,
                        created: new Date(),
                        updated: new Date(),
                    },
                })

                return res
                    .status(200)
                    .send({ seo: created, message: 'Category added' })
            } catch (error) {
                logger.error(error)
                res.status(error.statusCode || 500).send(error)
            }

        default:
            res.status(405).send(`You can't do that!`)
    }
}

export default handler
