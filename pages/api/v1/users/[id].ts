import { NextApiRequest, NextApiResponse } from 'next'
import logger from '../../utils/logger'
import { createHashPass } from '../../utils/authMethods'
import prisma from '../../lib/prisma'

const handler = async (req: NextApiRequest, res: NextApiResponse) => {
    const { method } = req

    switch (method) {
        case 'GET':
            try {
                const { id } = req.query as { id: string }
                const user = await prisma.users.findFirst({
                    where: { id },
                })
                res.status(200).send({ user })
            } catch (error) {
                logger.error(error)
                res.status(error.statusCode || 500).send(error.message)
            }
            break

        case 'PUT':
            try {
                const { id } = req.query as { id: string }
                const data = req.body

                const existingUser = await prisma.users.findFirst({
                    where: { id },
                })

                if (!existingUser.email) {
                    return res.status(404).send('user does not exist')
                }

                const update = {
                    ...data,
                    updated: new Date().toISOString(),
                }

                delete update.id

                if (data.password) {
                    const hashPass = await createHashPass(data.password)
                    update.password = hashPass
                }

                const updatedUser = await prisma.users.update({
                    where: { id },
                    data: update,
                    select: {
                        id: true,
                        email: true,
                        name: true,
                        role: true,
                        created: true,
                        updated: true,
                        lastLogin: true,
                    },
                })

                const message = 'Successfully updated user'

                res.status(200).send({
                    message,
                    user: updatedUser,
                })
            } catch (error) {
                logger.error(error)
                res.status(error.statusCode || 500).send(error.message)
            }
            break

        case 'DELETE':
            try {
                const { id } = req.query as { id: string }
                const user = await prisma.users.findFirst({
                    where: { id },
                })

                if (!user.id) {
                    return res.status(404).send('user does not exist')
                }

                const deletedUser = await prisma.users.delete({
                    where: { id },
                })

                res.status(200).send({
                    message: 'User successfully deleted',
                    user: deletedUser,
                })
            } catch (error) {
                logger.error(error)
                res.status(error.statusCode || 500).send(error.message)
            }
            break

        default:
            res.status(405).send(`You can't do that!`)
    }
}

export default handler
