import { NextApiRequest, NextApiResponse } from 'next'
import logger from '../../utils/logger'
import { createHashPass } from '../../utils/authMethods'
import prisma from '../../lib/prisma'

const handler = async (req: NextApiRequest, res: NextApiResponse) => {
    const { method } = req
    switch (method) {
        case 'POST':
            try {
                const data = req.body
                const user = await prisma.users.findFirst({
                    where: { email: data.email },
                })

                if (user?.id) {
                    return res.status(400).json({
                        message: 'User already exists',
                    })
                }

                const hashPass = await createHashPass(data.password)

                const savedUser = await prisma.users.create({
                    data: {
                        ...data,
                        password: hashPass,
                    },
                })

                res.status(201).send({
                    message: 'New user created',
                    user: savedUser,
                })
            } catch (error) {
                logger.error(error)
                res.status(error.statusCode || 500).send(error.message)
            }
            break

        case 'GET':
            try {
                const users = await prisma.users.findMany({
                    select: {
                        id: true,
                        name: true,
                        email: true,
                        role: true,
                        created: true,
                        lastLogin: true,
                        updated: true,
                    },
                })
                res.status(200).send({ users })
            } catch (error) {
                logger.error(error)
                res.status(error.statusCode || 500).send(error.message)
            }
            break

        default:
            res.status(405).send(`You can't do that!`)
    }
}

export default handler
