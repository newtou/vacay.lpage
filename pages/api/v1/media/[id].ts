import { NextApiRequest, NextApiResponse } from 'next'
import logger from '../../utils/logger'
import { deleteImage } from '../../resources/cloudinary'
import prisma from 'pages/api/lib/prisma'

const handler = async (req: NextApiRequest, res: NextApiResponse) => {
    const { method } = req
    switch (method) {
        case 'DELETE':
            try {
                const { id } = req.query as { id: string }
                const deletedMedia = await prisma.images.delete({
                    where: { id },
                })

                if (deletedMedia?.url.includes('res.cloudinary.com')) {
                    await deleteImage(deletedMedia.pub_id)
                }

                return res
                    .status(200)
                    .send({
                        image: deletedMedia,
                        message: 'Image Deleted Successfully',
                    })
            } catch (error) {
                logger.error(error)
                res.status(error.statusCode || 500).send(error.message)
            }
            break
        default:
            res.status(405).send(`You can't do that!`)
    }
}

export default handler
