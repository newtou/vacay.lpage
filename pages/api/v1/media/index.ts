import { NextApiRequest, NextApiResponse } from 'next'
import logger from '../../utils/logger'
import prisma from 'pages/api/lib/prisma'

const handler = async (req: NextApiRequest, res: NextApiResponse) => {
    const { method } = req

    switch (method) {
        case 'POST':
            try {
                const { map_id, files } = req.query as unknown as {
                    map_id: string
                    files: string | string[]
                }

                if (!files) {
                    return res.status(400).send('No files uploaded.')
                }

                const data = Array.isArray(files) ? files : [files]

                const imageRecords = await prisma.images.createMany({
                    data: data.map((image) => ({
                        url: image,
                        pub_id: map_id,
                        alt: '',
                        map_id,
                    })),
                })

                return res.status(200).send({ images: imageRecords })
            } catch (error) {
                logger.error(error)
                res.status(error.statusCode || 500).send(error)
            }
            break

        case 'GET':
            try {
                const { mapId } = req.query as { mapId: string }
                const images = await prisma.images.findMany({
                    where: { map_id: mapId },
                })

                return res.status(200).send({ images })
            } catch (error) {
                logger.error(error)
                res.status(error.statusCode || 500).send(error)
            }
            break
        default:
            res.status(405).send(`You can't do that!`)
    }
}

export default handler

export const config = {
    api: {
        bodyParser: false,
    },
}
