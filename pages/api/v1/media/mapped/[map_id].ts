import { NextApiRequest, NextApiResponse } from 'next'
import logger from '../../../utils/logger'
import prisma from 'pages/api/lib/prisma'

const handler = async (req: NextApiRequest, res: NextApiResponse) => {
    const { method } = req
    switch (method) {
        case 'GET':
            try {
                const { map_id } = req.query as { map_id: string }
                const images = await prisma.images.findMany({
                    where: { map_id },
                })

                return res.status(200).send({ images })
            } catch (error) {
                logger.error(error)
                res.status(error.statusCode || 500).send(error)
            }
            break
        default:
            res.status(405).send(`You can't do that!`)
    }
}

export default handler
