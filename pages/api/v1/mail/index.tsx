import { NextApiRequest, NextApiResponse } from 'next'
import logger from '../../utils/logger'
import { sendMail, sendAdminMail } from '../../resources/mailer'
import withAuth from 'pages/api/middleware/withAuth'

const handler = async (req: NextApiRequest, res: NextApiResponse) => {
    const { method } = req

    switch (method) {
        case 'PUT':
            try {
                const data = req.body

                const mailOptions: any = {
                    to: data.to,
                    subject: data.subject,
                    text: `<p>${data.text}</p>`,
                }

                const adminMailOptions: any = {
                    subject: data.subject,
                    text: `<p> An email has been sent to ${data.to}</p>`,
                }

                sendMail(mailOptions)
                sendAdminMail(adminMailOptions)

                res.status(200).json({ success: 'mail sent' })
            } catch (error) {
                logger.error(error)
                res.status(error.statusCode || 500).send(error)
            }
            break

        case 'GET':
            res.status(200).json({ success: true })
            break
        default:
            res.status(405).send(`You can't do that!`)
    }
}

export default withAuth(handler, ["PUT"])
