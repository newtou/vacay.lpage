import {
    Box,
    Button,
    Grid,
    Heading,
    Alert,
    AlertIcon,
    Text,
    Divider,
    HStack,
    IconButton,
} from '@chakra-ui/react'
import React, { useEffect, useMemo, useState } from 'react'
import { Formik } from 'formik'
import * as yup from 'yup'
import { useRouter } from 'next/router'
import { HiOutlineChevronLeft } from 'react-icons/hi2'
import { useSelector } from 'react-redux'
import { signIn, useSession } from 'next-auth/react'
import LoginForm from '@/src/components/forms/auth/LoginForm'
import { invoke } from '@/src/lib/axios.config'
import Layout from 'pages/Layout'
import { RootState } from '@/src/store'

function SignIn({ userExists }) {
    const { authError } = useSelector((state: RootState) => state.alert)
    const router = useRouter()
    const { data: session } = useSession()

    const isAuthenticated = useMemo(() => {
        return session?.user?.email
    }, [session])

    const [alert, setAlert] = useState(null)
    const validateSchema = yup.object({
        email: yup
            .string()
            .email('A valid email is required')
            .required('Email field is required'),
        password: yup.string().required('Password field is required').min(6),
    })

    useEffect(() => {
        if (authError) {
            setAlert(authError)

            setTimeout(() => {
                setAlert(null)
            }, 4000)
        }
    }, [authError])

    useEffect(() => {
        if (isAuthenticated) {
            router.push('/dashboard/offers')
        }
    }, [isAuthenticated, router])

    return (
        <Layout title='Auth Page' hasHeader={false} hasFooter={false}>
            <Grid
                bgImage='/images/images/vacay.jpg'
                bgSize='cover'
                bgRepeat='no-repeat'
                bgPosition='center'
                alignItems='center'
                p='20px'
                width='100%'
                height='100vh'
                overflow='hidden'>
                <Box
                    width={['100%', '80%', '70%', '50%', '35%', '25%']}
                    mx='auto'
                    bg='whiteAlpha.900'
                    backdropFilter={'blur(10px)'}
                    rounded='10px'
                    shadow='xl'>
                    <Box p='20px 30px'>
                        <HStack spacing='4'>
                            <IconButton
                                aria-label='Back to home'
                                icon={<HiOutlineChevronLeft size='20px' />}
                                _focus={{ outline: 'none' }}
                                _active={{ outline: 'none' }}
                                variant='outline'
                                rounded='lg'
                                onClick={() => router.replace('/')}
                            />
                            <Heading fontSize='xl'>
                                Vacay Holiday Deals{' '}
                            </Heading>
                        </HStack>
                        <Divider mt='1rem' />
                        {alert && (
                            <Alert
                                status='error'
                                rounded='10px'
                                border='1px solid'
                                borderColor='red.100'
                                alignItems='flex-start'
                                p='20px'>
                                <AlertIcon />
                                <Text fontSize='lg'>{alert}</Text>
                            </Alert>
                        )}
                    </Box>
                    <Box>
                        <Formik
                            initialValues={{
                                email: '',
                                password: '',
                            }}
                            validationSchema={validateSchema}
                            onSubmit={async (values, _) => {
                                _.setSubmitting(true)
                                const credentials = {
                                    email: values.email,
                                    password: values.password,
                                }

                                const { error } = await signIn('credentials', {
                                    ...credentials,
                                    redirect: false,
                                })

                                if (error) {
                                    setAlert(error)
                                    _.setSubmitting(false)
                                    return
                                }

                                _.setSubmitting(false)
                                _.resetForm()
                            }}>
                            {({
                                handleSubmit,
                                errors,
                                touched,
                                isSubmitting,
                            }) => (
                                <Box>
                                    <Box p='0 30px'>
                                        <LoginForm
                                            loading={isSubmitting}
                                            errors={errors}
                                            touched={touched}
                                            submit={handleSubmit}
                                        />
                                    </Box>
                                    <Button
                                        type='submit'
                                        width='100%'
                                        mt='1rem'
                                        height='4rem'
                                        bg='brand.base'
                                        _hover={{ bg: 'brand.primary.light' }}
                                        _focus={{
                                            bg: 'brand.primary.light',
                                            outline: 'none',
                                        }}
                                        _active={{
                                            bg: 'brand.base',
                                            outline: 'none',
                                        }}
                                        color='white'
                                        fontSize='lg'
                                        rounded='0 0 10px 10px'
                                        isDisabled={isSubmitting}
                                        isLoading={isSubmitting}
                                        onClick={() => handleSubmit()}
                                        disabled={isSubmitting}>
                                        {userExists ? 'Sign In' : 'Register'}{' '}
                                        &rarr;
                                    </Button>
                                </Box>
                            )}
                        </Formik>
                    </Box>
                </Box>
            </Grid>
        </Layout>
    )
}

export async function getServerSideProps() {
    const { res, error } = await invoke('GET', `auth/exists`)
    if (error) {
        return {
            props: {
                userExists: false,
            },
        }
    }

    return {
        props: {
            userExists: res.exists,
        },
    }
}

export default SignIn
