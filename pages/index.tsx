import React from 'react'
import { Box } from '@chakra-ui/react'
import Layout from './Layout'
import Hero from '@/src/components/home/Hero'
import FeaturedList from '@/src/components/home/FeaturedList'
import OffersList from '@/src/components/home/OffersList'
import OffersSearchList from '@/src/components/home/OfferSearchList'
import PopularList from '@/src/components/home/PopularList'
import prisma from './api/lib/prisma'

function Home({ offers, featured }) {
    return (
        <Layout>
            <Box mx='auto'>
                <Hero />
                <Box width={['95%', '95%', '80%', '75%']} mx='auto'>
                    <OffersSearchList />
                    {featured.length > 0 && (
                        <FeaturedList featured={featured} />
                    )}
                    <Box id='offers'>
                        <OffersList offers={offers} />
                    </Box>
                    <PopularList />
                </Box>
            </Box>
        </Layout>
    )
}

export async function getStaticProps() {
    const data = await prisma.offers.findMany({
        where: {
            active: true,
            featured: true,
        },
        select: {
            id: true,
            name: true,
            slug: true,
            images: true,
            locations: true,
        },
        orderBy: {
            created: 'desc',
        },
        take: 4,
    })
    const offers = JSON.parse(JSON.stringify(data))

    return {
        props: {
            offers,
            featured: offers,
        },
        revalidate: 60,
    }
}

Home.defaultProps = {
    featured: [],
}

export default Home
