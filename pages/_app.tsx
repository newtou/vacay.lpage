import '../scss/global.scss'
import '@fontsource-variable/figtree'
import 'slick-carousel/slick/slick.css'
import 'slick-carousel/slick/slick-theme.css'

import React from 'react'
import { Provider as ReduxProvider } from 'react-redux'
import { SessionProvider } from 'next-auth/react'
import { AnimatePresence, motion } from 'framer-motion'
import { CSSReset, ChakraProvider } from '@chakra-ui/react'
import Router, { useRouter } from 'next/router'

import ContextProvider from '../src/context/ContextProvider'
import ProgressBar from '@badrap/bar-of-progress'
import store from '@/src/store'
import { theme } from '../src/lib/chakra.config'
import { usePreserveScroll } from '@/src/hooks/usePreserveScroll'
import usePixel from '@/src/hooks/usePixel'
import { EdgeStoreProvider } from '@/src/lib/edgestore'

const progress = new ProgressBar({
    size: 3,
    color: '#000411',
    className: 'progress-bar',
    delay: 50,
})

function MyApp({ Component, pageProps }) {
    const router = useRouter()

    Router.events.on('routeChangeStart', progress.start)
    Router.events.on('routeChangeComplete', progress.finish)
    Router.events.on('routeChangeError', progress.finish)

    usePixel()
    usePreserveScroll()

    return (
        <AnimatePresence mode='wait'>
            <motion.div
                key={router.route}
                initial='intial'
                animate='animate'
                exit='exit'
                // transition={{ duration: 0.5, delay: 0.5 }}
                variants={{
                    initial: {
                        opacity: 0,
                    },
                    animate: {
                        opacity: 1,
                    },
                    exit: {
                        opacity: 0,
                    },
                }}>
                <SessionProvider session={pageProps.session}>
                    <EdgeStoreProvider>
                        <ChakraProvider theme={theme}>
                            <CSSReset />
                            <ReduxProvider store={store}>
                                <ContextProvider>
                                    <Component {...pageProps} />
                                </ContextProvider>
                            </ReduxProvider>
                        </ChakraProvider>
                    </EdgeStoreProvider>
                </SessionProvider>
            </motion.div>
        </AnimatePresence>
    )
}

export default MyApp
