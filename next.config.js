/** @type {import('next').NextConfig} */
const nextConfig = {
    reactStrictMode: false,
    images: {
        domains: ['res.cloudinary.com', 'files.edgestore.dev'],
    },

    env: {
        // db
        MONGODB_URI: process.env.MONGODB_URI,

        // authorized emails
        AUTHORIZED_EMAILS: process.env.AUTHORIZED_EMAILS,

        // tinymce api key
        TINYMCE_KEY: process.env.TINYMCE_KEY,

        // slack
        SLACK_WEBHOOK: process.env.SLACK_WEBHOOK,

        // jwt
        JWT_SECRET: process.env.JWT_SECRET,

        // api
        SITE_URL: process.env.SITE_URL,

        // Cloudinary
        CLOUD_NAME: process.env.CLOUD_NAME,
        CLOUD_KEY: process.env.CLOUD_KEY,
        CLOUD_SECRET: process.env.CLOUD_SECRET,
        CLOUD_UPLOAD_PRESET: process.env.CLOUD_UPLOAD_PRESET,

        // mailer
        MAILER_HOST: process.env.MAILER_HOST,
        MAILER_USR: process.env.MAILER_USR,
        MAILER_PWD: process.env.MAILER_PWD,

        // sendgrid
        SENDGRID_API_KEY: process.env.SENDGRID_API_KEY,

        // meta - pixels
        META_PIXEL: process.env.META_PIXEL,
    },
}

module.exports = nextConfig
