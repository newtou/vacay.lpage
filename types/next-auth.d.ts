import NextAuth, { DefaultSession } from 'next-auth' // eslint-disable-line no-unused-vars

declare module 'next-auth' {
    /**
     * Returned by `useSession`, `getSession` and received as a prop on the `SessionProvider` React Context
     */
    // eslint-disable-next-line no-unused-vars
    interface Session {
        user: {
            user_id: string
        } & DefaultSession['user']
    }
}
