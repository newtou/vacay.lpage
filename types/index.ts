export type Offer = {
    id: string
    active: boolean
    created: string
    destinations: string[]
    featured: boolean
    images: Image[]
    inclexcl: string
    itinerary: string
    moreInfo: string
    name: string
    overview: string
    pricing: string
    slug: string
    updated: string
    locations: Location[]
    enquiries: Enquiry[]
    seo: Seo
}

export type Image = {
    id: string
    url: string
    pub_id: string
    alt: string
}

export type Seo = {
    id: string
    title: string
    keywords: string
    imageUrl: string
    description: string
    updated: string
    created: string
    map_id: string
}

export type Location = {
    name: string
    longitude: number
    latitude: number
}

export type Enquiry = {
    id: string
    adult: number
    budget: string
    child: number
    created: string
    departure: string
    destination: string
    email: string
    message: string
    name: string
    number: string
    offer: Offer
    offersId: string
}

export type User = {
    id?: string
    name: string
    email: string
    created?: string
    lastLogin?: string
    permissions?: string[]
    password?: string
    role?: string
    updated?: string
}

export type OffersQueryParams = {
    page?: number
    limit?: number
    search?: string
    sort?: string
    location?: string
    active?: boolean
    featured?: boolean
}
